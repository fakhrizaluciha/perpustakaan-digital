-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 22, 2023 at 10:55 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpustakaan`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_activation_attempts`
--

CREATE TABLE `auth_activation_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups`
--

CREATE TABLE `auth_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_groups`
--

INSERT INTO `auth_groups` (`id`, `name`, `description`) VALUES
(1, 'Admin', 'admin'),
(2, 'User', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups_permissions`
--

CREATE TABLE `auth_groups_permissions` (
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `permission_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups_users`
--

CREATE TABLE `auth_groups_users` (
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_groups_users`
--

INSERT INTO `auth_groups_users` (`group_id`, `user_id`) VALUES
(1, 5),
(2, 34),
(2, 38);

-- --------------------------------------------------------

--
-- Table structure for table `auth_logins`
--

CREATE TABLE `auth_logins` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `date` datetime NOT NULL,
  `success` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_logins`
--

INSERT INTO `auth_logins` (`id`, `ip_address`, `email`, `user_id`, `date`, `success`) VALUES
(1, '::1', 'Rizal', 1, '2023-01-09 06:39:04', 0),
(2, '::1', 'rizalmustaqim150@gmail.com', 3, '2023-01-09 06:50:05', 1),
(3, '::1', 'rizalmustaqim150@gmail.com', 3, '2023-01-09 06:56:34', 1),
(4, '::1', 'rizalmustaqim150@gmail.com', 3, '2023-01-09 07:04:34', 1),
(5, '::1', 'rizalmustaqim150@gmail.com', 3, '2023-01-09 07:04:48', 1),
(6, '::1', 'asasas@gmail.com', 4, '2023-01-09 07:10:23', 1),
(7, '::1', 'rizalmustaqim150@gmail.com', 3, '2023-01-09 07:49:50', 1),
(8, '::1', 'asasas@gmail.com', 4, '2023-01-09 07:50:31', 1),
(9, '::1', 'rizalmustaqim150@gmail.com', 3, '2023-01-09 07:55:29', 1),
(10, '::1', 'asasas@gmail.com', 4, '2023-01-09 07:59:13', 1),
(11, '::1', 'rizalmustaqim150@gmail.com', 3, '2023-01-09 07:59:22', 1),
(12, '::1', 'rizalmustaqim150@gmail.com', 3, '2023-01-09 08:01:32', 1),
(13, '::1', 'asasas@gmail.com', 4, '2023-01-09 08:04:16', 1),
(14, '::1', 'rizalmustaqim150@gmail.com', 3, '2023-01-09 08:04:24', 1),
(15, '::1', 'rizalmustaqim150@gmail.com', 3, '2023-01-09 08:05:06', 1),
(16, '::1', 'rizalmustaqim150@gmail.com', 3, '2023-01-09 08:06:16', 1),
(17, '::1', 'asasas@gmail.com', 4, '2023-01-09 08:06:35', 1),
(18, '::1', 'rizalmustaqim150@gmail.com', 3, '2023-01-09 08:07:04', 1),
(19, '::1', 'rizalmustaqim150@gmail.com', 3, '2023-01-09 08:09:46', 1),
(20, '::1', 'rizalmustaqim150@gmail.com', 3, '2023-01-09 08:12:00', 1),
(21, '::1', 'asasas@gmail.com', 4, '2023-01-09 08:12:27', 1),
(22, '::1', 'rizalmustaqim150@gmail.com', 3, '2023-01-09 08:21:47', 1),
(23, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 08:22:56', 1),
(24, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 08:30:07', 1),
(25, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 08:31:05', 1),
(26, '::1', 'asasas@gmail.com', 4, '2023-01-09 08:31:27', 1),
(27, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 08:41:58', 1),
(28, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 08:42:30', 1),
(29, '::1', 'asasas@gmail.com', 4, '2023-01-09 08:42:44', 1),
(30, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 08:43:13', 1),
(31, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 08:44:22', 1),
(32, '::1', 'asasas@gmail.com', 4, '2023-01-09 08:44:34', 1),
(33, '::1', 'asasas@gmail.com', 4, '2023-01-09 08:45:36', 1),
(34, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 08:45:46', 1),
(35, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 08:47:03', 1),
(36, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:41:59', 1),
(37, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:49:25', 1),
(38, '::1', 'asasas@gmail.com', 4, '2023-01-09 21:49:37', 1),
(39, '::1', 'asasas@gmail.com', 4, '2023-01-09 21:53:01', 1),
(40, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:53:10', 1),
(41, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:53:42', 1),
(42, '::1', 'asasas@gmail.com', 4, '2023-01-09 21:53:55', 1),
(43, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:54:05', 1),
(44, '::1', 'asasas@gmail.com', 4, '2023-01-09 21:54:22', 1),
(45, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:54:35', 1),
(46, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:54:55', 1),
(47, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:55:03', 1),
(48, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:55:52', 1),
(49, '::1', 'asasas@gmail.com', 4, '2023-01-09 21:56:00', 1),
(50, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:56:41', 1),
(51, '::1', 'asasas@gmail.com', 4, '2023-01-09 21:56:58', 1),
(52, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:57:22', 1),
(53, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:57:44', 1),
(54, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:58:19', 1),
(55, '::1', 'asasas@gmail.com', 4, '2023-01-09 21:58:40', 1),
(56, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:58:56', 1),
(57, '::1', 'asasas@gmail.com', 4, '2023-01-09 21:59:10', 1),
(58, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:59:16', 1),
(59, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 21:59:30', 1),
(60, '::1', 'asasas@gmail.com', 4, '2023-01-09 21:59:38', 1),
(61, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:00:15', 1),
(62, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:00:30', 1),
(63, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:00:56', 1),
(64, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:01:05', 1),
(65, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:01:12', 1),
(66, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:01:32', 1),
(67, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:01:43', 1),
(68, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:02:13', 1),
(69, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:02:24', 1),
(70, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:02:31', 1),
(71, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:03:19', 1),
(72, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:03:25', 1),
(73, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:03:32', 1),
(74, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:03:40', 1),
(75, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:03:47', 1),
(76, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:03:53', 1),
(77, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:03:59', 1),
(78, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:10:48', 1),
(79, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:10:56', 1),
(80, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:11:03', 1),
(81, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:11:12', 1),
(82, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:11:18', 1),
(83, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:11:22', 1),
(84, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:11:28', 1),
(85, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:14:40', 1),
(86, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:14:46', 1),
(87, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:14:52', 1),
(88, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:14:58', 1),
(89, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:15:04', 1),
(90, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:17:34', 1),
(91, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:17:40', 1),
(92, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:17:51', 1),
(93, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:18:14', 1),
(94, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:18:20', 1),
(95, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:18:26', 1),
(96, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:18:36', 1),
(97, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:19:06', 1),
(98, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:19:16', 1),
(99, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:19:29', 1),
(100, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:19:48', 1),
(101, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:20:47', 1),
(102, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:20:53', 1),
(103, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:21:02', 1),
(104, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:21:07', 1),
(105, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:21:13', 1),
(106, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:22:53', 1),
(107, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:24:55', 1),
(108, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:25:07', 1),
(109, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:25:15', 1),
(110, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:26:14', 1),
(111, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:29:31', 1),
(112, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:29:51', 1),
(113, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:30:00', 1),
(114, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:30:13', 1),
(115, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:33:12', 1),
(116, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:33:21', 1),
(117, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:33:36', 1),
(118, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:33:46', 1),
(119, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:36:39', 1),
(120, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:39:16', 1),
(121, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:39:30', 1),
(122, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:41:32', 1),
(123, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:41:56', 1),
(124, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:42:02', 1),
(125, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:43:32', 1),
(126, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:43:54', 1),
(127, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:46:49', 1),
(128, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:47:00', 1),
(129, '::1', 'asasas@gmail.com', 4, '2023-01-09 22:48:19', 1),
(130, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:49:41', 1),
(131, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 22:52:00', 1),
(132, '::1', 'asasas@gmail.com', 4, '2023-01-09 23:41:07', 1),
(133, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-09 23:41:35', 1),
(134, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-10 00:03:30', 1),
(135, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-10 00:18:47', 1),
(136, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-10 00:28:48', 1),
(137, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-10 01:05:02', 1),
(138, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-10 08:18:43', 1),
(139, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-10 21:36:09', 1),
(140, '::1', 'asasas@gmail.com', 4, '2023-01-10 21:36:22', 1),
(141, '::1', 'asasas@gmail.com', 4, '2023-01-10 21:37:00', 1),
(142, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-10 21:37:12', 1),
(143, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-10 21:49:20', 1),
(144, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-10 21:51:26', 1),
(145, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-13 07:12:47', 1),
(146, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-14 08:09:21', 1),
(147, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-14 21:10:06', 1),
(148, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-14 21:40:20', 1),
(149, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-15 08:08:22', 1),
(150, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-15 23:47:49', 1),
(151, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-24 07:36:48', 1),
(152, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-25 06:15:02', 1),
(153, '::1', 'asasas@gmail.com', 4, '2023-01-25 06:15:50', 1),
(154, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-25 06:16:49', 1),
(155, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-25 10:02:02', 1),
(156, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-26 04:20:16', 1),
(157, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-26 22:24:02', 1),
(158, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-28 21:56:52', 1),
(159, '::1', 'asasas@gmail.com', 4, '2023-01-29 00:48:23', 1),
(160, '::1', 'asasas@gmail.com', 4, '2023-01-29 07:06:49', 1),
(161, '::1', 'asasas@gmail.com', 4, '2023-01-30 01:02:25', 1),
(162, '::1', 'asasas@gmail.com', 4, '2023-01-30 06:52:17', 1),
(163, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-30 07:49:04', 1),
(164, '::1', 'asasas@gmail.com', 4, '2023-01-30 07:49:55', 1),
(165, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-30 08:04:33', 1),
(166, '::1', 'asasas@gmail.com', 4, '2023-01-30 08:04:59', 1),
(167, '::1', 'novariza', NULL, '2023-01-30 08:15:39', 0),
(168, '::1', 'fakhrizalmustaqim', NULL, '2023-01-30 08:15:46', 0),
(169, '::1', 'ijal@gmail.com', 7, '2023-01-31 01:19:46', 1),
(170, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-31 01:20:32', 1),
(171, '::1', 'ijal@gmail.com', 7, '2023-01-31 01:24:22', 1),
(172, '::1', 'ijal@gmail.com', 7, '2023-01-31 01:54:10', 1),
(173, '::1', 'ijal@gmail.com', 7, '2023-01-31 01:59:30', 1),
(174, '::1', 'asasas@gmail.com', 4, '2023-01-31 05:35:40', 1),
(175, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-31 06:06:21', 1),
(176, '::1', 'asasas@gmail.com', 4, '2023-01-31 06:08:31', 1),
(177, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-01-31 06:08:49', 1),
(178, '::1', 'ijal@gmail.com', 7, '2023-01-31 06:09:32', 1),
(179, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-02 00:35:13', 1),
(180, '::1', 'ijal@gmail.com', 7, '2023-02-02 00:51:48', 1),
(181, '::1', 'asasas@gmail.com', 4, '2023-02-02 07:30:56', 1),
(182, '::1', 'asasas@gmail.com', 4, '2023-02-03 07:29:14', 1),
(183, '::1', 'asasas@gmail.com', 4, '2023-02-03 07:48:01', 1),
(184, '::1', 'asasas@gmail.com', 4, '2023-02-03 07:53:33', 1),
(185, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-03 08:05:31', 1),
(186, '::1', 'ijal@gmail.com', 7, '2023-02-03 08:07:36', 1),
(187, '::1', 'ijal@gmail.com', 7, '2023-02-04 00:34:18', 1),
(188, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-04 00:53:56', 1),
(189, '::1', 'ijal@gmail.com', 7, '2023-02-04 01:03:30', 1),
(190, '::1', 'ijal@gmail.com', 7, '2023-02-04 05:32:17', 1),
(191, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-04 05:45:25', 1),
(192, '::1', 'ijal@gmail.com', 7, '2023-02-04 05:48:52', 1),
(193, '::1', 'asasas@gmail.com', 4, '2023-02-05 01:28:33', 1),
(194, '::1', 'ijal@gmail.com', 7, '2023-02-05 06:45:35', 1),
(195, '::1', 'asasas@gmail.com', 4, '2023-02-05 20:24:25', 1),
(196, '::1', 'ijal@gmail.com', 7, '2023-02-05 21:20:26', 1),
(197, '::1', 'ijal@gmail.com', 7, '2023-02-05 21:25:30', 1),
(198, '::1', 'ijal@gmail.com', 7, '2023-02-05 21:25:45', 1),
(199, '::1', 'asasas@gmail.com', 4, '2023-02-06 02:02:32', 1),
(200, '::1', 'Bayu@gmail.com', 9, '2023-02-06 02:05:13', 1),
(201, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-06 02:08:40', 1),
(202, '::1', 'ijal@gmail.com', 7, '2023-02-07 22:33:37', 1),
(203, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-08 00:50:56', 1),
(204, '::1', 'ijal@gmail.com', 7, '2023-02-08 00:54:32', 1),
(205, '::1', 'ijal@gmail.com', 7, '2023-02-08 07:58:46', 1),
(206, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-14 00:17:41', 1),
(207, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-23 23:32:23', 1),
(208, '::1', 'ijal@gmail.com', 7, '2023-02-23 23:55:57', 1),
(209, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-24 00:07:02', 1),
(210, '::1', 'ijal', NULL, '2023-02-24 00:52:56', 0),
(211, '::1', 'ijal@gmail.com', 7, '2023-02-24 00:53:00', 1),
(212, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-24 00:54:34', 1),
(213, '::1', 'ijal@gmail.com', 7, '2023-02-24 00:57:27', 1),
(214, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-24 01:00:58', 1),
(215, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-24 01:36:31', 1),
(216, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-24 01:52:11', 1),
(217, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-24 01:57:17', 1),
(218, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-24 02:04:57', 1),
(219, '::1', 'asasas@gmail.com', 4, '2023-02-24 02:05:12', 1),
(220, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-24 02:05:46', 1),
(221, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-24 04:06:41', 1),
(222, '::1', 'asasas@gmail.com', 4, '2023-02-24 04:28:14', 1),
(223, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-24 04:28:37', 1),
(224, '::1', 'ijal@gmail.com', 7, '2023-02-24 04:29:08', 1),
(225, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-24 04:29:22', 1),
(226, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-02-24 06:34:44', 1),
(227, '::1', 'ijal@gmail.com', 7, '2023-02-24 07:09:23', 1),
(228, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-03 09:05:59', 1),
(229, '::1', 'ijal@gmail.com', 7, '2023-03-03 09:07:42', 1),
(230, '::1', 'ijal@gmail.com', 7, '2023-03-03 09:18:03', 1),
(231, '::1', 'asasas@gmail.com', 4, '2023-03-04 07:03:40', 1),
(232, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-04 07:53:18', 1),
(233, '::1', 'asasas@gmail.com', 4, '2023-03-04 08:03:17', 1),
(234, '::1', 'ijal@gmail.com', 7, '2023-03-04 08:16:27', 1),
(235, '::1', 'ijal@gmail.com', 7, '2023-03-04 08:19:52', 1),
(236, '::1', 'asasas@gmail.com', 4, '2023-03-04 09:53:05', 1),
(237, '::1', 'asasas@gmail.com', 4, '2023-03-04 22:59:30', 1),
(238, '::1', 'ijal@gmail.com', 7, '2023-03-04 23:23:56', 1),
(239, '::1', 'asasas@gmail.com', 4, '2023-03-04 23:28:25', 1),
(240, '::1', 'ijal@gmail.com', 7, '2023-03-04 23:28:37', 1),
(241, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-05 01:59:21', 1),
(242, '::1', 'ijal@gmail.com', 7, '2023-03-05 02:00:15', 1),
(243, '::1', 'ijal@gmail.com', 7, '2023-03-05 06:55:38', 1),
(244, '::1', 'asasas@gmail.com', 4, '2023-03-06 08:07:38', 1),
(245, '::1', 'fakhrizalmustaqim', NULL, '2023-03-12 23:51:30', 0),
(246, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-12 23:51:37', 1),
(247, '::1', 'asasas@gmail.com', 4, '2023-03-13 04:46:22', 1),
(248, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-13 05:14:36', 1),
(249, '::1', 'asasas@gmail.com', 4, '2023-03-13 06:32:36', 1),
(250, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-14 01:27:17', 1),
(251, '::1', 'asasas@gmail.com', 4, '2023-03-14 07:17:59', 1),
(252, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-14 07:18:07', 1),
(253, '::1', 'asasas@gmail.com', 4, '2023-03-14 07:25:53', 1),
(254, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-14 07:30:07', 1),
(255, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-15 09:57:29', 1),
(256, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-15 13:37:50', 1),
(257, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-15 23:28:46', 1),
(258, '::1', 'asasas@gmail.com', 4, '2023-03-15 23:30:32', 1),
(259, '::1', 'asasas@gmail.com', 4, '2023-03-16 05:28:09', 1),
(260, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-16 07:54:28', 1),
(261, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-18 07:36:39', 1),
(262, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-24 07:31:13', 1),
(263, '::1', 'fakhrizaluciha@gmail.com', 10, '2023-03-24 08:30:06', 1),
(264, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-24 08:30:16', 1),
(265, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-24 08:32:16', 1),
(266, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-24 08:34:22', 1),
(267, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-25 01:13:11', 1),
(268, '::1', 'asasas@gmail.com', 4, '2023-03-25 01:19:49', 1),
(269, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-25 01:20:17', 1),
(270, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-25 01:35:09', 1),
(271, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-25 01:37:10', 1),
(272, '::1', 'asasas@gmail.com', 4, '2023-03-25 01:38:22', 1),
(273, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-25 01:40:02', 1),
(274, '::1', 'fakhrizaluciha@gmail.com', 12, '2023-03-30 07:25:58', 1),
(275, '::1', 'fakhrizaluciha@gmail.com', 12, '2023-03-30 11:52:50', 1),
(276, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-30 12:14:00', 1),
(277, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-30 22:53:53', 1),
(278, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-03-31 01:04:29', 1),
(279, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-04-02 02:00:30', 1),
(280, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-04-03 00:49:42', 1),
(281, '::1', 'asasas@gmail.com', 4, '2023-04-07 06:34:23', 1),
(282, '::1', 'asasas@gmail.com', 4, '2023-04-07 07:02:15', 1),
(283, '::1', 'asasas@gmail.com', 4, '2023-04-09 23:21:16', 1),
(284, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-04-12 03:35:17', 1),
(285, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-04-13 04:08:49', 1),
(286, '::1', 'asasas@gmail.com', 4, '2023-04-13 04:46:50', 1),
(287, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-04-13 04:47:27', 1),
(288, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-04-17 09:03:15', 1),
(289, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-04-17 09:04:19', 1),
(290, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-03 09:42:33', 1),
(291, '::1', 'asasas@gmail.com', 4, '2023-05-11 21:07:44', 1),
(292, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-11 21:11:26', 1),
(293, '::1', 'asasas@gmail.com', 4, '2023-05-11 21:13:07', 1),
(294, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-11 21:22:22', 1),
(295, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-15 03:40:49', 1),
(296, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-15 07:04:36', 1),
(297, '::1', 'asasas@gmail.com', 4, '2023-05-18 01:58:00', 1),
(298, '::1', 'asasas@gmail.com', 4, '2023-05-18 02:00:34', 1),
(299, '::1', 'asasas@gmail.com', 4, '2023-05-18 07:59:56', 1),
(300, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-19 01:24:36', 1),
(301, '::1', 'fakhrizalmustaqim', NULL, '2023-05-19 02:34:11', 0),
(302, '::1', 'fakhrizalmustaqim', NULL, '2023-05-19 02:34:23', 0),
(303, '::1', 'fakhrizalmustaqim1', NULL, '2023-05-19 02:34:30', 0),
(304, '::1', 'fakhrizalmustaqim', NULL, '2023-05-19 02:35:29', 0),
(305, '::1', 'fakhrizalmustaqim1', NULL, '2023-05-19 02:35:49', 0),
(306, '::1', 'fakhrizalmustaqim', NULL, '2023-05-19 02:36:05', 0),
(307, '::1', 'fakhrizalmustaqim1', NULL, '2023-05-19 02:36:10', 0),
(308, '::1', 'fakhrizalmustaqim', NULL, '2023-05-19 02:36:25', 0),
(309, '::1', 'fakhrizalmustaqim1', NULL, '2023-05-19 02:36:32', 0),
(310, '::1', 'fakhrizalmustaqim', NULL, '2023-05-19 02:37:00', 0),
(311, '::1', 'fakhrizalmustaqim1', NULL, '2023-05-19 02:37:06', 0),
(312, '::1', 'fakhrizalmustaqim', NULL, '2023-05-19 02:38:15', 0),
(313, '::1', 'fakhrizalmustaqim', NULL, '2023-05-19 02:38:20', 0),
(314, '::1', 'fakhrizalmustaqim1', NULL, '2023-05-19 02:38:41', 0),
(315, '::1', 'fakhrizalmustaqim', NULL, '2023-05-19 02:38:45', 0),
(316, '::1', 'fakhrizalmustaqim', NULL, '2023-05-19 02:39:12', 0),
(317, '::1', 'fakhrizalmustaqim1', NULL, '2023-05-19 02:39:17', 0),
(318, '::1', 'fakhrizalmustaqim1', NULL, '2023-05-19 02:43:46', 0),
(319, '::1', 'fakhrizalmustaqim', NULL, '2023-05-19 02:44:06', 0),
(320, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-19 02:48:02', 1),
(321, '::1', 'fakhrizalmustaqim1', NULL, '2023-05-19 07:25:22', 0),
(322, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-19 07:25:31', 1),
(323, '::1', 'fakhrizalmustaqim', NULL, '2023-05-19 08:00:51', 0),
(324, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-19 08:00:56', 1),
(325, '::1', 'asasas@gmail.com', 4, '2023-05-21 23:24:49', 1),
(326, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-21 23:34:46', 1),
(327, '::1', 'asasas@gmail.com', 4, '2023-05-21 23:35:44', 1),
(328, '::1', 'asasas@gmail.com', 4, '2023-05-23 07:35:27', 1),
(329, '::1', 'asasas@gmail.com', 4, '2023-05-23 08:25:05', 1),
(330, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-24 20:19:48', 1),
(331, '::1', 'asasas@gmail.com', 4, '2023-05-24 20:22:19', 1),
(332, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-24 20:22:42', 1),
(333, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-24 20:24:13', 1),
(334, '::1', 'asasas@gmail.com', 4, '2023-05-24 20:24:21', 1),
(335, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-24 20:45:53', 1),
(336, '::1', 'asasas@gmail.com', 4, '2023-05-24 22:17:00', 1),
(337, '::1', 'fakhrizaluciha@gmail.com', 12, '2023-05-24 22:30:23', 1),
(338, '::1', 'asasas@gmail.com', 4, '2023-05-24 22:30:59', 1),
(339, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-25 00:02:09', 1),
(340, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-25 05:00:33', 1),
(341, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-25 08:24:02', 1),
(342, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-25 09:42:49', 1),
(343, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-26 07:40:51', 1),
(344, '::1', 'asasas@gmail.com', 4, '2023-05-26 07:44:21', 1),
(345, '::1', 'asasas@gmail.com', 4, '2023-05-27 00:45:41', 1),
(346, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-27 01:00:30', 1),
(347, '::1', 'asasas@gmail.com', 4, '2023-05-27 02:24:39', 1),
(348, '::1', 'asasas@gmail.com', 4, '2023-05-27 08:32:43', 1),
(349, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-27 08:45:39', 1),
(350, '::1', 'asasas@gmail.com', 4, '2023-05-27 08:46:31', 1),
(351, '::1', 'asasas@gmail.com', 4, '2023-05-28 07:13:19', 1),
(352, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-28 07:33:17', 1),
(353, '::1', 'asasas@gmail.com', 4, '2023-05-28 07:33:40', 1),
(354, '::1', 'asasas@gmail.com', 4, '2023-05-29 07:37:52', 1),
(355, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-05-30 07:46:08', 1),
(356, '::1', 'asasas@gmail.com', 4, '2023-06-01 03:08:50', 1),
(357, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-01 03:09:16', 1),
(358, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-01 03:15:38', 1),
(359, '::1', 'asasas@gmail.com', 4, '2023-06-01 05:06:14', 1),
(360, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-01 07:04:29', 1),
(361, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-03 00:19:09', 1),
(362, '::1', 'asasas@gmail.com', 4, '2023-06-07 22:16:24', 1),
(363, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-07 22:25:50', 1),
(364, '::1', 'asasas@gmail.com', 4, '2023-06-07 23:01:20', 1),
(365, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-07 23:31:45', 1),
(366, '::1', 'asasas@gmail.com', 4, '2023-06-07 23:58:56', 1),
(367, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-08 00:05:35', 1),
(368, '::1', 'asasas@gmail.com', 4, '2023-06-08 00:32:46', 1),
(369, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-08 00:33:46', 1),
(370, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-08 22:58:44', 1),
(371, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-09 04:27:28', 1),
(372, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-09 07:47:34', 1),
(373, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-09 08:33:44', 1),
(374, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-09 08:40:16', 1),
(375, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-09 09:07:26', 1),
(376, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-10 22:15:38', 1),
(377, '::1', 'fakhrizalmustaqimbaru', NULL, '2023-06-10 22:34:12', 0),
(378, '::1', 'fakhrizalmustaqim', NULL, '2023-06-10 22:34:18', 0),
(379, '::1', 'fakhrizalmustaqimbaru', NULL, '2023-06-10 22:34:50', 0),
(380, '::1', 'fakhrizalmustaqimbaru', NULL, '2023-06-10 22:35:01', 0),
(381, '::1', 'fakhrizalmustaqimbaru', NULL, '2023-06-10 22:35:48', 0),
(382, '::1', 'fakhrizalmustaqimbaru', NULL, '2023-06-10 22:36:27', 0),
(383, '::1', 'fakhrizalmustaqimbaru', NULL, '2023-06-10 22:37:17', 0),
(384, '::1', 'asasas@gmail.com', 4, '2023-06-10 22:37:21', 1),
(385, '::1', 'fakhrizalmustaqimbaru', NULL, '2023-06-10 22:37:50', 0),
(386, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-10 22:39:41', 1),
(387, '::1', 'fakhrizalmustaqim', NULL, '2023-06-10 22:40:49', 0),
(388, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-10 22:40:53', 1),
(389, '::1', 'fakhrizalmustaqim', NULL, '2023-06-10 22:47:08', 0),
(390, '::1', 'fakhrizalmustaqim', NULL, '2023-06-10 22:47:21', 0),
(391, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-10 22:48:41', 1),
(392, '::1', 'fakhrizalmustaqim', NULL, '2023-06-10 22:49:19', 0),
(393, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-10 22:50:20', 1),
(394, '::1', 'asasas@gmail.com', 4, '2023-06-10 22:53:38', 1),
(395, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-10 23:52:33', 1),
(396, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-11 07:43:41', 1),
(397, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-11 07:58:25', 1),
(398, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-12 00:53:25', 1),
(399, '::1', 'asasas@gmail.com', 4, '2023-06-12 00:53:56', 1),
(400, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-12 07:35:41', 1),
(401, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-12 08:17:29', 1),
(402, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-13 07:33:54', 1),
(403, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-13 08:54:25', 1),
(404, '::1', 'fakhrizalmustaqim', NULL, '2023-06-13 09:03:48', 0),
(405, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-13 09:16:58', 1),
(406, '::1', 'coba@gmail.com', 28, '2023-06-13 09:18:37', 1),
(407, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-13 09:18:54', 1),
(408, '::1', 'coba@gmail.com', 28, '2023-06-13 09:34:53', 1),
(409, '::1', 'asasas@gmail.com', 4, '2023-06-13 09:36:38', 1),
(410, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 05:33:08', 1),
(411, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 05:34:46', 1),
(412, '::1', 'asasas@gmail.com', 4, '2023-06-14 07:31:01', 1),
(413, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 21:44:00', 1),
(414, '::1', 'asasas@gmail.com', 4, '2023-06-14 21:53:40', 1),
(415, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 22:55:15', 1),
(416, '::1', 'asasas@gmail.com', 4, '2023-06-14 22:56:43', 1),
(417, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 22:57:12', 1),
(418, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 23:00:54', 1),
(419, '::1', 'asasas@gmail.com', 4, '2023-06-14 23:01:37', 1),
(420, '::1', 'asasas@gmail.com', 4, '2023-06-14 23:01:50', 1),
(421, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 23:02:20', 1),
(422, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 23:03:41', 1),
(423, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 23:04:29', 1),
(424, '::1', 'asasas@gmail.com', 4, '2023-06-14 23:05:43', 1),
(425, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 23:05:55', 1),
(426, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 23:06:29', 1),
(427, '::1', 'fakhrizalmustaqim', NULL, '2023-06-14 23:07:03', 0),
(428, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 23:07:07', 1),
(429, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 23:07:45', 1),
(430, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 23:10:04', 1),
(431, '::1', 'asasas@gmail.com', 4, '2023-06-14 23:11:56', 1),
(432, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 23:12:23', 1),
(433, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 23:13:43', 1),
(434, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 23:17:23', 1),
(435, '::1', 'asasas@gmail.com', 4, '2023-06-14 23:18:58', 1),
(436, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-14 23:20:10', 1),
(437, '::1', 'asasas@gmail.com', 4, '2023-06-14 23:21:16', 1),
(438, '::1', 'asasas@gmail.com', 4, '2023-06-15 04:02:26', 1),
(439, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-15 05:26:16', 1),
(440, '::1', 'novariza', NULL, '2023-06-15 05:27:43', 0),
(441, '::1', 'asasas@gmail.com', 4, '2023-06-15 05:27:48', 1),
(442, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-15 10:51:38', 1),
(443, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-16 07:20:10', 1),
(444, '::1', 'asasas@gmail.com', 4, '2023-06-16 08:56:09', 1),
(445, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-16 08:58:00', 1),
(446, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-16 10:31:07', 1),
(447, '::1', 'asasas@gmail.com', 4, '2023-06-16 10:39:12', 1),
(448, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-16 10:40:39', 1),
(449, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-16 22:56:18', 1),
(450, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-16 22:57:34', 1),
(451, '::1', 'asasas@gmail.com', 4, '2023-06-16 23:18:13', 1),
(452, '::1', 'asasas@gmail.com', 4, '2023-06-16 23:45:06', 1),
(453, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-16 23:55:43', 1),
(454, '::1', 'asasas@gmail.com', 4, '2023-06-17 00:00:35', 1),
(455, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 00:01:43', 1),
(456, '::1', 'asasas@gmail.com', 4, '2023-06-17 00:02:55', 1),
(457, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 00:10:44', 1),
(458, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 00:23:50', 1),
(459, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 00:24:12', 1),
(460, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-17 00:25:34', 1),
(461, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 00:25:47', 1),
(462, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-17 00:29:36', 1),
(463, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 00:40:23', 1),
(464, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-17 00:40:51', 1),
(465, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 00:41:42', 1),
(466, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-17 00:43:29', 1),
(467, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-17 00:45:58', 1),
(468, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 01:00:18', 1),
(469, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 01:01:08', 1),
(470, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 01:02:45', 1),
(471, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-17 01:53:30', 1),
(472, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 01:58:09', 1),
(473, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-17 02:13:57', 1),
(474, '::1', 'Admin', NULL, '2023-06-17 02:14:45', 0),
(475, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 02:14:49', 1),
(476, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-17 02:16:07', 1),
(477, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 02:16:23', 1),
(478, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 04:53:04', 1),
(479, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-17 05:13:33', 1),
(480, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 05:22:48', 1),
(481, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-17 05:25:00', 1),
(482, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 07:00:58', 1),
(483, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-17 07:51:42', 1),
(484, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-17 23:02:33', 1),
(485, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-17 23:05:17', 1),
(486, '::1', 'fakhrizalmustaqim', NULL, '2023-06-18 08:31:20', 0),
(487, '::1', 'fakhrizaluciha@gmail.com', NULL, '2023-06-18 08:31:39', 0),
(488, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-18 08:31:48', 1),
(489, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-18 08:32:06', 1),
(490, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-18 17:23:20', 1),
(491, '::1', 'Fakhrizal ', NULL, '2023-06-18 17:23:40', 0),
(492, '::1', 'Fakhrizal ', NULL, '2023-06-18 17:23:46', 0),
(493, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-18 17:23:53', 1),
(494, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-18 17:54:36', 1),
(495, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-18 23:27:31', 1),
(496, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 00:40:14', 1),
(497, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 01:13:12', 1),
(498, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 02:19:14', 1),
(499, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 02:25:58', 1),
(500, '::1', 'Fakhrizal ', NULL, '2023-06-19 02:27:34', 0),
(501, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 02:29:10', 1),
(502, '::1', 'saa', NULL, '2023-06-19 02:36:52', 0),
(503, '::1', 'fakhrizalmustaqim', NULL, '2023-06-19 02:37:26', 0),
(504, '::1', 'sasa', NULL, '2023-06-19 02:38:31', 0),
(505, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 02:38:37', 1),
(506, '::1', 'fakhrizalmustaqim', NULL, '2023-06-19 02:40:41', 0),
(507, '::1', 'sass', NULL, '2023-06-19 02:44:07', 0),
(508, '::1', 'Fakhrizal ', NULL, '2023-06-19 02:46:39', 0),
(509, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-19 02:46:46', 1),
(510, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 02:49:43', 1),
(511, '::1', 'fakhrizalmustaqim', NULL, '2023-06-19 03:05:14', 0),
(512, '::1', 'Admin', NULL, '2023-06-19 03:19:53', 0),
(513, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 03:20:00', 1),
(514, '::1', 'fakhrizalmustaqim', NULL, '2023-06-19 03:25:30', 0),
(515, '::1', 'Admin', NULL, '2023-06-19 03:26:03', 0),
(516, '::1', 'Fakhrizal ', NULL, '2023-06-19 03:26:09', 0),
(517, '::1', 'fakhrizalmustaqim', NULL, '2023-06-19 03:26:14', 0),
(518, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 03:26:30', 1),
(519, '::1', 'Admin', NULL, '2023-06-19 03:29:45', 0),
(520, '::1', 'fakhrizalmustaqim', NULL, '2023-06-19 06:44:52', 0),
(521, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-19 06:44:59', 1),
(522, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 06:45:08', 1),
(523, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-19 06:56:29', 1),
(524, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 06:57:21', 1),
(525, '::1', 'Admin', NULL, '2023-06-19 11:17:22', 0),
(526, '::1', 'Admin', NULL, '2023-06-19 11:17:53', 0),
(527, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 11:18:01', 1),
(528, '::1', 'novariza', NULL, '2023-06-19 11:18:20', 0),
(529, '::1', 'Admin', NULL, '2023-06-19 17:21:57', 0),
(530, '::1', 'Fakhrizal', NULL, '2023-06-19 17:22:07', 0),
(531, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 17:22:11', 1),
(532, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-19 17:22:29', 1),
(533, '::1', 'Admin', NULL, '2023-06-19 20:12:18', 0),
(534, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 21:37:51', 1),
(535, '::1', 'coba@gmail.com', 31, '2023-06-19 21:58:16', 1),
(536, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 22:00:31', 1),
(537, '::1', 'Fakhrizal ', NULL, '2023-06-19 22:10:15', 0),
(538, '::1', 'Fakhrizal ', NULL, '2023-06-19 22:10:24', 0),
(539, '::1', 'Mustaqim', NULL, '2023-06-19 22:10:38', 0),
(540, '::1', 'coba2@gmail.com', 32, '2023-06-19 22:11:11', 1),
(541, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 23:03:05', 1),
(542, '::1', 'Fakhrizal ', NULL, '2023-06-19 23:03:18', 0),
(543, '::1', 'Fakhrizal ', NULL, '2023-06-19 23:03:24', 0),
(544, '::1', 'Fakhrizal ', NULL, '2023-06-19 23:03:30', 0),
(545, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-19 23:03:38', 1),
(546, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-19 23:05:09', 1),
(547, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 23:05:15', 1),
(548, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-19 23:54:49', 1),
(549, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-19 23:55:01', 1),
(550, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-20 00:02:27', 1),
(551, '::1', 'Fakhrizal ', NULL, '2023-06-20 09:38:11', 0),
(552, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-20 09:38:16', 1),
(553, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-06-28 23:29:50', 1),
(554, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-06-29 06:26:42', 1),
(555, '::1', 'fakhrizalmustaqim', NULL, '2023-07-05 08:59:10', 0),
(556, '::1', 'fakhrizalmustaqim', NULL, '2023-07-05 08:59:19', 0),
(557, '::1', 'fakhrizalmustaqim', NULL, '2023-07-05 09:00:46', 0),
(558, '::1', 'Admin', NULL, '2023-07-05 23:44:49', 0),
(559, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-07-05 23:52:31', 1),
(560, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-05 23:54:52', 1),
(561, '::1', 'Mustaqim', NULL, '2023-07-05 23:58:52', 0),
(562, '::1', 'Mustaqim1', NULL, '2023-07-05 23:59:00', 0),
(563, '::1', 'mustaqim@gmail.com', 33, '2023-07-05 23:59:10', 1),
(564, '::1', 'Fakhrizal ', NULL, '2023-07-05 23:59:49', 0),
(565, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-07-05 23:59:59', 1),
(566, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-07-06 00:01:06', 1),
(567, '::1', 'Fakhrizal ', NULL, '2023-07-06 00:01:13', 0),
(568, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-07-06 00:01:46', 1),
(569, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-07-06 00:03:21', 1),
(570, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-06 00:03:42', 1),
(571, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-07-06 00:03:55', 1),
(572, '::1', 'Fakhrizal ', NULL, '2023-07-06 02:59:45', 0),
(573, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-07-06 02:59:50', 1),
(574, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-06 03:01:35', 1),
(575, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-11 07:31:28', 1),
(576, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-18 00:43:50', 1),
(577, '::1', 'aziz@gmail.com', 34, '2023-07-19 07:38:35', 1),
(578, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-19 07:49:46', 1),
(579, '::1', 'aziz@gmail.com', 34, '2023-07-19 07:53:14', 1),
(580, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-19 07:53:48', 1),
(581, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-21 04:53:17', 1),
(582, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-07-21 04:55:47', 1),
(583, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-21 04:57:55', 1),
(584, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-07-21 08:07:31', 1),
(585, '::1', 'Mustaqim', NULL, '2023-07-21 22:18:49', 0),
(586, '::1', 'Mustaqim123@gmail.com', NULL, '2023-07-21 22:19:14', 0),
(587, '::1', 'fakhrizaluciha@gmail.com', 29, '2023-07-21 22:21:00', 1),
(588, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-21 22:29:06', 1),
(589, '::1', 'fakhrizaluciha@gmail.com', NULL, '2023-07-21 22:43:38', 0),
(590, '::1', 'fakhrizaluciha@gmail.com', NULL, '2023-07-21 22:43:50', 0),
(591, '::1', 'Fakhrizal ', NULL, '2023-07-21 22:43:57', 0),
(592, '::1', 'aziz@gmail.com', 34, '2023-07-21 22:44:06', 1),
(593, '::1', 'aziz@gmail.com', 34, '2023-07-21 22:51:59', 1),
(594, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-21 22:52:33', 1),
(595, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-22 08:40:28', 1),
(596, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-22 22:45:29', 1),
(597, '::1', 'aziz@gmail.com', 34, '2023-07-22 23:37:18', 1),
(598, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-22 23:48:48', 1),
(599, '::1', 'aziz@gmail.com', 34, '2023-07-23 01:56:15', 1),
(600, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-23 02:36:13', 1),
(601, '::1', 'aziz@gmail.com', 34, '2023-07-23 02:46:38', 1),
(602, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-23 03:37:19', 1),
(603, '::1', 'aziz@gmail.com', 34, '2023-07-23 03:39:24', 1),
(604, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-23 04:05:44', 1),
(605, '::1', 'Aziz', NULL, '2023-07-23 04:06:01', 0),
(606, '::1', 'aziz@gmail.com', 34, '2023-07-23 04:06:08', 1),
(607, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-23 04:07:09', 1),
(608, '::1', 'aziz@gmail.com', 34, '2023-07-23 04:08:32', 1),
(609, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-23 04:09:46', 1),
(610, '::1', 'aziz@gmail.com', 34, '2023-07-23 04:11:49', 1),
(611, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-23 05:50:36', 1),
(612, '::1', 'aziz@gmail.com', 34, '2023-07-23 05:53:20', 1),
(613, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-23 09:51:41', 1),
(614, '::1', 'aziz@gmail.com', 34, '2023-07-23 10:38:21', 1),
(615, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-23 10:38:50', 1),
(616, '::1', 'aziz@gmail.com', 34, '2023-07-23 10:40:11', 1),
(617, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-23 10:41:28', 1),
(618, '::1', 'aziz@gmail.com', 34, '2023-07-23 21:33:23', 1),
(619, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-23 21:36:58', 1),
(620, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-23 21:58:15', 1),
(621, '::1', 'fakhrizaluciha@gmail.com', 36, '2023-07-23 21:59:28', 1),
(622, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-23 22:00:02', 1),
(623, '::1', 'fakhrizaluciha@gmail.com', 36, '2023-07-23 22:11:31', 1),
(624, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-23 22:11:41', 1),
(625, '::1', 'fakhrizaluciha@gmail.com', 36, '2023-07-23 22:31:32', 1),
(626, '::1', 'fakhrizaluciha@gmail.com', 36, '2023-07-23 22:32:15', 1),
(627, '::1', 'fakhrizaluciha@gmail.com', 36, '2023-07-23 22:36:12', 1),
(628, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-23 22:36:35', 1),
(629, '::1', 'fakhrizaluciha@gmail.com', 36, '2023-07-24 00:01:39', 1),
(630, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-24 00:10:40', 1),
(631, '::1', 'fakhrizaluciha@gmail.com', 36, '2023-07-24 00:15:26', 1),
(632, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-24 00:20:26', 1),
(633, '::1', 'fakhrizaluciha@gmail.com', 36, '2023-07-24 00:20:48', 1),
(634, '::1', 'Admin', NULL, '2023-07-24 00:21:04', 0),
(635, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-24 00:21:08', 1),
(636, '::1', 'fakhrizaluciha@gmail.com', 36, '2023-07-24 01:22:53', 1),
(637, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-24 01:23:23', 1),
(638, '::1', 'fakhrizaluciha@gmail.com', 36, '2023-07-24 01:24:00', 1),
(639, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-24 01:24:18', 1),
(640, '::1', 'fakhrizaluciha@gmail.com', 36, '2023-07-24 01:38:22', 1),
(641, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-24 01:38:37', 1),
(642, '::1', 'fakhrizaluciha@gmail.com', 36, '2023-07-24 02:05:30', 1),
(643, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-24 02:19:37', 1),
(644, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-24 05:27:14', 1),
(645, '::1', 'fakhrizaluciha@gmail.com', 36, '2023-07-24 05:54:37', 1),
(646, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-24 05:56:24', 1),
(647, '::1', 'fakhrizaluciha@gmail.com', 36, '2023-07-24 05:58:25', 1),
(648, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-24 05:58:35', 1),
(649, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-24 06:05:20', 1),
(650, '::1', 'Fakhrizal ', NULL, '2023-07-24 06:08:05', 0),
(651, '::1', 'aziz@gmail.com', 34, '2023-07-24 06:08:16', 1),
(652, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-24 06:08:43', 1),
(653, '::1', 'Fakhrizal ', NULL, '2023-07-24 10:06:17', 0),
(654, '::1', 'aziz@gmail.com', 34, '2023-07-24 10:06:26', 1),
(655, '::1', 'aziz@gmail.com', 34, '2023-07-24 10:07:15', 1),
(656, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-24 10:08:04', 1),
(657, '::1', 'aziz@gmail.com', 34, '2023-07-24 10:47:26', 1),
(658, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-24 10:49:01', 1),
(659, '::1', 'aziz@gmail.com', 34, '2023-07-24 10:55:40', 1),
(660, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-24 21:25:55', 1),
(661, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-25 06:43:32', 1),
(662, '::1', 'aziz@gmail.com', 34, '2023-07-25 06:51:21', 1),
(663, '::1', 'aziz@gmail.com', 34, '2023-07-25 07:17:35', 1),
(664, '::1', 'fakhrizaluciha@gmail.com', 38, '2023-07-25 09:06:38', 1),
(665, '::1', 'aziz@gmail.com', 34, '2023-07-25 21:19:46', 1),
(666, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-25 21:26:10', 1),
(667, '::1', 'aziz@gmail.com', 34, '2023-07-25 22:20:03', 1),
(668, '::1', 'aziz@gmail.com', 34, '2023-07-26 02:06:16', 1),
(669, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-26 02:11:44', 1),
(670, '::1', 'aziz@gmail.com', 34, '2023-07-26 07:06:00', 1),
(671, '::1', 'fakhrizaluciha@gmail.com', 38, '2023-07-26 07:36:18', 1),
(672, '::1', 'aziz@gmail.com', 34, '2023-07-26 07:36:30', 1),
(673, '::1', 'fakhrizaluciha@gmail.com', 38, '2023-07-26 07:38:17', 1),
(674, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-26 07:39:53', 1),
(675, '::1', 'fakhrizaluciha@gmail.com', 38, '2023-07-26 07:40:25', 1),
(676, '::1', 'fakhrizaluciha@gmail.com', 38, '2023-07-26 07:53:26', 1),
(677, '::1', 'aziz@gmail.com', 34, '2023-07-26 07:53:52', 1),
(678, '::1', 'aziz@gmail.com', 34, '2023-07-26 09:03:00', 1),
(679, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-26 09:33:23', 1),
(680, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-26 09:33:57', 1),
(681, '::1', 'aziz@gmail.com', 34, '2023-07-26 09:34:06', 1),
(682, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-26 09:37:47', 1),
(683, '::1', 'aziz@gmail.com', 34, '2023-07-26 09:37:59', 1),
(684, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-26 09:41:00', 1),
(685, '::1', 'aziz@gmail.com', 34, '2023-07-26 09:41:14', 1),
(686, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-26 10:05:03', 1),
(687, '::1', 'aziz@gmail.com', 34, '2023-07-26 10:05:17', 1),
(688, '::1', 'aziz@gmail.com', 34, '2023-07-26 10:14:03', 1),
(689, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-26 10:15:34', 1),
(690, '::1', 'aziz@gmail.com', 34, '2023-07-26 10:15:47', 1),
(691, '::1', 'aziz@gmail.com', 34, '2023-07-26 10:34:33', 1),
(692, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-26 10:35:03', 1),
(693, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-26 10:35:18', 1),
(694, '::1', 'aziz@gmail.com', 34, '2023-07-26 10:35:28', 1),
(695, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-07-26 11:28:56', 1),
(696, '::1', 'aziz@gmail.com', 34, '2023-07-26 11:45:12', 1),
(697, '::1', 'Admin', NULL, '2023-08-01 01:33:14', 0),
(698, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-08-01 01:33:19', 1),
(699, '::1', 'fakhrizaluciha@gmail.com', 38, '2023-08-01 01:33:52', 1),
(700, '::1', 'aziz@gmail.com', 34, '2023-08-01 01:34:09', 1),
(701, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-08-01 09:37:38', 1),
(702, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-08-06 23:35:24', 1),
(703, '::1', 'aziz@gmail.com', 34, '2023-08-06 23:43:25', 1),
(704, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-08-06 23:44:25', 1),
(705, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-08-21 22:48:23', 1),
(706, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-08-21 22:52:18', 1),
(707, '::1', 'aziz@gmail.com', 34, '2023-08-21 22:53:37', 1),
(708, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-08-21 22:58:03', 1),
(709, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-08-21 23:00:36', 1),
(710, '::1', 'aziz@gmail.com', 34, '2023-08-21 23:17:04', 1),
(711, '::1', 'rizalmustaqim150@gmail.com', 5, '2023-08-22 00:36:14', 1),
(712, '::1', 'aziz@gmail.com', 34, '2023-08-22 00:45:07', 1),
(713, '::1', 'aziz@gmail.com', 34, '2023-08-22 03:33:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth_permissions`
--

CREATE TABLE `auth_permissions` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_reset_attempts`
--

CREATE TABLE `auth_reset_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_reset_attempts`
--

INSERT INTO `auth_reset_attempts` (`id`, `email`, `ip_address`, `user_agent`, `token`, `created_at`) VALUES
(1, 'rizalmustaqim150@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '5d3629a03c4d10b440f9ef91409f8591', '2023-06-10 22:39:21'),
(2, 'rizalmustaqim150@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '5d3629a03c4d10b440f9ef91409f8591', '2023-06-10 22:39:33'),
(3, 'rizalmustaqim150@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '1503f6e10b9a40a3e619483f4fe680c6', '2023-06-10 22:48:34'),
(4, 'rizalmustaqim150@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'a8b2f31c5dfdfc762d6936893b8dcf15', '2023-06-10 22:50:05'),
(5, 'rizalmustaqim150@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'a8b2f31c5dfdfc762d6936893b8dcf15', '2023-06-10 22:50:12'),
(6, 'rizalmustaqim150@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '3d5cd8d89425ecee9e0bfd3b96dde2c2', '2023-06-14 05:31:56'),
(7, 'rizalmustaqim150@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '3d5cd8d89425ecee9e0bfd3b96dde2c2', '2023-06-14 05:32:06'),
(8, 'rizalmustaqim150@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '3d5cd8d89425ecee9e0bfd3b96dde2c2', '2023-06-14 05:32:15'),
(9, 'rizalmustaqim150@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '3d5cd8d89425ecee9e0bfd3b96dde2c2', '2023-06-14 05:32:25'),
(10, 'rizalmustaqim150@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '3d5cd8d89425ecee9e0bfd3b96dde2c2', '2023-06-14 05:32:57'),
(11, 'rizalmustaqim150@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'f18945f46bb28148c2e5ccd160bd21a2', '2023-06-14 05:34:16'),
(12, 'rizalmustaqim150@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'f18945f46bb28148c2e5ccd160bd21a2', '2023-06-14 05:34:35'),
(13, 'rizalmustaqim150@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'dc40624ee17c124331cc66721ca2f950', '2023-06-14 21:43:49'),
(14, 'rizalmustaqim150@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'c290129dde0c7af9dfc31d980ff16f03', '2023-06-16 22:56:13'),
(15, 'fakhrizaluciha@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'b014e9a59f1d32f9a4893ac0860cd047', '2023-06-17 23:02:25'),
(16, 'fakhrizaluciha@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '9fd3058dd0b6ce4363781c9181a03ddd', '2023-06-19 02:46:12'),
(17, 'fakhrizaluciha@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '9fd3058dd0b6ce4363781c9181a03ddd', '2023-06-19 02:46:23'),
(18, 'fakhrizaluciha@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '9fd3058dd0b6ce4363781c9181a03ddd', '2023-06-19 02:46:32'),
(19, 'fakhrizaluciha@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2eda7d9b538fc994ba99e02dbf24260d', '2023-06-19 23:04:59'),
(20, 'fakhrizaluciha@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '5c33cd490bb76a874f0eaaad74f0aaa2', '2023-07-06 00:00:46'),
(21, 'fakhrizaluciha@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '5c33cd490bb76a874f0eaaad74f0aaa2', '2023-07-06 00:00:59'),
(22, 'fakhrizaluciha@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '9e994c449413b881efb25f0986e4119e', '2023-07-06 00:01:40'),
(23, 'fakhrizaluciha@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '17a0ebaff43c5b7e1630c7c9e00be8ac', '2023-07-21 22:20:45'),
(24, 'fakhrizaluciha@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '7abf3ee318a18260e6f5b5a07df9a93c', '2023-07-23 22:11:24'),
(25, 'fakhrizaluciha@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2a87b4b5147cd58f48a4acd2ea027961', '2023-07-23 22:32:02'),
(26, 'fakhrizaluciha@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '2a87b4b5147cd58f48a4acd2ea027961', '2023-07-23 22:32:09'),
(27, 'fakhrizaluciha@gmail.com', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', '669693992fa083c915f415fa078cedd3', '2023-07-25 09:06:26');

-- --------------------------------------------------------

--
-- Table structure for table `auth_tokens`
--

CREATE TABLE `auth_tokens` (
  `id` int(11) UNSIGNED NOT NULL,
  `selector` varchar(255) NOT NULL,
  `hashedValidator` varchar(255) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `expires` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_users_permissions`
--

CREATE TABLE `auth_users_permissions` (
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `permission_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bukus`
--

CREATE TABLE `bukus` (
  `id_buku` int(11) NOT NULL,
  `kategori_id` int(11) DEFAULT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `judul_buku` varchar(255) NOT NULL,
  `stok` int(11) NOT NULL,
  `penulis` varchar(255) DEFAULT NULL,
  `penerbit` varchar(255) DEFAULT NULL,
  `uraian` text DEFAULT NULL,
  `tahun_terbit` int(11) DEFAULT NULL,
  `isbn` varchar(255) DEFAULT NULL,
  `sumber` varchar(255) DEFAULT NULL,
  `kode_tempat` varchar(255) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bukus`
--

INSERT INTO `bukus` (`id_buku`, `kategori_id`, `gambar`, `judul_buku`, `stok`, `penulis`, `penerbit`, `uraian`, `tahun_terbit`, `isbn`, `sumber`, `kode_tempat`, `created_at`, `updated_at`, `deleted_at`) VALUES
(93, 32, '1690103489_7ebacea905b4e1c975bd.jpg', 'Ensiklopedia Anak Cerdas: Olahraga', 0, 'Maincent, Geraldine (pengarang) Arlene, Alexandre (ilustrator) Isma Soekoto (pengalih bahasa) Aprillia Wirahma (penyunting)', 'Bhuana Ilmu Populer', 'Buku Ensiklopedia Anak Cerdas Olah Raga berisi penjelasan tentang olah raga zaman kuno, jenis-jenis pertandingan olahraga, dan juara dunia yang berhasil mencatat sejarah dunia. isinya menarik, ringkas disertai ilustrasi yang berwarna, mudah dipahami untuk anak-anak maupun remaja. Cro-Magnon merupakan nenek moyang sejak 35.000 tahun yang lalu. Berlari lebih cepat daripada Usain Bolt, juara dunia lari 100 meter!Â   R 796.03 MAI e (91)', 2019, '', 'https://opac.perpusnas.go.id/DetailOpac.aspx?id=1324751', 'A001', '2023-07-23', '2023-07-26', NULL),
(100, 32, '1690109493_b2edea28cf2ccf32110a.jpg', 'Ensiklopedia Anak Cerdas: Penemuan', 6, 'Crepon, Sophie (pengarang) Guerlais, Gerald (ilustrator) Kling, Laurent (ilustrator) Isma Soekoto (pengalih bahasa) Amelita Risa (penyunting)', 'Bhuana Ilmu Populer', 'Saat ini kamu hidup di zaman modern yang semuanya sudah serba praktis. Namun, bisakah kamu bayangkan dulu sebelum ada teknologi modern seperti sekarang ini?.. Waktu demi waktu, manusia mulai menciptakan barang-barang yang mereka butuhkan pada masa itu.Â Semakin hari penemuan itu semakin disempurnakan hingga seperti sekarang ini. Ensiklopedi ini memberikan informasi penemuan sikat gigi, pensil, antibiotik, tempat sampah, dan masih banyak lainnya. Bisa dibaca oleh anak-anakÂ   R 608.03 CRE e (93)', 2019, '', 'https://opac.perpusnas.go.id/DetailOpac.aspx?id=1324727', 'A002', '2023-07-23', '2023-08-22', NULL),
(104, 33, '1690210312_d6e5245dcd7b08d38529.jpg', 'Abc Chinese - English Dictionary', 8, 'editor, John de Francis ; associate editors, Bai Yuging ... [et al.]', 'Curzon Press', 'Kamus ini merupakan perluasan dari Kamus ABC Cina-Inggris yang inovatif, kamus komputerisasi Pinyin pertama yang diurutkan secara ketat berdasarkan abjad. Ini berisi lebih dari 196.000 entri, dibandingkan dengan 71.486 entri dari karya sebelumnya, menjadikannya kamus satu volume bahasa Mandarin yang paling komprehensif. Urutan abjad tunggal dari entri menyediakan cara termudah dan tercepat untuk mencari istilah yang pengucapannya diketahui. Grafik radikal membantu menemukan karakter ketika pengucapan suatu istilah tidak diketahui dan memfasilitasi akses ke bentuk karakter tradisional, sederhana, dan varian.  R 495.13 ABC (xix, 897 )', 1996, '', 'https://opac.perpusnas.go.id/DetailOpac.aspx?id=96487', 'A001', '2023-07-24', '2023-08-22', NULL),
(105, 31, 'undraw_profile.svg', 'sasa', 4, 'saas', 'saasas', 'asas', 12212, '21212', 'sasa', 'A001', '2023-07-24', '2023-08-19', '2023-08-19'),
(106, 31, '1691383040_4bfb0818ecf11325c08b.jpg', 'Masakan Perancis', 4, 'penerjemah, Lisa ; editor, Jenny E. Nabaiho, Efendi B. Tjauw, Yohanes E.', 'Periplus', 'Masakan Perancis dikenal sangat bervariasi, tergantung daerah mana masakan tersebut berasal. Diantara masakan tersebut ada yang penuh gizi dan menyehatkan atau mengenyangkan dan bercita rasa lembut. Walaupun demikian, masakan Perancis selalu merupakan perpaduan yang serasi antara cita rasa dan tekstur. Masyarakat Perancis memiliki bakat alamiah untuk memadukan bahan-bahan masakan dengan apik dan serasi. Buku ini menyajikan resep masakan Perancis, mulai dari resep sayuran yang dapat disajikan sebagai hidangan utama, resep masakan daging, ayam, ikan, serta hidangan penutup.  Â   -Indah Ekaputri-', 0, '', '', 'A002', '2023-08-06', '2023-08-22', NULL),
(107, 31, '1691383179_4141b0270e65f8c742dd.jpg', 'Aneka masakan dari lele', 5, 'Wahyuni Yulia Pradata ; penyunting, Tetty Yulia', 'AgroMedia Pustaka', 'Lele merupakan ikan air tawar yang banyak dikonsumsi dan dinikmati oleh masyarakat Indonesia. Selain murah, cita rasa daging lele yang gurih dan khas menjadikan lele sebagai salah satu primadona bahan baku hidangan ikan. Buku ini menyajikan berbagai resep olahan lele cita rasa nusantara, mulai dari pecak, pepes, sampai dengan tim lele. Selain itu, buku ini pun dilengkapi dengan cara penanganan lele sebelum dimasak beserta pedoman umum resep masakan.  Â   -Indah Ekaputri-', 0, '', '', 'A004', '2023-08-06', '2023-08-06', NULL),
(108, 35, '1691383283_e8515421a9ed9ab69851.jpg', 'Perry\'s Chemical Engineers\' Handbook 9 Ed', 5, 'Don Green ; Marylee Z. Southard', 'McGraw-Hill', 'Standar emas untuk informasi teknik kimiaâ€”dari dasar hingga yang paling canggih. Sumber daya standar industri ini, pertama kali diterbitkan pada tahun 1934, telah melengkapi generasi insinyur dan ahli kimia dengan informasi, data, dan wawasan penting. Direvisi secara menyeluruh untuk mencerminkan kemajuan dan proses teknologi terbaru , Buku Pegangan Insinyur Kimia Perry, Edisi Kesembilan, sangat diperlukan untuk cakupan yang tak tertandingi dari setiap aspek teknik kimia. Fitur baru termasuk cakupan bioreactors dan bioprocessing, presentasi praktis dari dasar-dasar termodinamika, dan data properti kimia baru dan diperbarui.  R 660 SOU p (xvi; )', 2019, '', 'https://opac.perpusnas.go.id/DetailOpac.aspx?id=1333186', 'A003', '2023-08-06', '2023-08-06', NULL),
(109, 34, '1691383370_4ba87641151eecbd72be.jpg', 'Solusi sehat mengatasi asam urat dan rematik', 5, 'Redaksi AgroMedia ; penyunting, Bagus', 'AgroMedia Pustaka', 'Asam urat dan rematik merupakan dua penyakit yang sering dianggap berhubungan. Hal ini disebabkan karena salah satu penyebab rematik adalah terbentuknya kristal di sendi akibat tingginya kadar asam urat. Setiap manusia memiliki kandungan asam urat dalam tubuhnya. Hal ini merupakan hal yang alami karena asam urat terbentuk dari hasil metabolisme purin yang terdapat dalam makanan. Akan tetapi, permasalahan muncul pada saat kadarnya dalam tubuh jauh di atas normal. Buku ini membahas mengenai asam urat dan rematik. Mulai dari pengenalan hingga mengontrol kadar asam urat di dalam tubuh, serta ramuan herbal, resep makanan, dan terapi jus untuk mengatasi tingginya kadar asam urat dan rematik.  Â   -Indah Ekaputri-', 0, '', '', 'A006', '2023-08-06', '2023-08-19', NULL),
(123, 31, 'Screenshot (745).png', 'sasa3saas', 5, 'asas', NULL, 'asas', 21212, NULL, NULL, 'sas', '2023-08-19', '2023-08-19', '2023-08-22'),
(124, 31, 'C21a_Q3_Fakhrizal Mustaqim.jpg', 'qwqwqwqww', 5, 'qwqwqw', NULL, 'saasasas', NULL, NULL, NULL, 'wqqqw', '2023-08-19', '2023-08-19', '2023-08-22');

-- --------------------------------------------------------

--
-- Table structure for table `kategoris`
--

CREATE TABLE `kategoris` (
  `id_kategori` int(11) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategoris`
--

INSERT INTO `kategoris` (`id_kategori`, `kategori`, `created_at`, `updated_at`) VALUES
(31, 'Resep', NULL, '2023-08-21 23:15:23'),
(32, 'Ensiklopedia Anak Cerdas', NULL, NULL),
(33, 'Kamus', NULL, NULL),
(34, 'Kesehatan', NULL, NULL),
(35, 'Asing', NULL, NULL),
(37, 'Karya Umum', '2023-08-22 00:43:08', '2023-08-22 00:43:08'),
(38, 'Filsafat dan Psikologi', '2023-08-22 00:43:13', '2023-08-22 00:43:13'),
(39, 'Agama', '2023-08-22 00:43:17', '2023-08-22 00:43:17'),
(40, 'Ilmu Sosial', '2023-08-22 00:43:23', '2023-08-22 00:43:23'),
(41, 'Bahasa', '2023-08-22 00:43:27', '2023-08-22 00:43:27'),
(42, 'Ilmu Murni', '2023-08-22 00:43:31', '2023-08-22 00:43:31'),
(43, 'Seni dan Olahraga', '2023-08-22 00:43:34', '2023-08-22 00:43:34'),
(44, 'Sejarah dan Biografi', '2023-08-22 00:43:38', '2023-08-22 00:43:38'),
(45, 'Ilmu Terapan', '2023-08-22 00:43:55', '2023-08-22 00:43:55');

-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

CREATE TABLE `keranjang` (
  `id_keranjang` int(11) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `buku_id` int(11) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `deadline` date NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(1, '2017-11-20-223112', 'Myth\\Auth\\Database\\Migrations\\CreateAuthTables', 'default', 'Myth\\Auth', 1673246517, 1);

-- --------------------------------------------------------

--
-- Table structure for table `peminjamans`
--

CREATE TABLE `peminjamans` (
  `id_peminjaman` int(11) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `deadline` date NOT NULL,
  `kode` varchar(255) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `status_peminjaman` enum('dipinjam','dikembalikan') NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `peminjamans`
--

INSERT INTO `peminjamans` (`id_peminjaman`, `tgl_pinjam`, `deadline`, `kode`, `user_id`, `qty`, `status_peminjaman`, `created_at`, `updated_at`, `deleted_at`) VALUES
(222, '2023-08-22', '2023-09-01', '64e448e3a1f1d', 34, 1, 'dikembalikan', '2023-08-22', '2023-08-22', NULL),
(223, '2023-08-22', '2023-09-01', '64e44918c5f9d', 34, 1, 'dikembalikan', '2023-08-22', '2023-08-22', NULL),
(224, '2023-08-22', '2023-09-01', '64e4752db8a5d', 34, 1, 'dipinjam', '2023-08-22', '2023-08-22', NULL),
(225, '2023-08-22', '2023-09-01', '64e4752dc3b76', 34, 1, 'dipinjam', '2023-08-22', '2023-08-22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman_has_buku`
--

CREATE TABLE `peminjaman_has_buku` (
  `peminjaman_id` int(11) NOT NULL,
  `buku_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `peminjaman_has_buku`
--

INSERT INTO `peminjaman_has_buku` (`peminjaman_id`, `buku_id`, `created_at`, `updated_at`) VALUES
(222, 106, '2023-08-22', '2023-08-22'),
(223, 104, '2023-08-22', '2023-08-22'),
(224, 106, '2023-08-22', '2023-08-22'),
(225, 100, '2023-08-22', '2023-08-22');

-- --------------------------------------------------------

--
-- Table structure for table `pengembalians`
--

CREATE TABLE `pengembalians` (
  `id_pengembalian` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `peminjaman_id` int(11) DEFAULT NULL,
  `tgl_kembali` date DEFAULT NULL,
  `denda` int(11) NOT NULL DEFAULT 0,
  `jml_pengembalian` int(11) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengembalians`
--

INSERT INTO `pengembalians` (`id_pengembalian`, `user_id`, `peminjaman_id`, `tgl_kembali`, `denda`, `jml_pengembalian`, `created_at`, `updated_at`, `deleted_at`) VALUES
(189, 34, 222, '2023-08-22', 0, 1, '2023-08-22', '2023-08-22', NULL),
(190, 34, 223, '2023-08-22', 0, 1, '2023-08-22', '2023-08-22', NULL),
(191, 34, 224, '0000-00-00', 0, 0, '2023-08-22', '2023-08-22', NULL),
(192, 34, 225, '0000-00-00', 0, 0, '2023-08-22', '2023-08-22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pengembalian_has_buku`
--

CREATE TABLE `pengembalian_has_buku` (
  `buku_id` int(11) NOT NULL,
  `pengembalian_id` int(11) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengembalian_has_buku`
--

INSERT INTO `pengembalian_has_buku` (`buku_id`, `pengembalian_id`, `created_at`, `updated_at`) VALUES
(106, 189, '2023-08-22', '2023-08-22'),
(104, 190, '2023-08-22', '2023-08-22'),
(106, 191, '2023-08-22', '2023-08-22'),
(100, 192, '2023-08-22', '2023-08-22');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id_pesan` int(11) NOT NULL,
  `pesan` text DEFAULT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id_pesan`, `pesan`, `user_id`, `created_at`, `updated_at`) VALUES
(42, 'tolong cepat kembalikan', 34, '2023-07-19 12:51:37', '2023-07-19 00:51:45'),
(44, 'coba', 34, '2023-07-22 03:56:30', '2023-07-21 15:56:37');

-- --------------------------------------------------------

--
-- Table structure for table `pinjam`
--

CREATE TABLE `pinjam` (
  `id_pinjam` int(11) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `buku_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Belum dikembalikan',
  `denda` int(11) NOT NULL DEFAULT 0,
  `tgl_pinjam` date NOT NULL,
  `tgl_balik` date DEFAULT NULL,
  `tgl_kembali` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(30) NOT NULL,
  `foto` varchar(255) NOT NULL DEFAULT 'undraw_profile_2.svg',
  `jenis_kelamin` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `nis` int(11) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `reset_hash` varchar(255) DEFAULT NULL,
  `reset_at` datetime DEFAULT NULL,
  `reset_expires` datetime DEFAULT NULL,
  `activate_hash` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `status_message` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `force_pass_reset` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `foto`, `jenis_kelamin`, `password_hash`, `tgl_lahir`, `nis`, `alamat`, `reset_hash`, `reset_at`, `reset_expires`, `activate_hash`, `status`, `status_message`, `active`, `force_pass_reset`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 'rizalmustaqim150@gmail.com', 'Admin', 'undraw_profile_2.svg', '', '$2y$10$GH.ZAPNOLJcOLNLt8QTxpeGfC.6jN/wS0qv4LA.UAte2tP0yypnRG', '0000-00-00', NULL, NULL, NULL, '2023-06-16 22:56:13', NULL, NULL, NULL, NULL, 1, 0, '2023-01-09 08:22:49', '2023-06-10 22:49:07', NULL),
(34, 'aziz@gmail.com', 'Aziz', 'undraw_profile_2.svg', 'laki-laki', '$2y$10$zXtsZfmmyvwcinJP63i.K.sNtmdRqlCgFhU1g9LBgEkxFLPL6MWl6', '2023-07-19', 12345677, 'palur', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, '2023-08-22 03:50:30', NULL),
(38, 'fakhrizaluciha@gmail.com', 'Fakhrizal', 'undraw_profile_2.svg', 'laki-laki', '$2y$10$fEt/1GnyX6KJNwIkOhRsneIq73zjEPMknU4wgiEkALFEdvXNZaXem', '2023-07-26', 21212, '12121', NULL, '2023-07-25 09:06:27', NULL, NULL, NULL, NULL, 1, 0, NULL, '2023-07-26 07:38:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_buku`
--

CREATE TABLE `user_buku` (
  `id_user_buku` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `buku_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id_video` int(11) NOT NULL,
  `video` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `judul_video` varchar(255) NOT NULL,
  `penerbit` varchar(255) NOT NULL,
  `tahun_terbit` int(11) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id_video`, `video`, `url`, `judul_video`, `penerbit`, `tahun_terbit`, `created_at`, `updated_at`, `deleted_at`) VALUES
(36, '1686986274_e7d63ab2c55c17205907.mp4', 'https://www.youtube.com/watch?v=8x0Dty5D6CA&t=45s', 'Deploy NodeJS App on Netlify for Free (Heroku Alternative) | NEW 2023 Tutorial ', 'Coding with Kazim', 2023, '2023-06-17', '2023-07-21', NULL),
(37, '1686986379_b053d324f90ec8618920.mp4', 'https://www.youtube.com/watch?v=E_IplykMZH0&t=2s', 'ARRAY pada Programming', 'Web Programming UNPAS', 2023, '2023-06-17', '2023-06-17', NULL),
(38, '1686986558_3292c2a68eed7547cffe.mp4', 'https://www.youtube.com/watch?v=C2Pv_ZHUdv0&t=2s', 'Teknik DEBUGGING Pada Programming', 'Web Programming UNPAS', 2023, '2023-06-17', '2023-06-17', NULL),
(39, '1686986835_c5a9f7b2b257763f14d9.mp4', 'https://www.youtube.com/watch?v=q04buHDFT6M&t=2s', 'STRUKTUR DATA dalam PEMROGRAMAN', 'Web Programming UNPAS', 2023, '2023-06-17', '2023-06-17', NULL),
(40, '1686986997_2c5ef22e896251eba585.mp4', 'https://www.youtube.com/watch?v=uqVJc9lLknA&t=1s', 'ALGORITMA dalam PEMROGRAMAN', 'Web Programming UNPAS', 2023, '2023-06-17', '2023-06-19', NULL),
(46, 'undraw_profile.svg', '', 'sasabaru', 'sas', 2212, '2023-07-24', '2023-07-24', '2023-07-25'),
(48, 'undraw_profile.svg', '', 'as', 'sasa', 2121, '2023-07-24', '2023-07-24', '2023-07-25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_activation_attempts`
--
ALTER TABLE `auth_activation_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_groups`
--
ALTER TABLE `auth_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_groups_permissions`
--
ALTER TABLE `auth_groups_permissions`
  ADD KEY `auth_groups_permissions_permission_id_foreign` (`permission_id`),
  ADD KEY `group_id_permission_id` (`group_id`,`permission_id`);

--
-- Indexes for table `auth_groups_users`
--
ALTER TABLE `auth_groups_users`
  ADD KEY `auth_groups_users_user_id_foreign` (`user_id`),
  ADD KEY `group_id_user_id` (`group_id`,`user_id`);

--
-- Indexes for table `auth_logins`
--
ALTER TABLE `auth_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_reset_attempts`
--
ALTER TABLE `auth_reset_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auth_tokens_user_id_foreign` (`user_id`),
  ADD KEY `selector` (`selector`);

--
-- Indexes for table `auth_users_permissions`
--
ALTER TABLE `auth_users_permissions`
  ADD KEY `auth_users_permissions_permission_id_foreign` (`permission_id`),
  ADD KEY `user_id_permission_id` (`user_id`,`permission_id`);

--
-- Indexes for table `bukus`
--
ALTER TABLE `bukus`
  ADD PRIMARY KEY (`id_buku`),
  ADD KEY `kategori_id` (`kategori_id`);

--
-- Indexes for table `kategoris`
--
ALTER TABLE `kategoris`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD PRIMARY KEY (`id_keranjang`),
  ADD KEY `buku_id` (`buku_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peminjamans`
--
ALTER TABLE `peminjamans`
  ADD PRIMARY KEY (`id_peminjaman`),
  ADD KEY `peminjaman_ibfk_1` (`user_id`);

--
-- Indexes for table `peminjaman_has_buku`
--
ALTER TABLE `peminjaman_has_buku`
  ADD KEY `buku_id` (`buku_id`),
  ADD KEY `peminjaman_id` (`peminjaman_id`);

--
-- Indexes for table `pengembalians`
--
ALTER TABLE `pengembalians`
  ADD PRIMARY KEY (`id_pengembalian`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `pengembalian_ibfk_2` (`peminjaman_id`);

--
-- Indexes for table `pengembalian_has_buku`
--
ALTER TABLE `pengembalian_has_buku`
  ADD KEY `buku_id` (`buku_id`),
  ADD KEY `pengembalian_id` (`pengembalian_id`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id_pesan`),
  ADD KEY `pesan_ibfk_1` (`user_id`);

--
-- Indexes for table `pinjam`
--
ALTER TABLE `pinjam`
  ADD PRIMARY KEY (`id_pinjam`),
  ADD KEY `pinjam_ibfk_1` (`user_id`),
  ADD KEY `pinjam_ibfk_2` (`buku_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `password` (`password_hash`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `user_buku`
--
ALTER TABLE `user_buku`
  ADD PRIMARY KEY (`id_user_buku`),
  ADD KEY `user_buku_ibfk_1` (`user_id`),
  ADD KEY `user_buku_ibfk_2` (`buku_id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id_video`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_activation_attempts`
--
ALTER TABLE `auth_activation_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_groups`
--
ALTER TABLE `auth_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `auth_logins`
--
ALTER TABLE `auth_logins`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=714;

--
-- AUTO_INCREMENT for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_reset_attempts`
--
ALTER TABLE `auth_reset_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bukus`
--
ALTER TABLE `bukus`
  MODIFY `id_buku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `kategoris`
--
ALTER TABLE `kategoris`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `keranjang`
--
ALTER TABLE `keranjang`
  MODIFY `id_keranjang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=246;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `peminjamans`
--
ALTER TABLE `peminjamans`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=226;

--
-- AUTO_INCREMENT for table `pengembalians`
--
ALTER TABLE `pengembalians`
  MODIFY `id_pengembalian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id_pesan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `pinjam`
--
ALTER TABLE `pinjam`
  MODIFY `id_pinjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `user_buku`
--
ALTER TABLE `user_buku`
  MODIFY `id_user_buku` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id_video` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_groups_permissions`
--
ALTER TABLE `auth_groups_permissions`
  ADD CONSTRAINT `auth_groups_permissions_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `auth_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `auth_groups_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `auth_permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `auth_groups_users`
--
ALTER TABLE `auth_groups_users`
  ADD CONSTRAINT `auth_groups_users_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `auth_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `auth_groups_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  ADD CONSTRAINT `auth_tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `auth_users_permissions`
--
ALTER TABLE `auth_users_permissions`
  ADD CONSTRAINT `auth_users_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `auth_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `auth_users_permissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `bukus`
--
ALTER TABLE `bukus`
  ADD CONSTRAINT `bukus_ibfk_1` FOREIGN KEY (`kategori_id`) REFERENCES `kategoris` (`id_kategori`);

--
-- Constraints for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD CONSTRAINT `keranjang_ibfk_1` FOREIGN KEY (`buku_id`) REFERENCES `bukus` (`id_buku`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `keranjang_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `peminjamans`
--
ALTER TABLE `peminjamans`
  ADD CONSTRAINT `peminjamans_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `peminjaman_has_buku`
--
ALTER TABLE `peminjaman_has_buku`
  ADD CONSTRAINT `peminjaman_has_buku_ibfk_1` FOREIGN KEY (`buku_id`) REFERENCES `bukus` (`id_buku`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `peminjaman_has_buku_ibfk_2` FOREIGN KEY (`peminjaman_id`) REFERENCES `peminjamans` (`id_peminjaman`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengembalians`
--
ALTER TABLE `pengembalians`
  ADD CONSTRAINT `pengembalians_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengembalians_ibfk_2` FOREIGN KEY (`peminjaman_id`) REFERENCES `peminjamans` (`id_peminjaman`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengembalian_has_buku`
--
ALTER TABLE `pengembalian_has_buku`
  ADD CONSTRAINT `pengembalian_has_buku_ibfk_1` FOREIGN KEY (`buku_id`) REFERENCES `bukus` (`id_buku`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengembalian_has_buku_ibfk_2` FOREIGN KEY (`pengembalian_id`) REFERENCES `pengembalians` (`id_pengembalian`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pesan`
--
ALTER TABLE `pesan`
  ADD CONSTRAINT `pesan_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pinjam`
--
ALTER TABLE `pinjam`
  ADD CONSTRAINT `pinjam_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pinjam_ibfk_2` FOREIGN KEY (`buku_id`) REFERENCES `bukus` (`id_buku`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_buku`
--
ALTER TABLE `user_buku`
  ADD CONSTRAINT `user_buku_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_buku_ibfk_2` FOREIGN KEY (`buku_id`) REFERENCES `bukus` (`id_buku`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
