<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('/login', 'Login::index');
$routes->get('/register', 'Register::index');
$routes->post('/detail/pinjam/save', 'Home::save', ['filter' => 'role:User']);
$routes->get('/keranjang', 'Keranjang::index', ['filter' => 'role:User']);
$routes->delete('/keranjang/(:any)', 'Keranjang::delete/$1', ['filter' => 'role:User']);
$routes->post('/keranjang', 'Keranjang::save', ['filter' => 'role:User']);
$routes->get('/detail/(:any)', 'Home::detail/$1', ['filter' => 'role:User']);
$routes->get('/koleksi-buku', 'Home::koleksi', ['filter' => 'role:User']);
$routes->get('/koleksi-buku/(:num)', 'Home::koleksiKategori/$1', ['filter' => 'role:User']);
$routes->get('/profile', 'Users::index', ['filter' => 'role:User']);
$routes->get('/pesan', 'Home::Pesan', ['filter' => 'role:User']);
$routes->post('/profile/save', 'Users::saveProfil', ['filter' => 'role:User']);
$routes->post('/foto/save', 'Users::saveFoto', ['filter' => 'role:User']);
$routes->get('/riwayat', 'Users::riwayat', ['filter' => 'role:User']);
$routes->get('/riwayat/cetak/(:num)', 'Users::cetak/$1', ['filter' => 'role:User']);
$routes->get('/coba', 'Home::coba', ['filter' => 'role:User']);
$routes->get('/koleksi-video', 'Home::koleksiVideo', ['filter' => 'role:User']);

// ADMIN
// $routes->get('/admin', 'Buku::index', ['filter' => 'role:Admin']);
// USER
$routes->get('/admin/data-user', 'Users::User', ['filter' => 'role:Admin']);
$routes->get('/admin/petugas', 'Users::Petugas', ['filter' => 'role:Admin']);
$routes->get('/admin/peminjam', 'Users::Peminjam', ['filter' => 'role:Admin']);
$routes->post('/admin/peminjam', 'Users::save', ['filter' => 'role:Admin']);
$routes->get('/admin/peminjam/sampah', 'Users::sampahPeminjam', ['filter' => 'role:Admin']);
$routes->post('/admin/peminjam/restore/(:num)', 'Users::restorePeminjam/$1', ['filter' => 'role:Admin']);
// $routes->get('/admin/peminjam/tambah', 'Users::create', ['filter' => 'role:Admin']);
$routes->delete('/admin/user/hapus/(:num)', 'Users::deleteUser/$1', ['filter' => 'role:Admin']);
$routes->get('/admin/petugas/edit/(:num)', 'Users::editPetugas/$1', ['filter' => 'role:Admin']);
$routes->post('/admin/petugas/(:num)', 'Users::updatePetugas/$1', ['filter' => 'role:Admin']);
// USER
// BUKU
$routes->get('/admin/buku', 'Buku::Buku', ['filter' => 'role:Admin']);
$routes->get('/admin/buku/tambah', 'Buku::create', ['filter' => 'role:Admin']);
$routes->post('/admin/buku', 'Buku::save', ['filter' => 'role:Admin']);
$routes->delete('/admin/buku/(:any)', 'Buku::deleteBuku/$1', ['filter' => 'role:Admin']);
$routes->post('/admin/buku/(:segment)', 'Buku::updateBuku/$1', ['filter' => 'role:Admin']);
$routes->post('/admin/buku/restore/(:num)', 'Buku::restore/$1', ['filter' => 'role:Admin']);
$routes->get('/admin/buku/sampah', 'Buku::sampah', ['filter' => 'role:Admin']);
$routes->get('/admin/buku/edit/(:any)', 'Buku::editBuku/$1', ['filter' => 'role:Admin']);
// BUKU
// PINJAM
$routes->get('/admin/peminjaman/(:num)', 'Pinjam::edit/$1', ['filter' => 'role:Admin']);
$routes->post('/admin/peminjaman/(:num)', 'Pinjam::update/$1', ['filter' => 'role:Admin']);
$routes->get('/admin/peminjaman', 'Pinjam::index', ['filter' => 'role:Admin']);
$routes->get('/admin/peminjaman/sampah', 'Pinjam::sampah', ['filter' => 'role:Admin']);
$routes->post('/admin/peminjaman/restore/(:any)', 'Pinjam::restore/$1', ['filter' => 'role:Admin']);
$routes->post('/admin/peminjaman/cetak', 'Pinjam::cetak', ['filter' => 'role:Admin']);
$routes->get('/admin/peminjaman/tambah', 'Pinjam::create', ['filter' => 'role:Admin']);
$routes->post('/admin/peminjaman', 'Pinjam::save', ['filter' => 'role:Admin']);
$routes->delete('/admin/peminjaman/(:any)', 'Pinjam::deletePinjam/$1', ['filter' => 'role:Admin']);
// PINJAM
// KATEGORI
$routes->get('/admin/kategori', 'Kategori::index', ['filter' => 'role:Admin']);
$routes->post('/admin/kategori', 'Kategori::save', ['filter' => 'role:Admin']);
$routes->delete('/admin/kategori/(:any)', 'Kategori::delete/$1', ['filter' => 'role:Admin']);
$routes->get('/admin/kategori/(:any)', 'Kategori::edit/$1', ['filter' => 'role:Admin']);
$routes->post('/admin/kategori/(:segment)', 'Kategori::update/$1', ['filter' => 'role:Admin']);
// KATEGORI
// PESAN
$routes->get('/admin/pesan/(:any)', 'Pesan::index/$1', ['filter' => 'role:Admin']);
$routes->post('/admin/pesan', 'Pesan::kirim', ['filter' => 'role:Admin']);
// PESAN
// $routes->get('/admin/data-pengembalian', 'Pengembalian::index');
// $routes->get('/admin/tambah-pengembalian', 'Pengembalian::createPengembalian');
// $routes->post('/admin/save-pengembalian', 'Pengembalian::save');
// $routes->delete('/admin/hapus-pengembalian/(:any)', 'Pengembalian::deletePengembalian/$1');
// $routes->post('/admin/data-pengembalian/cetak', 'Pengembalian::cetakPengembalian');
// $routes->get('/admin/edit-pengembalian/(:any)', 'Pengembalian::editPengembalian/$1');
// $routes->post('/admin/update-pengembalian/(:any)', 'Pengembalian::update/$1');
// VIDEO
$routes->get('/admin/video', 'Video::index', ['filter' => 'role:Admin']);
$routes->get('/admin/video/tambah', 'Video::create', ['filter' => 'role:Admin']);
$routes->post('/admin/video', 'Video::save', ['filter' => 'role:Admin']);
$routes->get('/admin/video/sampah', 'Video::sampah', ['filter' => 'role:Admin']);
$routes->post('/admin/video/restore/(:num)', 'Video::restore/$1', ['filter' => 'role:Admin']);
$routes->post('/admin/video/(:any)', 'Video::update/$1', ['filter' => 'role:Admin']);
$routes->delete('/admin/video/(:any)', 'Video::deleteVideo/$1', ['filter' => 'role:Admin']);
$routes->get('/admin/video/(:any)', 'Video::edit/$1', ['filter' => 'role:Admin']);
// VIDEO
// PELAPORAN
$routes->get('/admin/pelaporan', 'Pelaporan::index', ['filter' => 'role:Admin']);
$routes->post('/admin/pelaporan/cetak', 'Pelaporan::cetak', ['filter' => 'role:Admin']);
// PELAPORAN
// $routes->get('/video/(:any)')
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
