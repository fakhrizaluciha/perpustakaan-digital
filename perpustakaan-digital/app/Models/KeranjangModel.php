<?php

namespace App\Models;

use CodeIgniter\Model;

class KeranjangModel extends Model
{
    // protected $DBGroup          = 'default';
    protected $table            = 'keranjang';
    protected $primaryKey       = 'id_keranjang';
    protected $allowedFields    = ['user_id', 'buku_id', 'tgl_pinjam', 'deadline', 'qty'];
    // protected $useAutoIncrement = true;
    // protected $insertID         = 0;
    // protected $returnType       = 'array';
    // protected $useSoftDeletes   = false;
    // protected $protectFields    = true;

    // Dates
    protected $useTimestamps = true;
    // protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // Validation
    // protected $validationRules      = [];
    // protected $validationMessages   = [];
    // protected $skipValidation       = false;
    // protected $cleanValidationRules = true;

    // Callbacks
    // protected $allowCallbacks = true;
    // protected $beforeInsert   = [];
    // protected $afterInsert    = [];
    // protected $beforeUpdate   = [];
    // protected $afterUpdate    = [];
    // protected $beforeFind     = [];
    // protected $afterFind      = [];
    // protected $beforeDelete   = [];
    // protected $afterDelete    = [];

    public function getKeranjangByIdUser()
    {
        $builder = $this->db->table('keranjang')
            ->join('users', 'users.id = keranjang.user_id')
            ->join('bukus', 'bukus.id_buku = keranjang.buku_id')
            ->orderBy('id_keranjang', 'DESC');
        return $builder->where(['keranjang.user_id' => user()->id, 'qty >' => '0'])->get()->getResult();
    }

    public function getTotalKeranjangByIdUser()
    {
        $builder = $this->db->table('keranjang')
            ->select("sum(qty) as total")
            ->join('users', 'users.id = keranjang.user_id')
            ->join('bukus', 'bukus.id_buku = keranjang.buku_id')
            ->orderBy('id_keranjang', 'DESC');
        return $builder->where(['keranjang.user_id' => user()->id])->get()->getResult();
    }

    public function getKeranjangByIdBuku($id_buku)
    {
        $builder = $this->db->table('keranjang')
            ->join('users', 'users.id = keranjang.user_id')
            ->join('bukus', 'bukus.id_buku = keranjang.buku_id')
            ->orderBy('id_keranjang', 'DESC');
        return $builder->where(['keranjang.buku_id' => $id_buku])->get()->getResult();
    }

    public function getKeranjangByIdBukuAndIdUser($id_buku)
    {
        $builder = $this->db->table('keranjang')
            ->join('users', 'users.id = keranjang.user_id')
            ->join('bukus', 'bukus.id_buku = keranjang.buku_id')
            ->orderBy('id_keranjang', 'DESC');
        return $builder->where(['keranjang.user_id' => user()->id, 'keranjang.buku_id' => $id_buku])->get()->getResult();
    }

    public function deleteKeranjang()
    {
        $builder = $this->db->table('keranjang')
            ->join('users', 'users.id = keranjang.user_id')
            ->join('bukus', 'bukus.id_buku = keranjang.buku_id')
            ->orderBy('id_keranjang', 'DESC');
        return $builder->where(['keranjang.user_id' => user()->id])->delete();
    }
}
