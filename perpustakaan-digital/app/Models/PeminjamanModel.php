<?php

namespace App\Models;

use CodeIgniter\Model;

class PeminjamanModel extends Model
{
    // protected $DBGroup          = 'default';
    protected $table            = 'peminjamans';
    protected $primaryKey       = 'id_peminjaman';
    protected $allowedFields    = ['user_id', 'tgl_pinjam', 'qty', 'deadline', 'kode', 'status_peminjaman'];
    protected $useTimestamps = true;
    // protected $useAutoIncrement = true;
    // protected $insertID         = 0;
    // protected $returnType       = 'array';
    protected $useSoftDeletes   = true;
    // protected $protectFields    = true;

    // Dates
    // protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // Validation
    // protected $validationRules      = [];
    // protected $validationMessages   = [];
    // protected $skipValidation       = false;
    // protected $cleanValidationRules = true;

    // Callbacks
    // protected $allowCallbacks = true;
    // protected $beforeInsert   = [];
    // protected $afterInsert    = [];
    // protected $beforeUpdate   = [];
    // protected $afterUpdate    = [];
    // protected $beforeFind     = [];
    // protected $afterFind      = [];
    // protected $beforeDelete   = [];
    // protected $afterDelete    = [];
    public function getPeminjaman()
    {
        $builder = $this->db->table('peminjamans')
            ->join('users', 'users.id = peminjamans.user_id')
            ->join('pengembalians', 'pengembalians.peminjaman_id = peminjamans.id_peminjaman')
            ->join('peminjaman_has_buku', 'peminjaman_has_buku.peminjaman_id = peminjamans.id_peminjaman')
            ->join('bukus', 'bukus.id_buku = peminjaman_has_buku.buku_id');
        return $builder->orderBy('id_peminjaman', 'DESC')->where(['peminjamans.deleted_at' => null])->get()->getResult();
    }
    public function getSampahPeminjaman()
    {
        $builder = $this->db->table('peminjamans')->select('peminjamans.id_peminjaman, users.username, bukus.judul_buku, peminjamans.qty, users.nis, peminjamans.tgl_pinjam, peminjamans.deadline, pengembalians.tgl_kembali, pengembalians.denda, peminjamans.kode, peminjamans.status_peminjaman, peminjamans.deleted_at')
            ->join('users', 'users.id = peminjamans.user_id')
            ->join('pengembalians', 'pengembalians.peminjaman_id = peminjamans.id_peminjaman')
            ->join('peminjaman_has_buku', 'peminjaman_has_buku.peminjaman_id = peminjamans.id_peminjaman')
            ->join('bukus', 'bukus.id_buku = peminjaman_has_buku.buku_id');
        return $builder->orderBy('id_peminjaman', 'DESC')->where(['peminjamans.deleted_at !=' => null])->get()->getResult();
    }
    public function getPinjamByTgl($tglPinjam1, $tglPinjam2)
    {
        $builder = $this->db->table('peminjamans')
            ->join('users', 'users.id = peminjamans.user_id')
            ->join('pengembalians', 'pengembalians.peminjaman_id = peminjamans.id_peminjaman')
            ->join('peminjaman_has_buku', 'peminjaman_has_buku.peminjaman_id = peminjamans.id_peminjaman')
            ->join('bukus', 'bukus.id_buku = peminjaman_has_buku.buku_id');;
        if ($tglPinjam1 == false) {
            return $builder->get()->getResult();
        }
        return $builder->where(['tgl_pinjam >=' => $tglPinjam1, 'tgl_pinjam <=' => $tglPinjam2, 'peminjamans.deleted_at' => null])->get()->getResult();
    }
    public function getPeminjamanById($id_peminjaman = false)
    {
        $builder = $this->db->table('peminjamans')
            ->join('users', 'users.id = peminjamans.user_id')
            ->join('pengembalians', 'pengembalians.peminjaman_id = peminjamans.id_peminjaman')
            ->join('peminjaman_has_buku', 'peminjaman_has_buku.peminjaman_id = peminjamans.id_peminjaman')
            ->join('bukus', 'bukus.id_buku = peminjaman_has_buku.buku_id');
        if ($id_peminjaman == false) {
            return $builder->get()->getResult();
        }
        return $builder->where(['id_peminjaman' => $id_peminjaman, 'peminjamans.deleted_at' => null])->get()->getResult();
    }
    public function getPeminjamanByIdUser()
    {
        $builder = $this->db->table('peminjamans')
            ->join('users', 'users.id = peminjamans.user_id')
            ->join('pengembalians', 'pengembalians.peminjaman_id = peminjamans.id_peminjaman')
            ->join('peminjaman_has_buku', 'peminjaman_has_buku.peminjaman_id = peminjamans.id_peminjaman')
            ->join('bukus', 'bukus.id_buku = peminjaman_has_buku.buku_id')
            ->orderBy('id_peminjaman', 'DESC');
        return $builder->where(['pengembalians.user_id' => user()->id, 'peminjamans.deleted_at' => null])->get()->getResult();
    }
    public function getPinjamByTglAndStatus($tglPinjam1, $tglPinjam2, $status_peminjaman)
    {
        $builder = $this->db->table('peminjamans')
            ->join('users', 'users.id = peminjamans.user_id')
            ->join('pengembalians', 'pengembalians.peminjaman_id = peminjamans.id_peminjaman')
            ->join('peminjaman_has_buku', 'peminjaman_has_buku.peminjaman_id = peminjamans.id_peminjaman')
            ->join('bukus', 'bukus.id_buku = peminjaman_has_buku.buku_id');;
        if ($tglPinjam1 == false) {
            return $builder->get()->getResult();
        }
        return $builder->where(['tgl_pinjam >=' => $tglPinjam1, 'tgl_pinjam <=' => $tglPinjam2, 'status_peminjaman' => $status_peminjaman, 'peminjamans.deleted_at' => null])->get()->getResult();
    }
    public function getTotalJumlahByTgl($tglPinjam1, $tglPinjam2)
    {
        $builder = $this->db->table('peminjamans')
            ->select("sum(qty) as total")
            ->join('users', 'users.id = peminjamans.user_id')
            ->join('pengembalians', 'pengembalians.peminjaman_id = peminjamans.id_peminjaman')
            ->join('peminjaman_has_buku', 'peminjaman_has_buku.peminjaman_id = peminjamans.id_peminjaman')
            ->join('bukus', 'bukus.id_buku = peminjaman_has_buku.buku_id');;
        if ($tglPinjam1 == false) {
            return $builder->get()->getResult();
        }
        return $builder->where(['tgl_pinjam >=' => $tglPinjam1, 'tgl_pinjam <=' => $tglPinjam2, 'peminjamans.deleted_at' => null])->get()->getResult();
    }
    public function getTotalJumlahByTglAndStatus($tglPinjam1, $tglPinjam2, $status_peminjaman)
    {
        $builder = $this->db->table('peminjamans')
            ->select("sum(qty) as total")
            ->join('users', 'users.id = peminjamans.user_id')
            ->join('pengembalians', 'pengembalians.peminjaman_id = peminjamans.id_peminjaman')
            ->join('peminjaman_has_buku', 'peminjaman_has_buku.peminjaman_id = peminjamans.id_peminjaman')
            ->join('bukus', 'bukus.id_buku = peminjaman_has_buku.buku_id');;
        if ($tglPinjam1 == false) {
            return $builder->get()->getResult();
        }
        return $builder->where(['tgl_pinjam >=' => $tglPinjam1, 'tgl_pinjam <=' => $tglPinjam2, 'status_peminjaman' => $status_peminjaman, 'peminjamans.deleted_at' => null])->get()->getResult();
    }
    public function getPeminjamanByUserAndStatus()
    {
        $builder = $this->db->table('peminjamans')
            ->join('users', 'users.id = peminjamans.user_id')
            ->join('pengembalians', 'pengembalians.peminjaman_id = peminjamans.id_peminjaman')
            ->join('peminjaman_has_buku', 'peminjaman_has_buku.peminjaman_id = peminjamans.id_peminjaman')
            ->join('bukus', 'bukus.id_buku = peminjaman_has_buku.buku_id')
            ->orderBy('id_peminjaman', 'DESC');
        return $builder->where(['users.id' => user()->id, 'peminjamans.status_peminjaman' => 'dipinjam'])->get()->getResult();
    }
}
