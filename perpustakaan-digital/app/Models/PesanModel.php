<?php

namespace App\Models;

use CodeIgniter\Model;

class PesanModel extends Model
{
    // protected $DBGroup          = 'default';
    protected $table            = 'pesan';
    protected $primaryKey       = 'id_pesan';
    protected $allowedFields    = ['pesan', 'user_id'];
    // protected $useAutoIncrement = true;
    // protected $insertID         = 0;
    // protected $returnType       = 'array';
    // protected $useSoftDeletes   = false;
    // protected $protectFields    = true;

    // Dates
    protected $useTimestamps = true;
    // protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // Validation
    // protected $validationRules      = [];
    // protected $validationMessages   = [];
    // protected $skipValidation       = false;
    // protected $cleanValidationRules = true;

    // Callbacks
    // protected $allowCallbacks = true;
    // protected $beforeInsert   = [];
    // protected $afterInsert    = [];
    // protected $beforeUpdate   = [];
    // protected $afterUpdate    = [];
    // protected $beforeFind     = [];
    // protected $afterFind      = [];
    // protected $beforeDelete   = [];
    // protected $afterDelete    = [];
    public function getPesan($user_id)
    {
        $builder = $this->db->table('pesan');
        if ($user_id == false) {
            return $builder->get()->getResult();
        }
        return $builder->where(['user_id' => $user_id])->orderBy('id_pesan', 'DESC')->get()->getResult();;
    }
    // public function terimaPesan()
    // {
    //     $builder = $this->db->table('pesan');
    //     return $builder->get()->getResult();
    // }
}
