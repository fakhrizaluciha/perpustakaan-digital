<?php

namespace App\Models;

use CodeIgniter\Model;

class PengembalianModel extends Model
{
    // protected $DBGroup          = 'default';
    protected $table            = 'pengembalians';
    protected $primaryKey       = 'id_pengembalian';
    protected $allowedFields    = ['user_id', 'peminjaman_id', 'tgl_kembali', 'denda', 'jml_pengembalian'];
    protected $useTimestamps = true;
    // protected $useAutoIncrement = true;
    // protected $insertID         = 0;
    // protected $returnType       = 'array';
    protected $useSoftDeletes   = true;
    // protected $protectFields    = true;

    // Dates
    // protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // Validation
    // protected $validationRules      = [];
    // protected $validationMessages   = [];
    // protected $skipValidation       = false;
    // protected $cleanValidationRules = true;

    // Callbacks
    // protected $allowCallbacks = true;
    // protected $beforeInsert   = [];
    // protected $afterInsert    = [];
    // protected $beforeUpdate   = [];
    // protected $afterUpdate    = [];
    // protected $beforeFind     = [];
    // protected $afterFind      = [];
    // protected $beforeDelete   = [];
    // protected $afterDelete    = [];
    public function getPengembalian()
    {
        $builder = $this->db->table('pengembalians')
            ->join('users', 'users.id = pengembalians.user_id')
            ->join('peminjamans', 'peminjamans.id_peminjaman = pengembalians.peminjaman_id')
            ->join('pengembalian_has_buku', 'pengembalian_has_buku.pengembalian_id = pengembalians.id_pengembalian')
            ->join('bukus', 'bukus.id_buku = pengembalian_has_buku.buku_id');
        return $builder->orderBy('id_pengembalian', 'DESC')->get()->getResult();
    }
    public function getPengembalianByTgl($tglPinjam1, $tglPinjam2)
    {
        $builder = $this->db->table('pengembalians')->join('users', 'users.id = pengembalians.user_id')->join('peminjamans', 'peminjamans.id_peminjaman = pengembalians.peminjaman_id');
        if ($tglPinjam1 == false) {
            return $builder->get()->getResult();
        }
        return $builder->where(['tgl_kembali >=' => $tglPinjam1, 'tgl_kembali <=' => $tglPinjam2])->get()->getResult();
    }
    public function getUserByIdUser($user_id)
    {
        $builder = $this->db->table('pengembalians')->join('users', 'users.id = pengembalians.user_id');
        if ($user_id == false) {
            return $this->findAll();
        }
        return $builder->where(['user_id' => $user_id])->orderBy('user_id', 'DESC')->get()->getResult();
    }
    public function getPengembalianByIdUser()
    {
        $builder = $this->db->table('pengembalians')->join('users', 'users.id = pengembalians.user_id')->join('peminjamans', 'peminjamans.id_peminjaman = pengembalians.peminjaman_id')->orderBy('id_pengembalian', 'DESC');
        return $builder->where(['user_id' => user()->id])->get()->getResult();
    }
    public function getPengembalianById($id_pengembalian = false)
    {
        $builder = $this->db->table('pengembalians')
            ->join('users', 'users.id = pengembalians.user_id')
            ->join('peminjamans', 'peminjamans.id_peminjaman = pengembalians.peminjaman_id')
            ->join('pengembalian_has_buku', 'pengembalian_has_buku.pengembalian_id = pengembalians.id_pengembalian')
            ->join('bukus', 'bukus.id_buku = pengembalian_has_buku.buku_id');;
        if ($id_pengembalian == false) {
            return $builder->get()->getResult();
        }
        return $builder->where(['id_pengembalian' => $id_pengembalian])->get()->getResult();
    }
    public function getTotalDendaByTgl($tglPinjam1, $tglPinjam2)
    {
        $builder = $this->db->table('peminjamans')
            ->select("sum(denda) as total")
            ->join('users', 'users.id = peminjamans.user_id')
            ->join('pengembalians', 'pengembalians.peminjaman_id = peminjamans.id_peminjaman')
            ->join('peminjaman_has_buku', 'peminjaman_has_buku.peminjaman_id = peminjamans.id_peminjaman')
            ->join('bukus', 'bukus.id_buku = peminjaman_has_buku.buku_id');;
        if ($tglPinjam1 == false) {
            return $builder->get()->getResult();
        }
        return $builder->where(['tgl_pinjam >=' => $tglPinjam1, 'tgl_pinjam <=' => $tglPinjam2])->get()->getResult();
    }
    public function getTotalJumlahByTgl($tglPinjam1, $tglPinjam2)
    {
        $builder = $this->db->table('peminjamans')
            ->select("sum(jml_pengembalian) as total")
            ->join('users', 'users.id = peminjamans.user_id')
            ->join('pengembalians', 'pengembalians.peminjaman_id = peminjamans.id_peminjaman')
            ->join('peminjaman_has_buku', 'peminjaman_has_buku.peminjaman_id = peminjamans.id_peminjaman')
            ->join('bukus', 'bukus.id_buku = peminjaman_has_buku.buku_id');;
        if ($tglPinjam1 == false) {
            return $builder->get()->getResult();
        }
        return $builder->where(['tgl_pinjam >=' => $tglPinjam1, 'tgl_pinjam <=' => $tglPinjam2])->get()->getResult();
    }
    public function getTotalDendaByTglAndStatus($tglPinjam1, $tglPinjam2, $status_peminjaman)
    {
        $builder = $this->db->table('peminjamans')
            ->select("sum(denda) as total")
            ->join('users', 'users.id = peminjamans.user_id')
            ->join('pengembalians', 'pengembalians.peminjaman_id = peminjamans.id_peminjaman')
            ->join('peminjaman_has_buku', 'peminjaman_has_buku.peminjaman_id = peminjamans.id_peminjaman')
            ->join('bukus', 'bukus.id_buku = peminjaman_has_buku.buku_id');;
        if ($tglPinjam1 == false) {
            return $builder->get()->getResult();
        }
        return $builder->where(['tgl_pinjam >=' => $tglPinjam1, 'tgl_pinjam <=' => $tglPinjam2, 'status_peminjaman' => $status_peminjaman])->get()->getResult();
    }
    public function getTotalJumlahByTglAndStatus($tglPinjam1, $tglPinjam2, $status_peminjaman)
    {
        $builder = $this->db->table('peminjamans')
            ->select("sum(jml_pengembalian) as total")
            ->join('users', 'users.id = peminjamans.user_id')
            ->join('pengembalians', 'pengembalians.peminjaman_id = peminjamans.id_peminjaman')
            ->join('peminjaman_has_buku', 'peminjaman_has_buku.peminjaman_id = peminjamans.id_peminjaman')
            ->join('bukus', 'bukus.id_buku = peminjaman_has_buku.buku_id');;
        if ($tglPinjam1 == false) {
            return $builder->get()->getResult();
        }
        return $builder->where(['tgl_pinjam >=' => $tglPinjam1, 'tgl_pinjam <=' => $tglPinjam2, 'status_peminjaman' => $status_peminjaman])->get()->getResult();
    }
}
