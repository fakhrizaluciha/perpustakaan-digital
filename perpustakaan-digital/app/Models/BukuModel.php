<?php

namespace App\Models;

use CodeIgniter\Model;

class BukuModel extends Model
{
    // protected $DBGroup          = 'default';
    protected $table            = 'bukus';
    protected $primaryKey       = 'id_buku';
    protected $allowedFields    = ['kategori_id', 'kode_tempat', 'gambar', 'judul_buku', 'stok', 'penulis', 'penerbit', 'uraian', 'tahun_terbit', 'isbn', 'sumber'];
    // protected $useAutoIncrement = true;
    // protected $insertID         = 0;
    // protected $returnType       = 'array';
    protected $useSoftDeletes   = true;
    // protected $protectFields    = true;

    // Dates
    protected $useTimestamps = true;
    // protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // Validation
    // protected $validationRules      = [];
    // protected $validationMessages   = [];
    // protected $skipValidation       = false;
    // protected $cleanValidationRules = true;

    // Callbacks
    // protected $allowCallbacks = true;
    // protected $beforeInsert   = [];
    // protected $afterInsert    = [];
    // protected $beforeUpdate   = [];
    // protected $afterUpdate    = [];
    // protected $beforeFind     = [];
    // protected $afterFind      = [];
    // protected $beforeDelete   = [];
    // protected $afterDelete    = [];
    public function getBuku($keyword = null, $kategori = null)
    {
        $builder = $this->db->table('bukus')->join('kategoris', 'kategoris.id_kategori = bukus.kategori_id')->orderBy('id_buku', 'DESC');
        // if ($keyword != '' && $kategori != '') {
        //     $builder->like('judul_buku', $keyword);
        //     $builder->like('kategoris.kategori', $keyword);
        // }
        return $builder->where(['deleted_at' => null])->get()->getResult();
    }
    public function getSampahBuku($keyword = null)
    {
        $builder = $this->db->table('bukus')->join('kategoris', 'kategoris.id_kategori = bukus.kategori_id')->orderBy('id_buku', 'DESC');
        if ($keyword != '') {
            $builder->like('judul_buku', $keyword);
            $builder->orLike('kategori', $keyword);
        }
        return $builder->where(['deleted_at !=' => null])->get()->getResult();
    }
    public function getBukuById($id_buku = false)
    {
        if ($id_buku == false) {
            return $this->findAll();
        }
        return $this->join('kategoris', 'kategoris.id_kategori = buku.kategori_id')->where(['id_buku' => $id_buku, 'deleted_at' => null])->first();
    }
    public function getBukuByJudul($judul_buku = false)
    {
        if ($judul_buku == false) {
            return $this->findAll();
        }
        return $this->where(['judul_buku' => $judul_buku, 'deleted_at' => null])->first();
    }
    public function getBukuByKategori($kategori_id = false, $keyword = null)
    {
        $builder = $this->db->table('bukus')->join('kategoris', 'kategoris.id_kategori = bukus.kategori_id')->orderBy('id_buku', 'DESC');
        if ($keyword != '') {
            $builder->like('judul_buku', $keyword);
            $builder->orLike('kategori', $keyword);
        }
        if ($kategori_id == false) {
            return $this->findAll();
        }
        return $this->where(['kategori_id' => $kategori_id, 'deleted_at' => null])->get()->getResult();
    }
    public function doubleSearch($keyword, $kategori)
    {
        $builder = $this->table('bukus')
            // ->select('bukus.judul_buku, bukus.kategori_id, kategoris.id_kategori, kategoris.kategori, bukus.gambar, bukus.isbn, bukus.kode_tempat,bukus.penulis,bukus.penerbit,bukus.stok,bukus.deleted_at,bukus.created_at,bukus.updated_at,bukus.tahun_terbit')
            ->join('kategoris', 'kategoris.id_kategori = bukus.kategori_id')->orderBy('bukus.id_buku', 'DESC');
        $builder->like('bukus.judul_buku', $keyword);
        $builder->like('kategoris.kategori', $kategori);
        // return $builder->where('judul_buku', 'like', '%' . $keyword . '%')->where('kategoris.kategori', 'like', '%' . $kategori . '%');
        return $builder->where(['bukus.deleted_at' => null]);
    }

    public function searchJudul($keyword)
    {
        $builder = $this->table('bukus')->join('kategoris', 'kategoris.id_kategori = bukus.kategori_id')->orderBy('id_buku', 'DESC');
        $builder->like('judul_buku', $keyword);
        return $builder->where(['deleted_at' => null]);
    }

    public function searchKategori($kategori)
    {
        $builder = $this->table('bukus')->join('kategoris', 'kategoris.id_kategori = bukus.kategori_id')->orderBy('id_buku', 'DESC');
        $builder->like('kategori', $kategori);
        return $builder->where(['deleted_at' => null]);
    }
}
