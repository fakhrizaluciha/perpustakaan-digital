<?php

namespace App\Models;

use App\Controllers\User;
use CodeIgniter\Model;

class PinjamModel extends Model
{
    // protected $DBGroup          = 'default';
    protected $table            = 'pinjam';
    protected $primaryKey       = 'id_pinjam';
    protected $allowedFields    = ['user_id', 'buku_id', 'status', 'tgl_pinjam', 'tgl_balik', 'tgl_kembali', 'denda'];
    // protected $useAutoIncrement = true;
    // protected $insertID         = 0;
    // protected $returnType       = 'array';
    // protected $useSoftDeletes   = false;
    // protected $protectFields    = true;

    // Dates
    protected $useTimestamps = false;
    // protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // // Validation
    // protected $validationRules      = [];
    // protected $validationMessages   = [];
    // protected $skipValidation       = false;
    // protected $cleanValidationRules = true;

    // Callbacks
    // protected $allowCallbacks = true;
    // protected $beforeInsert   = [];
    // protected $afterInsert    = [];
    // protected $beforeUpdate   = [];
    // protected $afterUpdate    = [];
    // protected $beforeFind     = [];
    // protected $afterFind      = [];
    // protected $beforeDelete   = [];
    // protected $afterDelete    = [];
    public function getPinjam()
    {
        $builder = $this->db->table('pinjam')->join('users', 'users.id = pinjam.user_id')->join('buku', 'buku.id_buku = pinjam.buku_id');
        return $builder->orderBy('id_pinjam', 'DESC')->get()->getResult();
    }
    public function getPinjamByTgl($tglPinjam1, $tglPinjam2)
    {
        $builder = $this->db->table('pinjam')->join('users', 'users.id = pinjam.user_id')->join('buku', 'buku.id_buku = pinjam.buku_id');
        if ($tglPinjam1 == false) {
            return $builder->get()->getResult();
        }
        return $builder->where(['tgl_pinjam >=' => $tglPinjam1, 'tgl_pinjam <=' => $tglPinjam2])->get()->getResult();
    }
    public function getPinjamById($id_pinjam = false)
    {
        $builder = $this->db->table('pinjam')->join('users', 'users.id = pinjam.user_id')->join('buku', 'buku.id_buku = pinjam.buku_id');
        if ($id_pinjam == false) {
            return $builder->get()->getResult();
        }
        return $builder->where(['id_pinjam' => $id_pinjam])->get()->getResult();
    }
    public function getPinjamByIdUser()
    {
        $builder = $this->db->table('pinjam')->join('users', 'users.id = pinjam.user_id')->join('buku', 'buku.id_buku = pinjam.buku_id')->orderBy('id_pinjam', 'DESC');
        return $builder->where(['user_id' => user()->id])->get()->getResult();
    }
}
