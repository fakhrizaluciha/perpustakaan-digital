<?php

namespace App\Models;

use CodeIgniter\Model;

class VideoModel extends Model
{
    // protected $DBGroup          = 'default';
    protected $table            = 'video';
    protected $primaryKey       = 'id_video';
    protected $allowedFields    = ['judul_video', 'penerbit', 'tahun_terbit', 'video', 'url'];
    // protected $useAutoIncrement = true;
    // protected $insertID         = 0;
    // protected $returnType       = 'array';
    protected $useSoftDeletes   = true;
    // protected $protectFields    = true;

    // Dates
    protected $useTimestamps = true;
    // protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // Validation
    // protected $validationRules      = [];
    // protected $validationMessages   = [];
    // protected $skipValidation       = false;
    // protected $cleanValidationRules = true;

    // Callbacks
    // protected $allowCallbacks = true;
    // protected $beforeInsert   = [];
    // protected $afterInsert    = [];
    // protected $beforeUpdate   = [];
    // protected $afterUpdate    = [];
    // protected $beforeFind     = [];
    // protected $afterFind      = [];
    // protected $beforeDelete   = [];
    // protected $afterDelete    = [];
    public function getVideo($keyword = null)
    {
        $builder = $this->db->table('video')->orderBy('id_video', 'DESC');
        if ($keyword != null) {
            $builder->like('judul_video', $keyword);
            // $builder->orLike('kategori', $keyword);
        }
        return $builder->where(['deleted_at' => null])->get()->getResult();
    }
    public function getSampahVideo($keyword = null)
    {
        $builder = $this->db->table('video')->orderBy('id_video', 'DESC');
        if ($keyword != null) {
            $builder->like('judul_video', $keyword);
            // $builder->orLike('kategori', $keyword);
        }
        return $builder->where(['deleted_at !=' => null])->get()->getResult();
    }
    public function getVideoById($id_video = false)
    {
        if ($id_video == false) {
            return $this->findAll();
        }
        return $this->where(['id_video' => $id_video, 'deleted_at' => null])->first();
    }
    public function search($keyword)
    {
        $builder = $this->table('video');
        $builder->like('judul_video', $keyword);
        return $builder->where(['deleted_at' => null]);
    }
}
