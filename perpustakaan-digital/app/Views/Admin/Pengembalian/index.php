<?php if (in_groups('Admin')) : ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Data Peminjaman</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="<?= base_url() ?>/min/css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>

    <body class="sb-nav-fixed">
        <div id="layoutSidenav">
            <?= $this->include('Admin/Layout/topbar') ?>
            <?= $this->include('Admin/Layout/sidebar') ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Data Pengembalian Buku</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Data Pengembalian</li>
                        </ol>
                        <form action="/admin/data-pengembalian/cetak" method="post">
                            <div class="row mt-1">
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" name="tglPinjam1" required>
                                </div>
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" name="tglPinjam2" required>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-success">Cetak</button>
                                </div>
                            </div>
                        </form>
                        <a style="margin-top: 20px;" href="/admin/tambah-pengembalian" class="btn btn-primary mb-3">Tambah Pengembalian</a>
                        <?php if (session()->getFlashdata('pesan')) : ?>
                            <div class="alert alert-success" role="alert">
                                <?= session()->getFlashdata('pesan') ?>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="container">
                        <div class="card-body">
                            <table id="datatablesSimple">
                                <thead>
                                    <tr class="table-info">
                                        <th scope="col">No.</th>
                                        <th scope="col">Nama Anggota</th>
                                        <th scope="col">Judul Buku</th>
                                        <th scope="col">Tanggal Pengembalian</th>
                                        <th scope="col">Denda</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    <?php foreach ($pengembalian as $p) : ?>
                                        <th scope="row"><?= $i++ ?></th>
                                        <td><?= $p->username ?></td>
                                        <td><?= $p->judul_buku ?></td>
                                        <td>
                                            <?php
                                            if (date('d F Y', strtotime($p->tgl_kembali)) == "31 December 1969") {
                                            } else {
                                                echo date('d F Y', strtotime($p->tgl_kembali));
                                            }
                                            ?>
                                        </td>
                                        <td>Rp <?= $p->denda ?></td>
                                        <td>
                                            <form action="/admin/hapus-pengembalian/<?= $p->id_pengembalian ?>" method="post" class="d-inline" onsubmit="return confirm('Yakin akan hapus data anda?')">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button class="btn btn-danger btn-sm"><i class="fa-solid fa-trash"></i> Hapus</button>
                                            </form>
                                            <form action="/admin/edit-pengembalian/<?= $p->id_pengembalian ?>" class="d-inline">
                                                <button class="btn btn-success btn-sm"><i class="fa-solid fa-pen-to-square"></i> Edit</button>
                                            </form>
                                            <form action="/admin/pesan/<?= $p->user_id ?>" class="d-inline">
                                                <button class="btn btn-primary btn-sm"><i class="fa-solid fa-envelope"></i> Pesan</button>
                                            </form>
                                        </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website <?= date('Y') ?></div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/assets/demo/chart-area-demo.js"></script>
        <script src="<?= base_url() ?>/assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/datatables-simple-demo.js"></script>
    </body>

    </html>
<?php endif; ?>