<?php if (in_groups('Admin')) : ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Edit Video</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="<?= base_url() ?>/min/css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>

    <body class="sb-nav-fixed">
        <div id="layoutSidenav">
            <?= $this->include('Admin/Layout/topbar') ?>
            <?= $this->include('Admin/Layout/sidebar') ?>

            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Edit Video</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active"></li>
                        </ol>
                        <form action="/admin/video/<?= $video['id_video'] ?>" method="POST" enctype="multipart/form-data">
                            <?= csrf_field() ?>
                            <input type="hidden" name="id_video" value="<?= $video['id_video'] ?>">
                            <div class="row mb-3">
                                <label for="judul_video" class="col-sm-2 col-form-label">Judul Video</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control <?= ($validation->hasError('judul_video')) ? 'is-invalid' : '' ?>" id="judul_video" name="judul_video" value="<?= (old('judul_video')) ? old('judul_video') :  $video['judul_video'] ?>">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        <?= $validation->getError('judul_video') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="penerbit" class="col-sm-2 col-form-label">Penerbit</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control <?= ($validation->hasError('penerbit')) ? 'is-invalid' : '' ?>" id="penerbit" name="penerbit" value="<?= (old('penerbit')) ? old('penerbit') :  $video['penerbit'] ?>">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        Penerbit tidak boleh kosong.
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="tahun_terbit" class="col-sm-2 col-form-label">Tahun Terbit</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control <?= ($validation->hasError('tahun_terbit')) ? 'is-invalid' : '' ?>" id="tahun_terbit" name="tahun_terbit" value="<?= (old('tahun_terbit')) ? old('tahun_terbit') :  $video['tahun_terbit'] ?>">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        Tahun Terbit tidak boleh kosong.
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="url" class="col-sm-2 col-form-label">Url</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control <?= ($validation->hasError('url')) ? 'is-invalid' : '' ?>" id="url" name="url" value="<?= (old('url')) ? old('url') : $video['url'] ?>">
                                </div>
                            </div>
                            <input type="hidden" name="videoLama" id="videoLama" value="<?= $video['video'] ?>">
                            <div class="row mb-3">
                                <label for="video" class="col-sm-2 col-form-label">Video</label>
                                <div class="col-sm-8">
                                    <div class="input-group mb-3">
                                        <input type="file" class="form-control <?= ($validation->hasError('video')) ? 'is-invalid' : '' ?>" id="video" name="video" onchange="preview()">
                                        <div id="validationServer03Feedback" class="invalid-feedback">
                                            <?= $validation->getError('video') ?>
                                        </div>
                                        <label class="input-group-text" for="video">Upload</label>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </form>
                    </div>
                    <!-- <div class="container-fluid px-4">
                        <h1 class="mt-4">Data Buku</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Data Buku</li>
                        </ol>
                        <a href="/admin/create-buku" class="btn btn-primary mb-3">Tambah Buku</a>
                        <?php if (session()->getFlashdata('pesan')) : ?>
                            <div class="alert alert-success" role="alert">
                                <?= session()->getFlashdata('pesan') ?>
                            </div>
                        <?php endif ?>
                    </div> -->
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website <?= date('Y') ?></div>
                            <!-- <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div> -->
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/assets/demo/chart-area-demo.js"></script>
        <script src="<?= base_url() ?>/assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/datatables-simple-demo.js"></script>
    </body>

    </html>
<?php endif; ?>