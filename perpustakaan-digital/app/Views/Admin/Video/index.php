<?php if (in_groups('Admin')) : ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Data Video</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="<?= base_url() ?>/min/css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>

    <body class="sb-nav-fixed">
        <div id="layoutSidenav">
            <?= $this->include('Admin/Layout/topbar') ?>
            <?= $this->include('Admin/Layout/sidebar') ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Data Video</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Data Video</li>
                        </ol>
                        <a href="/admin/video/tambah" class="btn btn-primary mb-3">Tambah Video</a>
                        <a href="/admin/video/sampah" class="btn btn-danger mb-3"><i class="fa-solid fa-trash"></i> Sampah</a>
                        <?php if (session()->getFlashdata('pesan')) : ?>
                            <div class="alert alert-success" role="alert">
                                <?= session()->getFlashdata('pesan') ?>
                            </div>
                        <?php endif ?>
                    </div>
                    <!-- <div class="container"> -->
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr class="table-info">
                                    <th scope="col">No.</th>
                                    <th scope="col">Video</th>
                                    <th scope="col">Url</th>
                                    <th scope="col">Judul Video</th>
                                    <th scope="col">Penerbit</th>
                                    <th scope="col">Tahun Terbit</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1 ?>
                                <?php foreach ($video as $k) : ?>
                                    <tr>
                                        <th scope="row"><?= $i++ ?></th>
                                        <td><a style="text-decoration:none" href="<?= base_url() ?>/video/<?= $k->video ?>">
                                                <?php if ($k->video == 'undraw_profile.svg') : ?>
                                                <?php else : ?>
                                                    Lihat video
                                                <?php endif; ?></a></td>
                                        <td><a style="text-decoration:none" href="<?= $k->url ?>">
                                                <?php if (!$k->url) : ?>
                                                <?php else : ?>
                                                    Link video
                                                <?php endif; ?></a></td>
                                        <td><?= $k->judul_video ?></td>
                                        <td><?= $k->penerbit ?></td>
                                        <td><?= $k->tahun_terbit ?></td>
                                        <td>
                                            <form action="/admin/video/<?= $k->id_video ?>" method="post" class="d-inline" onsubmit="return confirm('Yakin akan hapus data anda?')">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button class="btn btn-danger btn-sm" style="margin-bottom: 5px;"><i class="fa-solid fa-trash"></i></button>
                                            </form>
                                            <form action="/admin/video/<?= $k->id_video ?>" class="d-inline">
                                                <button class="btn btn-success btn-sm" style="margin-bottom: 5px;"><i class="fa-solid fa-pen-to-square"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- </div> -->
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website <?= date('Y') ?></div>
                            <!-- <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div> -->
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/assets/demo/chart-area-demo.js"></script>
        <script src="<?= base_url() ?>/assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/datatables-simple-demo.js"></script>
    </body>

    </html>
<?php endif; ?>