<?php if (in_groups('Admin')) : ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Data Peminjaman</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="<?= base_url() ?>/min/css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>

    <body class="sb-nav-fixed" onload="print()">
        <div class="container">
            <div class="row">
                <div>
                    <?php if ($_POST['status_peminjaman'] == 'dipinjam') : ?>
                        <h1>Laporan Peminjaman</h1>
                    <?php elseif ($_POST['status_peminjaman'] == 'dikembalikan') : ?>
                        <h1>Laporan Pengembalian</h1>
                    <?php endif; ?>
                </div>
                <hr>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Judul Buku</th>
                                <th scope="col">NIS</th>
                                <th scope="col">Tanggal Peminjaman</th>
                                <th scope="col">Batas Pengembalian</th>
                                <th scope="col">Tanggal Pengembalian</th>
                                <th scope="col">Jumlah</th>
                                <?php if ($_POST['status_peminjaman'] == 'dipinjam') : ?>
                                <?php else : ?>
                                    <th scope="col">Denda</th>
                                <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1 ?>
                            <?php foreach ($pinjam as $k) : ?>
                                <tr>
                                    <th scope="row"><?= $i++ ?></th>
                                    <td><?= $k->username ?></td>
                                    <td><?= $k->judul_buku ?></td>
                                    <td><?= $k->nis ?></td>
                                    <td><?= date('d F Y', strtotime($k->tgl_pinjam))  ?></td>
                                    <td><?= date('d F Y', strtotime($k->deadline)) ?></td>
                                    <td><?php if ($k->tgl_kembali == '0000-00-00') : ?>
                                        <?php else : ?>
                                            <?= date('d F Y', strtotime($k->tgl_kembali)) ?>
                                        <?php endif; ?>
                                    </td>
                                    <?php if ($_POST['status_peminjaman'] == 'dipinjam') : ?>
                                        <td><?= $k->qty ?></td>
                                    <?php elseif ($_POST['status_peminjaman'] == 'dikembalikan') : ?>
                                        <td><?= $k->jml_pengembalian ?></td>
                                    <?php endif; ?>
                                    <?php if ($_POST['status_peminjaman'] == 'dipinjam') : ?>
                                    <?php else : ?>
                                        <td><?= $k->denda ?></td>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                        </tbody>
                        <tbody>
                            <tr>
                                <th>Total:</th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <?php if ($_POST['status_peminjaman'] == 'dipinjam') : ?>
                                    <td><?= $totalQtyDipinjam[0]->total ?></td>
                                <?php else : ?>
                                    <td><?= $totalQtyDikembalikan[0]->total ?></td>
                                <?php endif; ?>
                                <?php if ($_POST['status_peminjaman'] == 'dipinjam') : ?>
                                <?php else : ?>
                                    <td><?= $total[0]->total ?></td>
                                <?php endif; ?>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/assets/demo/chart-area-demo.js"></script>
        <script src="<?= base_url() ?>/assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/datatables-simple-demo.js"></script>
    </body>

    </html>
<?php endif; ?>