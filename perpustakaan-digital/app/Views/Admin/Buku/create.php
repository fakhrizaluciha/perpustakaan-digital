<?php if (in_groups('Admin')) : ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Create Buku</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="<?= base_url() ?>/min/css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>

    <body class="sb-nav-fixed">
        <div id="layoutSidenav">
            <?= $this->include('Admin/Layout/topbar') ?>
            <?= $this->include('Admin/Layout/sidebar') ?>

            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Tambah Buku</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active"></li>
                        </ol>
                        <form action="/admin/buku" method="POST" enctype="multipart/form-data">
                            <?= csrf_field() ?>
                            <input type="hidden" class="form-control" id="created_at" name="created_at" autofocus value="<?= date('Y-m-d') ?>">
                            <div class="row mb-3">
                                <label for="judul_buku" class="col-sm-2 col-form-label">Judul Buku</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control <?= ($validation->hasError('judul_buku')) ? 'is-invalid' : '' ?>" id="judul_buku" name="judul_buku" autofocus value="<?= old('judul_buku') ?>">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        <?= $validation->getError('judul_buku') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="kode_tempat" class="col-sm-2 col-form-label">Kode Tempat</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control <?= ($validation->hasError('kode_tempat')) ? 'is-invalid' : '' ?>" id="kode_tempat" name="kode_tempat" autofocus value="<?= old('kode_tempat') ?>">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        Kode Tempat tidak boleh kosong
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="stok" class="col-sm-2 col-form-label">Stok</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control <?= ($validation->hasError('stok')) ? 'is-invalid' : '' ?>" id="stok" name="stok" autofocus value="<?= 0 ?>">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        Stok tidak boleh kosong
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="kategori_id" class="col-sm-2 col-form-label">Kategori</label>
                                <div class="col-sm-10">
                                    <select class="form-select" id="kategori_id" name="kategori_id">
                                        <?php foreach ($kategori as $b) : ?>
                                            <option value="<?= $b->id_kategori ?>"><?= $b->kategori ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <!-- <div class="col-sm-10">
                                    <input type="text" class="form-control <?= ($validation->hasError('judul_buku')) ? 'is-invalid' : '' ?>" id="judul_buku_buku" name="judul_buku_buku" autofocus value="<?= old('judul_buku') ?>">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        <?= $validation->getError('judul_buku') ?>
                                    </div>
                                </div> -->
                            </div>
                            <div class="row mb-3">
                                <label for="penulis" class="col-sm-2 col-form-label">Penulis</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="penulis" name="penulis" value="<?= old('penulis') ?>">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="uraian" class="col-sm-2 col-form-label">Abstrak</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control <?= ($validation->hasError('uraian')) ? 'is-invalid' : '' ?>" id="uraian" name="uraian" value="<?= old('uraian') ?>">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        Abstrak tidak boleh kosong.
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="tahun_terbit" class="col-sm-2 col-form-label">Tahun Terbit</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="tahun_terbit" name="tahun_terbit" value="<?= old('tahun_terbit') ?>">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="isbn" class="col-sm-2 col-form-label">ISBN</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="isbn" name="isbn" value="<?= old('isbn') ?>">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="sumber" class="col-sm-2 col-form-label">Sumber</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="sumber" name="sumber" value="<?= old('sumber') ?>">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="penerbit" class="col-sm-2 col-form-label">Penerbit</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="penerbit" name="penerbit" value="<?= old('penerbit') ?>">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="gambar" class="col-sm-2 col-form-label">Sampul</label>
                                <div class="col-sm-2">
                                    <img src="/img/undraw_profile.svg" class="img-thumbnail img-preview">
                                </div>
                                <div class="col-sm-8">
                                    <div class="input-group mb-3">
                                        <input type="file" class="form-control <?= ($validation->hasError('gambar')) ? 'is-invalid' : '' ?>" id="gambar" name="gambar" onchange="preview()">
                                        <div id="validationServer03Feedback" class="invalid-feedback">
                                            <?= $validation->getError('gambar') ?>
                                        </div>
                                        <label class="input-group-text" for="gambar">Upload</label>
                                    </div>
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        <?= $validation->getError('gambar') ?>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </form>
                    </div>
                    <!-- <div class="container-fluid px-4">
                        <h1 class="mt-4">Data Buku</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Data Buku</li>
                        </ol>
                        <a href="/admin/create-buku" class="btn btn-primary mb-3">Tambah Buku</a>
                        <?php if (session()->getFlashdata('pesan')) : ?>
                            <div class="alert alert-success" role="alert">
                                <?= session()->getFlashdata('pesan') ?>
                            </div>
                        <?php endif ?>
                    </div> -->
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website <?= date('Y') ?></div>
                            <!-- <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div> -->
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/assets/demo/chart-area-demo.js"></script>
        <script src="<?= base_url() ?>/assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/datatables-simple-demo.js"></script>
    </body>

    </html>
<?php endif; ?>