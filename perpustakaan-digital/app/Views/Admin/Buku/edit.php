<?php if (in_groups('Admin')) : ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Edit Buku</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="<?= base_url() ?>/min/css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>

    <body class="sb-nav-fixed">
        <div id="layoutSidenav">
            <?= $this->include('Admin/Layout/topbar') ?>
            <?= $this->include('Admin/Layout/sidebar') ?>

            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Edit Buku</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active"></li>
                        </ol>
                        <form action="/admin/buku/<?= $buku['id_buku'] ?>" method="POST" enctype="multipart/form-data">
                            <?= csrf_field() ?>
                            <input type="hidden" name="id_buku" value="<?= $buku['id_buku'] ?>">
                            <div class="row mb-3">
                                <label for="judul_buku" class="col-sm-2 col-form-label">Judul Buku</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control <?= ($validation->hasError('judul_buku')) ? 'is-invalid' : '' ?>" id="judul_buku" name="judul_buku" value="<?= (old('judul_buku')) ? old('judul_buku') : $buku['judul_buku'] ?>">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        <?= $validation->getError('judul_buku') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="kode_tempat" class="col-sm-2 col-form-label">Kode Tempat</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control <?= ($validation->hasError('kode_tempat')) ? 'is-invalid' : '' ?>" id="kode_tempat" name="kode_tempat" autofocus value="<?= (old('kode_tempat')) ? old('kode_tempat') : $buku['kode_tempat'] ?>">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        Kode Tempat tidak boleh kosong
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="stok" class="col-sm-2 col-form-label">Stok</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control <?= ($validation->hasError('stok')) ? 'is-invalid' : '' ?>" id="stok" name="stok" autofocus value="<?= (old('stok')) ? old('stok') : $buku['stok'] ?>">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        Stok tidak boleh kosong
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="kategori_id" id="kategori_id" value="<?= $buku['kategori_id'] ?>">
                            <div class="row mb-3">
                                <label for="kategori_id" class="col-sm-2 col-form-label">Kategori</label>
                                <div class="col-sm-10">
                                    <select class="form-select" id="kategori_id" name="kategori_id" value="<?= (old('kategori_id')) ? old('kategori_id') : $buku['kategori_id'] ?>">
                                        <?php foreach ($kategori as $b) : ?>
                                            <option value="<?= $b->id_kategori ?>"><?= $b->kategori ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <small class="form-text text-muted">Mohon untuk mengisi kategori lagi</small>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="penulis" class="col-sm-2 col-form-label">Penulis</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="penulis" name="penulis" value="<?= (old('penulis')) ? old('penulis') : $buku['penulis'] ?>">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="uraian" class="col-sm-2 col-form-label">Abstrak</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="uraian" name="uraian" value="<?= (old('uraian')) ? old('uraian') : $buku['uraian'] ?>">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="tahun_terbit" class="col-sm-2 col-form-label">Tahun Terbit</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="tahun_terbit" name="tahun_terbit" value="<?= (old('tahun_terbit')) ? old('tahun_terbit') : $buku['tahun_terbit'] ?>">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="isbn" class="col-sm-2 col-form-label">ISBN</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="isbn" name="isbn" value="<?= (old('isbn')) ? old('isbn') : $buku['isbn'] ?>">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="sumber" class="col-sm-2 col-form-label">Sumber</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="sumber" name="sumber" value="<?= (old('sumber')) ? old('sumber') : $buku['sumber'] ?>">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="penerbit" class="col-sm-2 col-form-label">Penerbit</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="penerbit" name="penerbit" value="<?= (old('penerbit')) ? old('penerbit') : $buku['penerbit'] ?>">
                                </div>
                            </div>
                            <input type="hidden" name="gambarLama" id="gambarLama" value="<?= $buku['gambar'] ?>">
                            <div class="row mb-3">
                                <label for="gambar" class="col-sm-2 col-form-label">Sampul</label>
                                <div class="col-sm-2">
                                    <img src="<?= base_url() ?>/img/<?= (old('gambar')) ? old('gambar') : $buku['gambar'] ?>" class="img-thumbnail img-preview">
                                </div>
                                <div class="col-sm-8">
                                    <div class="input-group mb-3">
                                        <input type="file" class="form-control <?= ($validation->hasError('gambar')) ? 'is-invalid' : '' ?>" id="gambar" name="gambar" onchange="preview()" value="<?= base_url() ?>/img/<?= (old('gambar')) ? old('gambar') : $buku['gambar'] ?>">
                                        <div id="validationServer03Feedback" class="invalid-feedback">
                                            <?= $validation->getError('gambar') ?>
                                        </div>
                                        <label class="input-group-text" for="gambar">Upload</label>
                                    </div>
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        <?= $validation->getError('gambar') ?>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </form>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website <?= date('Y') ?></div>
                            <!-- <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div> -->
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <!-- <script>
            function preview() {
                const gambar = document.querySelector('#gambar')
                const gambarLabel = document.querySelector('.custom-file-label')
                const gambarPreview = document.querySelector('.img-preview')
                gambarLabel.textContent = sampul.files[0].name
                const fileGambar = new FileReader()
                fileGambar.readAsDataURL(gambar.files[0])
                fileGambar.onload = function(e) {
                    gambarPreview.src = e.target.result
                }
            }
        </script> -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/assets/demo/chart-area-demo.js"></script>
        <script src="<?= base_url() ?>/assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/datatables-simple-demo.js"></script>
    </body>

    </html>
<?php endif; ?>