<?php if (in_groups('Admin')) : ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Data Buku</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="<?= base_url() ?>/min/css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>

    <body class="sb-nav-fixed">
        <div id="layoutSidenav">
            <?= $this->include('Admin/Layout/topbar') ?>
            <?= $this->include('Admin/Layout/sidebar') ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Data Buku</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Data Buku</li>
                        </ol>
                        <a href="/admin/buku/tambah" class="btn btn-primary mb-3">Tambah Buku</a>
                        <a href="/admin/buku/sampah" class="btn btn-danger mb-3"><i class="fa-solid fa-trash"></i> Sampah</a>
                        <?php if (session()->getFlashdata('pesan')) : ?>
                            <div class="alert alert-success" role="alert">
                                <?= session()->getFlashdata('pesan') ?>
                            </div>
                        <?php endif ?>
                    </div>
                    <!-- <div class="container"> -->
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr class="table-info">
                                    <th scope="col">No.</th>
                                    <th scope="col">Gambar</th>
                                    <th scope="col">Judul Buku</th>
                                    <th scope="col">Stok</th>
                                    <th scope="col">Kategori</th>
                                    <th scope="col">Penulis</th>
                                    <th scope="col">Penerbit</th>
                                    <th scope="col">Uraian</th>
                                    <th scope="col">Tahun Terbit</th>
                                    <th scope="col">ISBN</th>
                                    <th scope="col">Sumber</th>
                                    <th scope="col">Kode tempat</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1 ?>
                                <?php foreach ($buku as $k) : ?>
                                    <tr>
                                        <th scope="row"><?= $i++ ?></th>
                                        <?php if ($k->gambar == NULL) : ?>
                                            <td>NULL</td>
                                        <?php else : ?>
                                            <td><img src="<?= base_url() ?>/img/<?= $k->gambar ?>" alt="" width="100px"></td>
                                        <?php endif; ?>
                                        <td><?= $k->judul_buku ?></td>
                                        <td><?= $k->stok ?></td>
                                        <td><?= $k->kategori ?></td>
                                        <td><?= $k->penulis ?></td>
                                        <td><?= $k->penerbit ?></td>
                                        <td><?= $k->uraian ?></td>
                                        <td>
                                            <?php if ($k->tahun_terbit == 0) : ?>
                                            <?php else : ?>
                                                <?= $k->tahun_terbit ?>
                                            <?php endif; ?>
                                        </td>
                                        <td><?= $k->isbn ?></td>
                                        <td><?= $k->sumber ?></td>
                                        <td><?= $k->kode_tempat ?></td>
                                        <td>
                                            <form action="/admin/buku/<?= $k->id_buku ?>" method="post" class="d-inline" onsubmit="return confirm('Yakin akan hapus data anda?')">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button class="btn btn-danger btn-sm" style="margin-bottom: 5px;"><i class="fa-solid fa-trash"></i></button>
                                            </form>
                                            <form action="/admin/buku/edit/<?= $k->id_buku ?>" class="d-inline">
                                                <button class="btn btn-success btn-sm"><i class="fa-solid fa-pen-to-square"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- </div> -->
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website <?= date('Y') ?></div>
                            <!-- <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div> -->
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/assets/demo/chart-area-demo.js"></script>
        <script src="<?= base_url() ?>/assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/datatables-simple-demo.js"></script>
    </body>

    </html>
<?php endif; ?>