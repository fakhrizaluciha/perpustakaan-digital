<?php if (in_groups('Admin')) : ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Data Recycle Bin Peminjaman</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="<?= base_url() ?>/min/css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>

    <body class="sb-nav-fixed">
        <div id="layoutSidenav">
            <?= $this->include('Admin/Layout/topbar') ?>
            <?= $this->include('Admin/Layout/sidebar') ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Data Recycle Bin Peminjaman Buku</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Data Recycle Bin Peminjaman</li>
                        </ol>
                        <!-- <form action="/admin/peminjaman/cetak" method="post">
                            <div class="row mt-1">
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" name="tglPinjam1" style="margin-bottom: 5px;" required>
                                </div>
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" name="tglPinjam2" style="margin-bottom: 5px;" required>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-success">Cetak</button>
                                </div>
                            </div>
                        </form> -->
                        <?php if (session()->getFlashdata('pesan')) : ?>
                            <div class="alert alert-success" role="alert">
                                <?= session()->getFlashdata('pesan') ?>
                            </div>
                        <?php endif ?>
                        <?php if (session()->getFlashdata('gagal')) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= session()->getFlashdata('gagal') ?>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr class="table-info">
                                    <th>No.</th>
                                    <th>Nama Anggota</th>
                                    <th>Judul Buku</th>
                                    <th>Jumlah</th>
                                    <th>NIS</th>
                                    <th>Tanggal Peminjaman</th>
                                    <th>Batas Pengembalian</th>
                                    <th>Tanggal Pengembalian</th>
                                    <th>Denda</th>
                                    <th>Kode</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1 ?>
                                <?php foreach ($pinjam as $k) : ?>
                                    <tr>
                                        <th scope="row"><?= $i++ ?></th>
                                        <td>
                                            <p><?= $k->username ?></p>
                                        </td>
                                        <td><?= $k->judul_buku ?></td>
                                        <td><?= $k->qty ?></td>
                                        <td><?= $k->nis ?></td>
                                        <td><?= date('d F Y', strtotime($k->tgl_pinjam))  ?></td>
                                        <td><?= date('d F Y', strtotime($k->deadline)) ?></td>
                                        <td><?php if ($k->tgl_kembali == '0000-00-00') : ?>
                                            <?php else : ?>
                                                <?= date('d F Y', strtotime($k->tgl_kembali)) ?>
                                            <?php endif; ?>
                                        </td>
                                        <td><?= $k->denda ?></td>
                                        <td><?= $k->kode ?></td>
                                        <td>
                                            <?php if ($k->status_peminjaman == 'dipinjam') : ?>
                                                <h6><span class="badge bg-danger rounded-pill"><?= $k->status_peminjaman ?></span></h6>
                                            <?php elseif ($k->status_peminjaman == 'dikembalikan') : ?>
                                                <h6><span class="badge bg-success rounded-pill"><?= $k->status_peminjaman ?></span></h6>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <form action="/admin/peminjaman/restore/<?= $k->id_peminjaman ?>" method="post" class="d-inline">
                                                <button class="btn btn-primary btn-sm" style="margin-bottom: 5px;"><i class="fa-solid fa-trash-can-arrow-up"></i></button>
                                            </form>
                                        </td>
                                    <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website <?= date('Y') ?></div>
                            <!-- <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div> -->
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/assets/demo/chart-area-demo.js"></script>
        <script src="<?= base_url() ?>/assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/datatables-simple-demo.js"></script>
    </body>

    </html>
<?php endif; ?>