<?php if (in_groups('Admin')) : ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Edit Peminjaman</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="<?= base_url() ?>/min/css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>

    <body class="sb-nav-fixed">
        <div id="layoutSidenav">
            <?= $this->include('Admin/Layout/topbar') ?>
            <?= $this->include('Admin/Layout/sidebar') ?>

            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Edit Pinjaman</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active"></li>
                        </ol>
                        <form action="/admin/peminjaman/<?= $pengembalian[0]->id_pengembalian ?>" method="POST" enctype="multipart/form-data">
                            <?= csrf_field() ?>
                            <input type="hidden" name="id_pengembalian" id="id_pengembalian" value="<?= $pengembalian[0]->id_pengembalian ?>">
                            <input type="hidden" name="id_peminjaman" id="id_peminjaman" value="<?= $pengembalian[0]->id_peminjaman ?>">
                            <div class="row mb-3">
                                <label for="tgl_kembali" class="col-sm-2 col-form-label">Tanggal Kembali</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control <?= ($validation->hasError('tgl_kembali')) ? 'is-invalid' : '' ?>" id="tgl_kembali" name="tgl_kembali" value="<?= date('Y-m-d') ?>">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        Tanggal Kembali tidak boleh kosong.
                                    </div>
                                </div>
                            </div>
                            <?php
                            date_default_timezone_set('Asia/Jakarta');
                            $awal = date_create($pengembalian[0]->deadline);
                            $tglDeadline = $awal->format("d");
                            $blnDeadline = $awal->format("m");
                            // d('tglDeadline ' . $awal->format("d"));
                            // d('blnDeadline ' . $awal->format("m"));
                            // d($awal);
                            $akhir = date_create();
                            // d($akhir);
                            $tglKembali = $akhir->format("d");
                            $blnKembali = $akhir->format("m");
                            // d('tglKembali ' . $akhir->format("d"));
                            // d('blnKembali ' . $akhir->format("m"));
                            if ($tglKembali > $tglDeadline && $blnKembali == $blnDeadline) {
                                $diff = date_diff($awal, $akhir)->format("%a");
                                $denda = ($diff * 500) * $pengembalian[0]->qty;
                            } elseif ($tglKembali < $tglDeadline && $blnKembali > $blnDeadline) {
                                $diff = date_diff($awal, $akhir)->format("%a");
                                $denda = ($diff * 500) * $pengembalian[0]->qty;
                            } elseif ($tglKembali > $tglDeadline && $blnKembali > $blnDeadline) {
                                $diff = date_diff($awal, $akhir)->format("%a");
                                $denda = ($diff * 500) * $pengembalian[0]->qty;
                            } elseif ($tglKembali == $tglDeadline && $blnKembali > $blnDeadline) {
                                $diff = date_diff($awal, $akhir)->format("%a");
                                $denda = ($diff * 500) * $pengembalian[0]->qty;
                            } else {
                                $denda = 0;
                            }
                            ?>
                            <div class="row mb-3">
                                <label for="denda" class="col-sm-2 col-form-label">Denda</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control <?= ($validation->hasError('denda')) ? 'is-invalid' : '' ?>" id="denda" name="denda" value="<?= $denda ?>">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        Denda tidak boleh kosong.
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="status_peminjaman" id="status_peminjaman" value="dikembalikan">
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </form>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website <?= date('Y') ?></div>
                            <!-- <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div> -->
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/assets/demo/chart-area-demo.js"></script>
        <script src="<?= base_url() ?>/assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/datatables-simple-demo.js"></script>
    </body>

    </html>
<?php endif; ?>