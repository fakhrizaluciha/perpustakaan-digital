<?php if (in_groups('Admin')) : ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Create Peminjaman</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="<?= base_url() ?>/min/css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>

    <body class="sb-nav-fixed">
        <div id="layoutSidenav">
            <?= $this->include('Admin/Layout/topbar') ?>
            <?= $this->include('Admin/Layout/sidebar') ?>

            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Tambah Peminjaman</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active"></li>
                        </ol>
                        <?php if (session()->getFlashdata('gagal')) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= session()->getFlashdata('gagal') ?>
                            </div>
                        <?php endif ?>
                        <form action="/admin/peminjaman" method="POST" enctype="multipart/form-data">
                            <?= csrf_field() ?>
                            <!-- <input type="hidden" class="form-control" id="created_at" name="created_at" autofocus value="<?= date('Y-m-d') ?>"> -->
                            <div class="row mb-3">
                                <label for="user_id" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                    <select class="form-select" id="user_id" name="user_id">
                                        <?php foreach ($user as $b) : ?>
                                            <option value="<?= $b->id ?>"><?= $b->username ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="buku_id" class="col-sm-2 col-form-label">Judul Buku</label>
                                <div class="col-sm-10">
                                    <select class="form-select" id="buku_id" name="buku_id">
                                        <?php foreach ($buku as $b) : ?>
                                            <option value="<?= $b->id_buku ?>"><?= $b->judul_buku ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="tgl_pinjam" class="col-sm-2 col-form-label">Tanggal Pinjam</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control <?= ($validation->hasError('tgl_pinjam')) ? 'is-invalid' : '' ?>" id="tgl_pinjam" name="tgl_pinjam" value="<?= date('Y-m-d') ?>">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        Tanggal Pinjam tidak boleh kosong.
                                    </div>
                                </div>
                            </div>
                            <?php
                            $deadline = time() + 7 * 60 * 60 * 24;
                            $deadline2 = time() + 8 * 60 * 60 * 24;
                            if (date('Y-m-d', $deadline) > date('Y-m-d', $deadline2)) {
                                echo "Maksimal peminjaman 7 hari";
                            }
                            // dd(date('Y-m-d', $deadline));
                            // dd(date('Y-m-d', $deadline2))
                            ?>
                            <div class="row mb-3">
                                <label for="deadline" class="col-sm-2 col-form-label">Batas Pengembalian</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control <?= ($validation->hasError('deadline')) ? 'is-invalid' : '' ?>" id="deadline" name="deadline" value="<?= date('Y-m-d', $deadline) ?>">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        Batas pengembalian tidak boleh kosong.
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="denda" id="denda">
                            <input type="hidden" name="tgl_kembali" id="tgl_kembali">
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </form>
                    </div>
                    <!-- <div class="container-fluid px-4">
                        <h1 class="mt-4">Data Buku</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Data Buku</li>
                        </ol>
                        <a href="/admin/create-buku" class="btn btn-primary mb-3">Tambah Buku</a>
                        <?php if (session()->getFlashdata('pesan')) : ?>
                            <div class="alert alert-success" role="alert">
                                <?= session()->getFlashdata('pesan') ?>
                            </div>
                        <?php endif ?>
                    </div> -->
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website <?= date('Y') ?></div>
                            <!-- <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div> -->
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/assets/demo/chart-area-demo.js"></script>
        <script src="<?= base_url() ?>/assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/datatables-simple-demo.js"></script>
    </body>

    </html>
<?php endif; ?>