<?php if (in_groups('Admin')) : ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Data Peminjam</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="<?= base_url() ?>/min/css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>

    <body class="sb-nav-fixed">
        <div id="layoutSidenav">
            <?= $this->include('Admin/Layout/topbar') ?>
            <?= $this->include('Admin/Layout/sidebar') ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Data Peminjam</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Data Peminjam</li>
                        </ol>
                        <a href="/admin/peminjam/sampah" class="btn btn-danger mb-3"><i class="fa-solid fa-trash"></i> Sampah</a>
                        <!-- <a href="/admin/peminjam/tambah" class="btn btn-primary mb-3">Tambah Peminjam</a> -->
                        <?php if (session()->getFlashdata('pesan')) : ?>
                            <div class="alert alert-success" role="alert">
                                <?= session()->getFlashdata('pesan') ?>
                            </div>
                        <?php elseif (session()->getFlashdata('gagal')) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= session()->getFlashdata('gagal') ?>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr class="table-info">
                                    <th scope="col">No.</th>
                                    <th scope="col">Gambar</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Role</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1 ?>
                                <?php foreach ($users as $k) : ?>
                                    <tr>
                                        <th scope="row"><?= $i++ ?></th>
                                        <td><img src="<?= base_url() ?>/img/<?= $k->foto ?>" alt="" class="sampul" width="80px"></td>
                                        <td><?= $k->username ?></td>
                                        <td><?= $k->name ?></td>
                                        <td>
                                            <form action="/admin/user/hapus/<?= $k->user_id ?>" method="post" class="d-inline" onsubmit="return confirm('Yakin akan hapus data anda?')">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button class="btn btn-danger btn-sm"><i class="fa-solid fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website <?= date('Y') ?></div>
                            <!-- <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div> -->
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/assets/demo/chart-area-demo.js"></script>
        <script src="<?= base_url() ?>/assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="<?= base_url() ?>/min/js/datatables-simple-demo.js"></script>
    </body>

    </html>
<?php endif; ?>