<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Keranjang saya</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="<?= base_url() ?>/assets/favicon.ico" />
    <link href="<?= base_url() ?>/min/css/styles.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="<?= base_url() ?>/css/styles.css" rel="stylesheet" />
    <!-- Sweet Alert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- DataTables -->
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Navigation-->
    <?= $this->include('User/layout/navbar') ?>
    <section class="page-section bg-light" id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <h1 class="mt-5">Keranjang saya</h1>
                </div>
                <?php if (session()->getFlashdata('pesan')) : ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->getFlashdata('pesan') ?>
                    </div>
                <?php endif ?>
                <?php if (session()->getFlashdata('gagal')) : ?>
                    <div class="alert alert-danger" role="alert">
                        <?= session()->getFlashdata('gagal') ?>
                    </div>
                <?php endif ?>
                <div class="card-body">
                    <table id="datatablesSimple">
                        <thead>
                            <tr class="table-info">
                                <th scope="col">No.</th>
                                <th scope="col">Judul Buku</th>
                                <th scope="col">Tanggal Peminjaman</th>
                                <th scope="col">Batas Pengembalian</th>
                                <th scope="col">Jumlah</th>
                                <th scope="col">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1 ?>
                            <?php foreach ($keranjang as $k) : ?>
                                <tr>
                                    <th scope="row"><?= $i++ ?></th>
                                    <td><?= $k->judul_buku ?></td>
                                    <td><?= date('d F Y', strtotime($k->tgl_pinjam)) ?></td>
                                    <td><?= date('d F Y', strtotime($k->deadline)) ?></td>
                                    <td><?= $k->qty ?></td>
                                    <td>
                                        <form action="/keranjang/<?= $k->id_keranjang ?>" method="post" class="d-inline" onsubmit="return confirm('Yakin akan hapus data anda?')">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="buku_id" id="buku_id" value="<?= $k->id_buku ?>">
                                            <input type="hidden" name="stok" id="stok" value="<?= $k->stok ?>">
                                            <button class="btn btn-danger btn-sm" style="margin-bottom: 5px;"><i class="fa-solid fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                    <div class="row mt-1">
                        <form action="/detail/pinjam/save" method="POST" enctype="multipart/form-data">
                            <?php foreach ($keranjang as $k) : ?>
                                <input type="hidden" name="buku_id" id="buku_id" value="<?= $k->id_buku ?>">
                                <input type="hidden" name="id_keranjang" id="id_keranjang" value="<?= $k->id_keranjang ?>">
                                <input type="hidden" id="tgl_pinjam" name="tgl_pinjam" autofocus value="<?= $k->tgl_pinjam ?>">
                                <input type="hidden" id="deadline" name="deadline" value="<?= $k->deadline ?>">
                            <?php endforeach; ?>
                            <input type="hidden" id="user_id" name="user_id" autofocus value="<?= user_id() ?>">
                            <input type="hidden" name="denda" id="denda">
                            <input type="hidden" name="tgl_kembali" id="tgl_kembali">
                            <div class="col-sm-4">
                                <button class="btn btn-primary"><i class="fa-solid fa-basket-shopping fa-xl"></i> Check out</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="footer py-4">
        <div class="container">
            <div class="row align-items-center">
                <hr>
                <div class="col-lg-4 text-lg-start">Copyright &copy; E-Perpus SMA N 1 Kayen <?= date('Y') ?></div>
                <!-- <div class="col-lg-4 my-3 my-lg-0">
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Twitter"><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Facebook"><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="LinkedIn"><i class="fab fa-linkedin-in"></i></a>
                </div> -->
                <!-- <div class="col-lg-4 text-lg-end">
                    <a class="link-dark text-decoration-none me-3" href="#!">Privacy Policy</a>
                    <a class="link-dark text-decoration-none" href="#!">Terms of Use</a>
                </div> -->
            </div>
        </div>
    </footer>
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script> -->
    <!-- Core theme JS-->
    <script src="<?= base_url() ?>/js/scripts.js"></script>
    <script src="<?= base_url() ?>/min/js/scripts.js"></script>
    <script src="<?= base_url() ?>/min/assets/demo/chart-area-demo.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>/min/js/datatables-simple-demo.js"></script>
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <!-- * *                               SB Forms JS                               * *-->
    <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
</body>

</html>