<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand" href="/">
            SMA N 1 KAYEN
            <!-- <img src="<?= base_url() ?>/assets/img/navbar-logo.svg" alt="..." /> -->
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars ms-1"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
                <!-- <li class="nav-item"><a class="nav-link" href="#services">Services</a></li> -->
                <li class="nav-item"><a class="nav-link" href="/">Beranda</a></li>
                <li class="nav-item"><a class="nav-link" href="/koleksi-video">Koleksi Video</a></li>
                <li class="nav-item"><a class="nav-link" href="/koleksi-buku">Koleksi Buku</a></li>
                <li class="nav-item"><a class="nav-link" href="/profile">Profil Saya</a></li>
                <li class="nav-item"><a class="nav-link" href="/riwayat">Riwayat</a></li>
                <li class="nav-item"><a class="nav-link" href="/pesan"><i class="fa-solid fa-envelope" style="font-size: 18px; color: #ffffff;"></i></a></li>
                <li class="nav-item"><a class="nav-link" href="/keranjang"><i class="fa-solid fa-basket-shopping" style="font-size: 18px; color: white"></i></a></li>
                <!-- <li class="nav-item"><a class="nav-link" href="#about">About</a></li> -->
                <!-- <li class="nav-item"><a class="nav-link" href="#team">Team</a></li> -->
                <!-- <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li> -->
                <!-- <li class="nav-item">
                    <a class="nav-link">
                        <div class="btn-group dropstart">
                            <button type="button" data-bs-toggle="dropdown" style="background-color: Transparent; border: none">
                                <i class="fa-regular fa-bell" style="font-size: 20px; color: white"></i>
                            </button>
                            <ul class="dropdown-menu" style="min-width: 40ch;">
                                <?php
                                $db = \Config\Database::connect();
                                $user = user()->id;
                                $pesan = $db->query("SELECT * FROM pesan WHERE user_id = $user")->getResult();
                                // dd($pesan);
                                if ($pesan == null) : ?>
                                    <li>Tidak ada pesan</li>
                                <?php else : ?>
                                    <?php foreach ($pesan as $p) : ?>
                                        <li><?= $p->pesan ?></li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </div>
                    </a>
                </li> -->
                <?php if (logged_in()) : ?>
                    <li class="nav-item"><a class="nav-link" href="<?= base_url('logout') ?>">Logout</a></li>
                <?php else : ?>
                    <li class="nav-item"><a class="nav-link" href="<?= base_url('login') ?>">Login</a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>