<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>E-Perpus SMANSAKA</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="<?= base_url() ?>/assets/favicon.ico" />
    <link href="<?= base_url() ?>/min/css/styles.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="<?= base_url() ?>/css/styles.css" rel="stylesheet" />
    <!-- Sweet Alert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body id="page-top">
    <!-- Navigation-->
    <?= $this->include('User/layout/navbar') ?>
    <section class="page-section bg-light" id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <h1 class="mt-4">Detail Buku</h1>
                </div>
                <form action="/keranjang" method="POST" enctype="multipart/form-data">
                    <?= csrf_field() ?>
                    <input type="hidden" class="form-control " id="id_buku" name="id_buku" autofocus value="<?= $buku['id_buku'] ?>">
                    <input type="hidden" class="form-control " id="judul_buku" name="judul_buku" autofocus value="<?= $buku['judul_buku'] ?>">
                    <input type="hidden" class="form-control " id="user_id" name="user_id" autofocus value="<?= user_id() ?>">
                    <input type="hidden" class="form-control " id="stok" name="stok" autofocus value="<?= $buku['stok'] ?>">
                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row g-0">
                            <div class="col-md-4">
                                <img src="<?= base_url() ?>/img/<?= $buku['gambar'] ?>" class="img-fluid rounded-start" alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title" style="color: #ffc800;"><?= $buku['judul_buku'] ?></h5>
                                    <p class="card-text" style="color: #12B886; font-weight:500">Tanggal Post: <?= $buku['created_at'] ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-left">
                            <?php if (session()->getFlashdata('gagal')) : ?>
                                <div class="alert alert-danger" role="alert">
                                    <?= session()->getFlashdata('gagal') ?>
                                </div>
                            <?php endif ?>
                            <h5 class="alt-font text-extra-dark-gray fonr-weight-500">
                                Deskripsi Buku
                            </h5>
                            <table class="table table-hover table-striped table-condensed">
                                <tbody>
                                    <tr>
                                        <td width="200">Judul</td>
                                        <td><input type="hidden" class="form-control " id="buku_id" name="buku_id" autofocus value="<?= $buku['id_buku'] ?>"><?= $buku['judul_buku'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Penulis</td>
                                        <td><input type="hidden" class="form-control " id="penulis" name="penulis" autofocus value="<?= $buku['penulis'] ?>"><?= $buku['penulis'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Penerbit</td>
                                        <td><?= $buku['penerbit'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tahun Terbit</td>
                                        <td><?= $buku['tahun_terbit'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Stok</td>
                                        <td><?= $buku['stok'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>ISBN</td>
                                        <td><?= $buku['isbn'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Uraian</td>
                                        <td><?= $buku['uraian'] ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="book">
                                <div class="container flex_space">
                                    <div class="pinjam">
                                        <div class="row mt-1">
                                            <?php
                                            date_default_timezone_set('Asia/Jakarta');
                                            $deadline = time() + 10 * 60 * 60 * 24;
                                            $deadline2 = time() + 11 * 60 * 60 * 24;
                                            if (date('Y-m-d', $deadline) > date('Y-m-d', $deadline2)) {
                                                echo "Maksimal peminjaman 10 hari";
                                            }
                                            // d(date('Y-m-d', $deadline));
                                            // d(date('Y-m-d', $deadline2))
                                            ?>
                                            <input type="hidden" name="kode" id="kode">
                                            <div class="col-sm-4">
                                                <input type="date" class="form-control" id="tgl_pinjam" name="tgl_pinjam" autofocus value="<?= date('Y-m-d') ?>">
                                            </div>
                                            <div class="col-sm-2" style="display: flex; justify-content:center">
                                                <i class="fa-solid fa-right-long fa-2xl"></i>
                                                <!-- <img src="https://img.icons8.com/ios-filled/40/FFFFFF/long-arrow-right.png" /> -->
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="date" class="form-control" id="deadline" name="deadline" value="<?= date('Y-m-d', $deadline) ?>" style="margin-bottom: 10px;" required>
                                            </div>
                                            <div class="col-sm-2" style="display: flex; justify-content:center">
                                                <button type="submit" class="btn btn-primary">Tambah ke troli</button>
                                            </div>
                                        </div>
                                        <div class="row mt-1">
                                            <span style="color:red">*Maksimal peminjaman 10 hari</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="denda" id="denda">
                            <input type="hidden" name="tgl_kembali" id="tgl_kembali">
                        </div>
                    </div><br>
                </form>
                <div class="row mt-1">
                    <div class="col-sm-4">
                        <a href="/keranjang"><button class="btn btn-primary"><i class="fa-solid fa-basket-shopping fa-xl"></i> Lihat keranjang</button></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="footer py-4">
        <div class="container">
            <div class="row align-items-center">
                <hr>
                <div class="col-lg-4 text-lg-start">Copyright &copy; E-Perpus SMA N 1 Kayen <?= date('Y') ?></div>
                <!-- <div class="col-lg-4 my-3 my-lg-0">
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Twitter"><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Facebook"><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="LinkedIn"><i class="fab fa-linkedin-in"></i></a>
                </div> -->
                <!-- <div class="col-lg-4 text-lg-end">
                    <a class="link-dark text-decoration-none me-3" href="#!">Privacy Policy</a>
                    <a class="link-dark text-decoration-none" href="#!">Terms of Use</a>
                </div> -->
            </div>
        </div>
    </footer>
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script> -->
    <!-- Core theme JS-->
    <script src="<?= base_url() ?>/js/scripts.js"></script>
    <script src="<?= base_url() ?>/min/js/scripts.js"></script>
    <script src="<?= base_url() ?>/min/assets/demo/chart-area-demo.js"></script>
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <!-- * *                               SB Forms JS                               * *-->
    <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    <script type="text/javascript">
        function pinjam() {
            swal("Good job!", "You clicked the button!", "success");
        }
    </script>
</body>

</html>