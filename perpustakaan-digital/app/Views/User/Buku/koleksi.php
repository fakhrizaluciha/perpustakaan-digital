<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>E-Perpus SMANSAKA</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="<?= base_url() ?>/assets/favicon.ico" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="<?= base_url() ?>/css/styles.css" rel="stylesheet" />
    <!-- Lottie files -->
    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
</head>

<body id="page-top">
    <!-- Navigation-->
    <?= $this->include('User/layout/navbar') ?>
    <section class="page-section bg-light" id="portfolio">
        <div class="container">
            <div class="row mt-5">
                <!-- <lottie-player src="https://assets4.lottiefiles.com/private_files/lf30_n0xnbjiz.json" background="transparent" speed="1" style="width: 500px; height: 500px; position:absolute;" loop autoplay></lottie-player> -->
                <div class="col-md-6">
                    <h1>Koleksi Buku</h1>
                </div>
                <form action="" method="GET" autocomplete="off">
                    <div class="row mb-3">
                        <div class="col-sm-4 mb-3">
                            <select class="form-select" id="kategori" name="kategori">
                                <option value="" selected>-- Pilih Kategori --</option>
                                <?php foreach ($kategori as $b) : ?>
                                    <option value="<?= $b['kategori'] ?>"><?= $b['kategori'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <input type="text" class="form-control" placeholder="Cari judul buku" name="keyword">
                        </div>
                        <div class="col-sm-2 mb-2">
                            <button class="btn btn-success" type="submit"><i class="fas fa-search"></i> Cari</button>
                        </div>
                        <div class="col-sm-2">
                            <p>Total buku: <?= $jml_buku = count($buku) ?></p>
                        </div>
                    </div>
                </form>
                <!-- <hr> -->
                <?php foreach ($buku as $b) : ?>
                    <div class="col-6" id="demo">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row g-0">
                                <div class="col-md-4">
                                    <center>
                                        <img src="<?= base_url() ?>/img/<?= $b['gambar'] ?>" class="img-fluid rounded-start" width="60%" height="60%">
                                    </center>
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <a href="/detail/<?= $b['judul_buku'] ?>" style="text-decoration:none;">
                                            <h5 class="card-title"><?= $b['judul_buku'] ?></h5>
                                        </a>
                                        <p class="card-text" style="color: #12B886;">
                                            Tanggal Post: <?= $b['created_at'] ?><br>
                                        </p>
                                        <p><i class="fa-solid fa-user" style="color: #22c3e6;"></i>
                                            <span style="color: #22C3E6;"><?= $b['penulis'] ?></span><br>
                                            <i class="fa-solid fa-calendar" style="color: #22c3e6;"></i>
                                            <span style="color: #22C3E6;"><?= $b['tahun_terbit'] ?></span><br>
                                            <?php if ($b['stok'] == 0) : ?>
                                                <i class="fa-solid fa-circle-info" style="color: red;"></i>
                                                <span style="color: red;"><?= $b['stok'] ?></span>
                                            <?php else : ?>
                                                <i class="fa-solid fa-circle-info" style="color: #22c3e6;"></i>
                                                <span style="color: #22C3E6;"><?= $b['stok'] ?></span>
                                            <?php endif; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php if (empty($buku)) : ?>
                    <div class="regis-card flex-wrap rounded-3 py-3 px-3 px-lg-3 d-flex justify-content-center align-items-center">
                        <p class="text-center mt-3">No results found</p>
                    </div>
                <?php endif; ?>
            </div>
            <div class="container" style="margin-top: 50px">
                <?= $pager->links('default', 'pagination') ?>
            </div>
        </div>
    </section>
    <footer class="footer py-4">
        <div class="container">
            <div class="row align-items-center">
                <hr>
                <div class="col-lg-4 text-lg-start">Copyright &copy; E-Perpus SMA N 1 Kayen <?= date('Y') ?></div>
                <!-- <div class="col-lg-4 my-3 my-lg-0">
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Twitter"><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Facebook"><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="LinkedIn"><i class="fab fa-linkedin-in"></i></a>
                </div> -->
                <!-- <div class="col-lg-4 text-lg-end">
                    <a class="link-dark text-decoration-none me-3" href="#!">Privacy Policy</a>
                    <a class="link-dark text-decoration-none" href="#!">Terms of Use</a>
                </div> -->
            </div>
        </div>
    </footer>
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Core theme JS-->
    <script src="<?= base_url() ?>/js/scripts.js"></script>
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <!-- * *                               SB Forms JS                               * *-->
    <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
</body>

</html>