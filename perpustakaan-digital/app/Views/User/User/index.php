<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>E-Perpus SMANSAKA</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="<?= base_url() ?>/assets/favicon.ico" />
    <link href="<?= base_url() ?>/min/css/styles.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="<?= base_url() ?>/css/styles.css" rel="stylesheet" />
    <!-- Sweet Alert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body id="page-top">
    <!-- Navigation-->
    <?= $this->include('User/layout/navbar') ?>
    <section class="page-section bg-light" id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <h1 class="mt-5">My Profile</h1>
                </div>
                <div class="container rounded bg-white mt-2 mb-5">
                    <div class="row">
                        <div class="col-md-3 border-right">
                            <div class="d-flex flex-column align-items-center text-center p-3 py-2">
                                <img class="rounded-circle mt-5" width="150px" src="<?= base_url() ?>/img/<?= user()->foto ?>" id="foto">
                                <span class="font-weight-bold"><?= user()->username ?></span><span class="text-black-50"><?= user()->email ?></span>
                            </div>
                            <div class="d-flex flex-column align-items-center text-center p-3 py-2">
                                <form action="/foto/save" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="id" class="form-control" value="<?= user()->id ?>">
                                    <input type="file" class="form-control <?= ($validation->hasError('foto')) ? 'is-invalid' : '' ?>" id="file" name="foto" style="display: none;">
                                    <div id="validationServer03Feedback" class="invalid-feedback">
                                        <?= $validation->getError('foto') ?>
                                    </div>
                                    <div class="d-flex flex-column align-items-center text-center p-3 py-2">
                                        <div class="col-md-5">
                                            <label for="file" style="cursor: pointer;"><i class="fas fa-camera"></i></label>
                                        </div>
                                    </div>
                                    <button class="btn btn-success" type="submit">Simpan foto</button>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-6 border-right">
                            <div class="p-3 py-5">
                                <?php if (session()->getFlashdata('pesan')) : ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?= session()->getFlashdata('pesan') ?>
                                    </div>
                                <?php elseif (session()->getFlashdata('message')) : ?>
                                    <div class="alert alert-success" role="alert">
                                        <?= session()->getFlashdata('message') ?>
                                    </div>
                                <?php endif; ?>
                                <div class="d-flex justify-content-between align-items-center mb-3">
                                    <h4 class="text-right">Profile Settings</h4>
                                </div>
                                <form action="/profile/save" method="post" enctype="multipart/form-data">
                                    <input type="hidden" id="id" name="id" class="form-control" value="<?= user()->id ?>">
                                    <div class="row mt-2">
                                        <!-- <div class="col-md-6"><label class="labels">Surname</label><input type="text" class="form-control" value="" placeholder="surname"></div> -->
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-12"><label class="labels">Nama</label><input type="text" id="username" name="username" class="form-control" placeholder="first name" value="<?= user()->username ?>" readonly></div>
                                        <div class="col-md-12"><label class="labels">Email</label><input type="email" class="form-control" name="email" value="<?= user()->email ?>" readonly></div>
                                        <!-- <div class="col-md-12"><label class="labels">Kelas</label>
                                            <select class="form-select form-select-sm" aria-label=".form-select-sm example" name="kelas" value="<?= user()->kelas ?>">
                                                <option selected><?php if (user()->kelas == null) : ?>
                                                        --Pilih Kelas--
                                                    <?php else : ?>
                                                        <?= user()->kelas ?>
                                                    <?php endif; ?></option>
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                            </select>
                                        </div> -->
                                        <div class="col-md-12"><label class="labels">NIS</label><input type="text" class="form-control" placeholder="enter address line 1" name="nis" value="<?= user()->nis ?>"></div>
                                        <div class="col-md-12"><label class="labels">Jenis kelamin</label>
                                            <select class="form-select form-select-sm" aria-label=".form-select-sm example" name="jenis_kelamin">
                                                <option selected><?php if (user()->jenis_kelamin == null) : ?>
                                                        --Pilih--
                                                    <?php else : ?>
                                                        <?= user()->jenis_kelamin ?>
                                                    <?php endif; ?></option>
                                                <option value="laki-laki">Laki-laki</option>
                                                <option value="perempuan">Perempuan</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12"><label class="labels">Alamat</label><input type="text" class="form-control" name="alamat" placeholder="enter address line 2" value="<?= user()->alamat ?>"></div>
                                        <div class="col-md-12"><label class="labels">Tanggal Lahir</label><input type="date" class="form-control" placeholder="enter address line 2" name="tgl_lahir" value="<?= user()->tgl_lahir ?>"></div>
                                        <!-- <div class="col-md-12"><label class="labels">State</label><input type="text" class="form-control" placeholder="enter address line 2" value=""></div>
                                        <div class="col-md-12"><label class="labels">Area</label><input type="text" class="form-control" placeholder="enter address line 2" value=""></div>
                                        <div class="col-md-12"><label class="labels">Email ID</label><input type="text" class="form-control" placeholder="enter email id" value=""></div>
                                        <div class="col-md-12"><label class="labels">Education</label><input type="text" class="form-control" placeholder="education" value=""></div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-6"><label class="labels">Country</label><input type="text" class="form-control" placeholder="country" value=""></div>
                                        <div class="col-md-6"><label class="labels">State/Region</label><input type="text" class="form-control" value="" placeholder="state"></div>
                                    </div> -->
                                        <div class="mt-5 text-center"><button class="btn btn-primary profile-button" type="submit">Save Profile</button></div>
                                </form>
                            </div>
                        </div>
                        <!-- <div class="col-md-4">
                            <div class="p-3 py-5">
                                <div class="d-flex justify-content-between align-items-center experience"><span>Edit Experience</span><span class="border px-3 p-1 add-experience"><i class="fa fa-plus"></i>&nbsp;Experience</span></div><br>
                                <div class="col-md-12"><label class="labels">Experience in Designing</label><input type="text" class="form-control" placeholder="experience" value=""></div> <br>
                                <div class="col-md-12"><label class="labels">Additional Details</label><input type="text" class="form-control" placeholder="additional details" value=""></div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="footer py-4">
        <div class="container">
            <div class="row align-items-center">
                <hr>
                <div class="col-lg-4 text-lg-start">Copyright &copy; E-Perpus SMA N 1 Kayen <?= date('Y') ?></div>
                <!-- <div class="col-lg-4 my-3 my-lg-0">
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Twitter"><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Facebook"><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="LinkedIn"><i class="fab fa-linkedin-in"></i></a>
                </div> -->
                <!-- <div class="col-lg-4 text-lg-end">
                    <a class="link-dark text-decoration-none me-3" href="#!">Privacy Policy</a>
                    <a class="link-dark text-decoration-none" href="#!">Terms of Use</a>
                </div> -->
            </div>
        </div>
    </footer>
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script> -->
    <!-- Core theme JS-->
    <script src="<?= base_url() ?>/js/scripts.js"></script>
    <script src="<?= base_url() ?>/min/js/scripts.js"></script>
    <script src="<?= base_url() ?>/min/assets/demo/chart-area-demo.js"></script>
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <!-- * *                               SB Forms JS                               * *-->
    <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
</body>

</html>