<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>E-Perpus SMANSAKA</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="<?= base_url() ?>/assets/favicon.ico" />
    <link href="<?= base_url() ?>/min/css/styles.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="<?= base_url() ?>/css/styles.css" rel="stylesheet" />
    <!-- Sweet Alert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- DataTables -->
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Navigation-->
    <?= $this->include('User/layout/navbar') ?>
    <section class="page-section bg-light" id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <h1 class="mt-5">Riwayat Saya</h1>
                    <?php
                    $db = \Config\Database::connect();
                    $u = user()->id;
                    $tgl = date("Y-m-d");
                    $alert = $db->query("SELECT *, DATEDIFF(deadline, '$tgl') AS tanggalKembali FROM peminjamans 
                    JOIN pengembalians ON pengembalians.peminjaman_id = peminjamans.id_peminjaman
                    JOIN peminjaman_has_buku ON peminjaman_has_buku.peminjaman_id = peminjamans.id_peminjaman
                    JOIN bukus ON bukus.id_buku = peminjaman_has_buku.buku_id WHERE pengembalians.user_id = $u AND pengembalians.tgl_kembali = '0000-00-00'")->getResult();
                    // dd($alert);
                    foreach ($alert as $a) : ?>
                        <?php if ($a->tanggalKembali == 1) : ?>
                            <p><i class="fa-solid fa-triangle-exclamation" style="color: red;"></i></i> Buku dengan judul <?= strtoupper($a->judul_buku) ?> besok adalah batas pengembalian buku</p>
                        <?php elseif ($a->tanggalKembali == 2) : ?>
                            <p><i class="fa-solid fa-triangle-exclamation" style="color: red;"></i></i> Buku dengan judul <?= strtoupper($a->judul_buku) ?> batas pengembalian buku tinggal 2 hari</p>
                        <?php elseif ($a->tanggalKembali == 3) : ?>
                            <p><i class="fa-solid fa-triangle-exclamation" style="color: red;"></i></i> Buku dengan judul <?= strtoupper($a->judul_buku) ?> batas pengembalian buku tinggal 3 hari</p>
                        <?php elseif ($a->tanggalKembali == 0) : ?>
                            <p><i class="fa-solid fa-triangle-exclamation" style="color: red;"></i></i> Buku dengan judul <?= strtoupper($a->judul_buku) ?> hari ini adalah batas pengembalian buku</p>
                        <?php elseif ($a->tanggalKembali < 0) : ?>
                            <p><i class="fa-solid fa-triangle-exclamation" style="color: red;"></i></i> Anda telah melewati batas pengembalian buku <?= strtoupper($a->judul_buku) ?> dan akan dikenakan denda</p>
                        <?php else : ?>
                            <p></p>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
                <div class="col-lg-8">
                    <div class="mb-2">
                        <button type="button" class="btn btn-primary">
                            <a href="/pesan" style="text-decoration: none; color:white"><i class="fa-solid fa-envelope" style="font-size: 20px;"></i> Pesan</a>
                        </button>
                    </div>
                    <div class="col-sm-4">
                        <?php if (session()->getFlashdata('pesan')) : ?>
                            <div class="alert alert-success" role="alert">
                                <?= session()->getFlashdata('pesan') ?>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatablesSimple">
                        <thead>
                            <tr class="table-info">
                                <th scope="col">No.</th>
                                <th scope="col">Judul Buku</th>
                                <th scope="col">Jumlah</th>
                                <th scope="col">Tanggal Peminjaman</th>
                                <th scope="col">Batas Pengembalian</th>
                                <th scope="col">Tanggal Pengembalian</th>
                                <th scope="col">Denda</th>
                                <th scope="col">Status</th>
                                <th scope="col">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1 ?>
                            <?php foreach ($peminjaman as $k) : ?>
                                <tr>
                                    <th scope="row"><?= $i++ ?></th>
                                    <td><?= $k->judul_buku ?></td>
                                    <td><?= $k->qty ?></td>
                                    <td><?= date('d F Y', strtotime($k->tgl_pinjam)) ?></td>
                                    <td><?= date('d F Y', strtotime($k->deadline)) ?></td>
                                    <td><?php if ($k->tgl_kembali == '0000-00-00') : ?>
                                        <?php else : ?>
                                            <?= date('d F Y', strtotime($k->tgl_kembali)) ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>Rp <?= $k->denda ?></td>
                                    <td>
                                        <?php if ($k->status_peminjaman == 'dipinjam') : ?>
                                            <h6><span class="badge bg-danger rounded-pill"><?= $k->status_peminjaman ?></span></h6>
                                        <?php elseif ($k->status_peminjaman == 'dikembalikan') : ?>
                                            <h6><span class="badge bg-success rounded-pill"><?= $k->status_peminjaman ?></span></h6>
                                        <?php endif; ?>
                                    </td>
                                    <td><a href="/riwayat/cetak/<?= $k->id_peminjaman ?>" class="d-inline" target="_blank"><button class="btn btn-default btn-xs"><i class="fa fa-barcode"></i> Generate</button></a></td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <footer class="footer py-4">
        <div class="container">
            <div class="row align-items-center">
                <hr>
                <div class="col-lg-4 text-lg-start">Copyright &copy; E-Perpus SMA N 1 Kayen <?= date('Y') ?></div>
                <!-- <div class="col-lg-4 my-3 my-lg-0">
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Twitter"><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Facebook"><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="LinkedIn"><i class="fab fa-linkedin-in"></i></a>
                </div> -->
                <!-- <div class="col-lg-4 text-lg-end">
                    <a class="link-dark text-decoration-none me-3" href="#!">Privacy Policy</a>
                    <a class="link-dark text-decoration-none" href="#!">Terms of Use</a>
                </div> -->
            </div>
        </div>
    </footer>
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script> -->
    <!-- Core theme JS-->
    <script src="<?= base_url() ?>/js/scripts.js"></script>
    <script src="<?= base_url() ?>/min/js/scripts.js"></script>
    <script src="<?= base_url() ?>/min/assets/demo/chart-area-demo.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>/min/js/datatables-simple-demo.js"></script>
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <!-- * *                               SB Forms JS                               * *-->
    <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
</body>

</html>