<?php if (in_groups('User')) : ?>
<?= $this->include('User/index') ?>
<?php endif; ?>

<?php if (in_groups('Admin')) : ?>
<?= $this->include('Admin/index') ?>
<?php endif; ?>