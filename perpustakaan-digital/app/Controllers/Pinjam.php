<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\PinjamModel;
use App\Models\BukuModel;
use App\Models\PeminjamanModel;
use App\Models\PeminjamanHasBukuModel;
use App\Models\PengembalianModel;
use App\Models\PengembalianHasBukuModel;
use \Myth\Auth\Models\UserModel;

class Pinjam extends BaseController
{
    protected $PinjamModel;
    protected $BukuModel;
    protected $PeminjamanModel;
    protected $PengembalianModel;
    protected $PeminjamanHasBukuModel;
    protected $PengembalianHasBukuModel;
    protected $UserModel;
    public function __construct()
    {
        $this->PinjamModel = new PinjamModel();
        $this->BukuModel = new BukuModel();
        $this->PeminjamanModel = new PeminjamanModel();
        $this->PeminjamanHasBukuModel = new PeminjamanHasBukuModel();
        $this->PengembalianHasBukuModel = new PengembalianHasBukuModel();
        $this->PengembalianModel = new PengembalianModel();
        $this->UserModel = new UserModel();
    }
    public function index()
    {
        $data = [
            'pinjam' => $this->PeminjamanModel->getPeminjaman(),
            // 'pengembalian'  => $this->PengembalianModel->getPengembalian()
        ];
        // dd($data);
        // dd($tglPinjam1, $tglPinjam2);
        return view('Admin/Pinjam/index', $data);
    }
    public function cetak()
    {
        // $db = \Config\Database::connect();
        // $sql = "SELECT SUM(denda) AS total FROM pengembalian";
        // $result = $this->PengembalianModel->query($sql);
        // dd($result);
        $tglPinjam1 = $_POST['tglPinjam1'];
        $tglPinjam2 = $_POST['tglPinjam2'];
        $tgl1 = date_create($tglPinjam1)->format("d");
        $bln1 = date_create($tglPinjam1)->format("m");
        $tgl2 = date_create($tglPinjam2)->format("d");
        $bln2 = date_create($tglPinjam2)->format("m");
        if ($tgl2 < $tgl1 && $bln2 == $bln1) {
            session()->setFlashdata('gagal', 'Tanggal ke kedua tidak boleh kurang dari tanggal pertama!');
            return redirect()->to('/admin/peminjaman');
        } elseif ($tgl2 > $tgl1 && $bln2 < $bln1) {
            session()->setFlashdata('gagal', 'Tanggal ke kedua tidak boleh kurang dari tanggal pertama!');
            return redirect()->to('/admin/peminjaman');
        } elseif ($tgl2 == $tgl1 && $bln2 < $bln1) {
            session()->setFlashdata('gagal', 'Tanggal ke kedua tidak boleh kurang dari tanggal pertama!');
            return redirect()->to('/admin/peminjaman');
        }
        $data = [
            'pinjam' => $this->PeminjamanModel->getPinjamByTgl($tglPinjam1, $tglPinjam2),
            'total' => $this->PengembalianModel->getTotalDendaByTgl($tglPinjam1, $tglPinjam2),
            'totalQty' => $this->PeminjamanModel->getTotalJumlahByTgl($tglPinjam1, $tglPinjam2)
        ];
        // dd($data);
        // dd($tglPinjam1, $tglPinjam2);
        return view('Admin/Pinjam/cetak', $data);
    }
    public function edit($id_pengembalian)
    {
        $data = [
            'user' => $this->UserModel->get()->getResult(),
            'pengembalian'       => $this->PengembalianModel->getPengembalianById($id_pengembalian),
            'validation' => \Config\Services::validation(),
            'buku' => $this->BukuModel->getBuku()
        ];
        // dd($data);
        return view('Admin/Pinjam/edit', $data);
    }
    public function update($id_pengembalian)
    {
        // dd($this->request->getVar());
        $data = [
            'user' => $this->UserModel->get()->getResult(),
            'pengembalian'       => $this->PengembalianModel->getPengembalianById($id_pengembalian),
            'validation' => \Config\Services::validation(),
            'buku' => $this->BukuModel->getBuku()
        ];
        // dd($data);
        if (!$this->validate([
            'tgl_kembali'   => 'required',
            'denda' => 'required'
        ])) {
            // $validation = \Config\Services::validation();
            return redirect()->to('/admin/peminjaman/' . $this->request->getVar('id_pengembalian'))->withInput();
        }
        $this->PeminjamanModel->save([
            'id_peminjaman'     => $this->request->getVar('id_peminjaman'),
            'status_peminjaman' => $this->request->getVar('status_peminjaman')
        ]);
        $this->PengembalianModel->save([
            'id_pengembalian'   => $id_pengembalian,
            'denda'             => $this->request->getVar('denda'),
            'tgl_kembali'       => $this->request->getVar('tgl_kembali'),
            'jml_pengembalian'  => $data['pengembalian'][0]->qty
        ]);
        $this->BukuModel->save([
            'id_buku'   => $data['pengembalian'][0]->id_buku,
            'stok'      => $data['pengembalian'][0]->stok + $data['pengembalian'][0]->qty
        ]);
        session()->setFlashdata('pesan', 'Berhasil diupdate');
        return redirect()->to('/admin/peminjaman');
    }
    public function create()
    {
        $where = "group_id='2'";
        $data = [
            'peminjaman' => $this->PeminjamanModel->getPeminjaman(),
            'validation' => \Config\Services::validation(),
            'user' => $this->UserModel->join('auth_groups_users', 'auth_groups_users.user_id = users.id')->where($where)->get()->getResult(),
            'buku' => $this->BukuModel->getBuku()
        ];
        // dd($data);
        // dd(count($data['peminjaman']));
        // dd(end($data['peminjaman'])->id_peminjaman);
        return view('Admin/Pinjam/create', $data);
    }
    public function save()
    {
        // dd($this->request->getVar());
        date_default_timezone_set('Asia/Jakarta');
        $batas = date_diff(date_create($this->request->getVar('deadline')), date_create($this->request->getVar('tgl_pinjam')))->format("%a");
        // dd($batas);
        if ($this->request->getVar('stok') == '0') {
            session()->setFlashdata('gagal', 'Stok buku sedang kosong');
            return redirect()->to('/admin/peminjaman/tambah');
        } elseif ($batas > 7) {
            session()->setFlashdata('gagal', 'Maksimal peminjaman 7 hari');
            return redirect()->to('/admin/peminjaman/tambah');
        }
        if (!$this->validate([
            'user_id' => 'required',
            'buku_id' => 'required',
            'tgl_pinjam' => 'required',
            'deadline' => 'required',
        ])) {
            return redirect()->to('/admin/peminjaman/tambah')->withInput();
        }
        // $slug = url_title($this->request->getVar('gambar'), '-', true);
        $peminjaman = $this->PeminjamanModel->insert([
            'user_id'        => $this->request->getVar('user_id'),
            'buku_id'    => $this->request->getVar('buku_id'),
            'tgl_pinjam'   => $this->request->getVar('tgl_pinjam'),
            'deadline'     => $this->request->getVar('deadline'),
            'kode'  => uniqid(),
            // 'status_peminjaman'    => $this->request->getVar('status_peminjaman'),
        ]);
        // $peminjaman_id = $peminjaman->getInsertId();        
        $this->PeminjamanHasBukuModel->save([
            'peminjaman_id' => $peminjaman,
            'buku_id'   => $this->request->getVar('buku_id')
        ]);
        $pengembalian = $this->PengembalianModel->insert([
            'user_id'   => $this->request->getVar('user_id'),
            'peminjaman_id' => $peminjaman,
            'denda'       => $this->request->getVar('denda'),
            'tgl_kembali'      => $this->request->getVar('tgl_kembali'),
        ]);
        // dd($pengembalian);
        $this->PengembalianHasBukuModel->save([
            'pengembalian_id' => $pengembalian,
            'buku_id'   => $this->request->getVar('buku_id')
        ]);
        session()->setFlashdata('pesan', 'Berhasil ditambahkan');
        return redirect()->to('/admin/peminjaman');
    }
    public function deletePinjam($id_peminjaman)
    {
        // $pinjam = $this->BukuModel->find($id_pinjam);
        // $this->PeminjamanModel->delete($id_peminjaman);
        date_default_timezone_set('Asia/Jakarta');
        $db = \Config\Database::connect();
        $db->table('peminjaman')->set('deleted_at', date_create()->format('Y-m-d'), true)->where(['id_peminjaman' => $id_peminjaman])->update();
        session()->setFlashdata('pesan', 'Berhasil dihapus');
        return redirect()->to('/admin/peminjaman');
    }
    public function sampah()
    {
        $data = [
            'pinjam' => $this->PeminjamanModel->getSampahPeminjaman()
        ];
        // dd($data);
        // dd(count($data['pinjam']));
        for ($i = 0; $i < count($data['pinjam']); $i++) {
            date_default_timezone_set('Asia/Jakarta');
            $tglDelete = date_create($data['pinjam'][$i]->deleted_at);
            $tglSekarang = date_create();
            // dd(date_diff($tglDelete, $tglSekarang)->format('%a'));
            if (date_diff($tglDelete, $tglSekarang)->format('%a') == 10) {
                $this->PeminjamanModel->delete($data['pinjam'][$i]->id_peminjaman, true);
            }
        }
        return view('Admin/Pinjam/sampah', $data);
    }
    public function restore($id_peminjaman)
    {
        $db = \Config\Database::connect();
        $db->table('peminjaman')->set('deleted_at', null, true)->where(['id_peminjaman' => $id_peminjaman])->update();
        session()->setFlashdata('pesan', 'Berhasil restore');
        return redirect()->to('/admin/peminjaman');
    }
}
