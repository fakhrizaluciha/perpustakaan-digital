<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\PeminjamanModel;
use App\Models\PengembalianModel;

class Pelaporan extends BaseController
{
    protected $PeminjamanModel;
    protected $PengembalianModel;
    public function __construct()
    {
        $this->PeminjamanModel = new PeminjamanModel();
        $this->PengembalianModel = new PengembalianModel();
    }
    public function index()
    {
        return view('Admin/Pelaporan/index');
    }

    public function cetak()
    {
        $tglPinjam1 = $_POST['tglPinjam1'];
        $tglPinjam2 = $_POST['tglPinjam2'];
        $status_peminjaman = $_POST['status_peminjaman'];
        $tgl1 = date_create($tglPinjam1)->format("d");
        $bln1 = date_create($tglPinjam1)->format("m");
        $tgl2 = date_create($tglPinjam2)->format("d");
        $bln2 = date_create($tglPinjam2)->format("m");
        if ($tgl2 < $tgl1 && $bln2 == $bln1) {
            session()->setFlashdata('gagal', 'Tanggal ke kedua tidak boleh kurang dari tanggal pertama!');
            return redirect()->to('/admin/pelaporan');
        } elseif ($tgl2 > $tgl1 && $bln2 < $bln1) {
            session()->setFlashdata('gagal', 'Tanggal ke kedua tidak boleh kurang dari tanggal pertama!');
            return redirect()->to('/admin/pelaporan');
        } elseif ($tgl2 == $tgl1 && $bln2 < $bln1) {
            session()->setFlashdata('gagal', 'Tanggal ke kedua tidak boleh kurang dari tanggal pertama!');
            return redirect()->to('/admin/pelaporan');
        } elseif ($status_peminjaman == '-- Pilih status peminjaman') {
            session()->setFlashdata('gagal', 'Status peminjaman harus diisi!');
            return redirect()->to('/admin/pelaporan');
        }
        $data = [
            'pinjam' => $this->PeminjamanModel->getPinjamByTglAndStatus($tglPinjam1, $tglPinjam2, $status_peminjaman),
            'total' => $this->PengembalianModel->getTotalDendaByTglAndStatus($tglPinjam1, $tglPinjam2, $status_peminjaman),
            'totalQtyDikembalikan' => $this->PengembalianModel->getTotalJumlahByTglAndStatus($tglPinjam1, $tglPinjam2, $status_peminjaman),
            'totalQtyDipinjam' => $this->PeminjamanModel->getTotalJumlahByTglAndStatus($tglPinjam1, $tglPinjam2, $status_peminjaman)
        ];
        // dd($data);
        return view('Admin/Pelaporan/cetak', $data);
    }
}
