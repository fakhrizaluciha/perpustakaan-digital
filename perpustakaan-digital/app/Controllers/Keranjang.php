<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\KeranjangModel;
use App\Models\BukuModel;
use App\Models\PeminjamanModel;

class Keranjang extends BaseController
{
    protected $KeranjangModel;
    protected $BukuModel;
    protected $PeminjamanModel;
    public function __construct()
    {
        $this->KeranjangModel = new KeranjangModel();
        $this->BukuModel = new BukuModel();
        $this->PeminjamanModel = new PeminjamanModel();
    }
    public function index()
    {
        $data = [
            'keranjang' => $this->KeranjangModel->getKeranjangByIdUser()
        ];
        // dd($data);
        return view('User/keranjang/index', $data);
    }
    public function save()
    {
        // dd($this->request->getVar());
        // dd(count($this->PeminjamanModel->getPeminjamanByUserAndStatus()));
        date_default_timezone_set('Asia/Jakarta');
        $batas = date_diff(date_create($this->request->getVar('deadline')), date_create($this->request->getVar('tgl_pinjam')))->format("%a");
        // dd($batas);
        if (user()->nis == null) {
            session()->setFlashdata('pesan', 'Lengkapi Data Diri Anda!');
            throw new \CodeIgniter\Router\Exceptions\RedirectException('/profile');
        } elseif (user()->alamat == null || user()->jenis_kelamin == "" || user()->tgl_lahir == "") {
            session()->setFlashdata('pesan', 'Lengkapi Data Diri Anda!');
            throw new \CodeIgniter\Router\Exceptions\RedirectException('/profile');
        } elseif ($this->request->getVar('stok') == '0') {
            session()->setFlashdata('gagal', 'Stok buku sedang kosong');
            return redirect()->to('/detail/' . $this->request->getVar('judul_buku'));
        } elseif ($batas > 10) {
            session()->setFlashdata('gagal', 'Maksimal peminjaman 10 hari');
            return redirect()->to('/detail/' . $this->request->getVar('judul_buku'));
        }
        if (!$this->validate([
            'deadline'     => 'min_length[3]'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/detail/' . $this->request->getVar('judul_buku'))->withInput()->with('validation', $validation);
        }
        $deadline = date_create($this->request->getVar('deadline'));
        $tglDeadline = $deadline->format("d");
        $blnDeadline = $deadline->format("m");
        $pinjam = date_create($this->request->getVar('tgl_pinjam'));
        $tglPinjam = $pinjam->format("d");
        $blnPinjam = $pinjam->format("m");
        if ($tglDeadline < $tglPinjam && $blnDeadline == $blnPinjam) {
            session()->setFlashdata('gagal', 'Tanggal pengembalian tidak boleh kurang dari tanggal peminjaman!');
            return redirect()->to('/detail/' . $this->request->getVar('judul_buku'));
        } elseif ($tglDeadline > $tglPinjam && $blnDeadline < $blnPinjam) {
            session()->setFlashdata('gagal', 'Tanggal pengembalian tidak boleh kurang dari tanggal peminjaman!');
            return redirect()->to('/detail/' . $this->request->getVar('judul_buku'));
        } elseif ($tglDeadline == $tglPinjam && $blnDeadline < $blnPinjam) {
            session()->setFlashdata('gagal', 'Tanggal pengembalian tidak boleh kurang dari tanggal peminjaman!');
            return redirect()->to('/detail/' . $this->request->getVar('judul_buku'));
        } elseif ($tglDeadline < $tglPinjam && $blnDeadline < $blnPinjam) {
            session()->setFlashdata('gagal', 'Tanggal pengembalian tidak boleh kurang dari tanggal peminjaman!');
            return redirect()->to('/detail/' . $this->request->getVar('judul_buku'));
        }
        $id_buku = $this->request->getVar('id_buku');
        // dd($id_buku);
        $data = [
            'keranjang' => $this->KeranjangModel->getKeranjangByIdUser(),
            'buku'      => $this->KeranjangModel->getKeranjangByIdBuku($id_buku),
            'total'     => $this->KeranjangModel->getTotalKeranjangByIdUser()
        ];
        // dd($data);
        if (count($this->PeminjamanModel->getPeminjamanByUserAndStatus()) == 2) {
            session()->setFlashdata('gagal', 'Buku yang masih dipinjam maksimal 2!');
            return redirect()->to('/detail/' . $this->request->getVar('judul_buku'));
        }
        if (count($this->PeminjamanModel->getPeminjamanByUserAndStatus()) == 1) {
            if ($data['total'][0]->total == 1) {
                session()->setFlashdata('gagal', 'Maksimal peminjaman adalah 1 buku!');
                return redirect()->to('/detail/' . $this->request->getVar('judul_buku'));
            }
            if ($id_buku == $this->PeminjamanModel->getPeminjamanByUserAndStatus()[0]->id_buku) {
                session()->setFlashdata('gagal', 'Tidak boleh meminjam buku yang sama!');
                return redirect()->to('/detail/' . $this->request->getVar('judul_buku'));
            }
            $this->KeranjangModel->save([
                'user_id'        => $this->request->getVar('user_id'),
                'buku_id'    => $this->request->getVar('buku_id'),
                'tgl_pinjam'   => $this->request->getVar('tgl_pinjam'),
                'deadline'     => $this->request->getVar('deadline'),
                'qty'       => 1
            ]);
            session()->setFlashdata('pesan', 'Berhasil dimasukkan ke troli');
            return redirect()->to('/keranjang');
            if ($this->request->getVar('buku_id') == $data['buku'][0]->buku_id) {
                session()->setFlashdata('gagal', 'Buku yang dimasukkan tidak boleh sama!');
                return redirect()->to('/detail/' . $this->request->getVar('judul_buku'));
            }
        }
        // if (empty($this->PeminjamanModel->getPeminjamanByUserAndStatus())) {
        //     $this->KeranjangModel->save([
        //         'user_id'        => $this->request->getVar('user_id'),
        //         'buku_id'    => $this->request->getVar('buku_id'),
        //         'tgl_pinjam'   => $this->request->getVar('tgl_pinjam'),
        //         'deadline'     => $this->request->getVar('deadline'),
        //         'qty'       => 1
        //     ]);
        //     session()->setFlashdata('pesan', 'Berhasil dimasukkan ke troli');
        //     return redirect()->to('/keranjang');
        // }        

        // if ($data['buku'][0]->qty >= $this->request->getVar('stok')) {
        //     session()->setFlashdata('gagal', 'Jumlah peminjaman melebihi jumlah stok!');
        //     return redirect()->to('/detail/' . $this->request->getVar('judul_buku'));
        // }
        if ($data['total'][0]->total == 2) {
            session()->setFlashdata('gagal', 'Maksimal peminjaman adalah 2 buku!');
            return redirect()->to('/detail/' . $this->request->getVar('judul_buku'));
        }
        if (empty($data['buku'])) {
            if (count($this->PeminjamanModel->getPeminjamanByUserAndStatus()) < 2) {
                $this->KeranjangModel->save([
                    'user_id'        => $this->request->getVar('user_id'),
                    'buku_id'    => $this->request->getVar('buku_id'),
                    'tgl_pinjam'   => $this->request->getVar('tgl_pinjam'),
                    'deadline'     => $this->request->getVar('deadline'),
                    'qty'       => 1
                ]);
                session()->setFlashdata('pesan', 'Berhasil dimasukkan ke troli');
                return redirect()->to('/keranjang');
            }
        }
        if ($this->request->getVar('buku_id') == $data['buku'][0]->buku_id) {
            session()->setFlashdata('gagal', 'Buku yang dimasukkan tidak boleh sama!');
            return redirect()->to('/detail/' . $this->request->getVar('judul_buku'));
        }
        if ($id_buku == $this->PeminjamanModel->getPeminjamanByUserAndStatus()[0]->id_buku) {
            session()->setFlashdata('gagal', 'Tidak boleh meminjam buku yang sama!');
            return redirect()->to('/detail/' . $this->request->getVar('judul_buku'));
        }
        // if ($this->request->getVar('id_buku') != $data['buku'][0]->buku_id) {
        //     $this->KeranjangModel->save([
        //         'user_id'        => $this->request->getVar('user_id'),
        //         'buku_id'    => $this->request->getVar('buku_id'),
        //         'tgl_pinjam'   => $this->request->getVar('tgl_pinjam'),
        //         'deadline'     => $this->request->getVar('deadline'),
        //         'qty'       => 1
        //     ]);
        //     session()->setFlashdata('pesan', 'Berhasil dimasukkan ke troli baru');
        //     return redirect()->to('/keranjang');
        // }
        // $this->KeranjangModel->save([
        //     'id_keranjang'  => $data['buku'][0]->id_keranjang,
        //     'user_id'        => $this->request->getVar('user_id'),
        //     'buku_id'    => $this->request->getVar('buku_id'),
        //     'tgl_pinjam'   => $this->request->getVar('tgl_pinjam'),
        //     'deadline'     => $this->request->getVar('deadline'),
        //     'qty'       => $data['buku'][0]->qty + 1
        // ]);
    }
    public function delete($id_keranjang)
    {
        // dd($this->request->getVar());
        // $id_buku = $this->request->getVar('buku_id');
        // $data = [
        //     'keranjang' => $this->KeranjangModel->getKeranjangByIdUser(),
        //     'buku'      => $this->KeranjangModel->getKeranjangByIdBuku($id_buku),
        //     'total'     => $this->KeranjangModel->getTotalKeranjangByIdUser()
        // ];
        // dd($data);
        $buku = $this->KeranjangModel->find($id_keranjang);
        $this->KeranjangModel->delete($id_keranjang);
        // $this->KeranjangModel->save([
        //     'id_keranjang' => $data['buku'][0]->id_keranjang,
        //     'qty'    => $data['buku'][0]->qty - 1
        // ]);
        session()->setFlashdata('pesan', 'Berhasil dihapus');
        return redirect()->to('/keranjang');
    }
}
