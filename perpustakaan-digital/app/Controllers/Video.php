<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\VideoModel;

class Video extends BaseController
{
    protected $VideoModel;
    public function __construct()
    {
        $this->VideoModel = new VideoModel();
    }
    public function index()
    {
        $data = [
            'video'       => $this->VideoModel->getVideo(),
        ];
        // dd($data);
        return view('Admin/Video/index', $data);
    }
    public function create()
    {
        $db = \Config\Database::connect();
        $data = [
            'video'      => $this->VideoModel->getVideo(),
            'validation' => \Config\Services::validation()
        ];
        // dd($data);
        return view('Admin/Video/create', $data);
    }
    public function save()
    {
        // dd($this->request->getVar());
        if (!$this->validate([
            'video' => [
                'rules' => [
                    'mime_in[video,video/mp4,video/mov]',
                ],
                'errors' => [
                    'mime_in' => 'Video harus berekstensi video.'
                ]
            ],
            'tahun_terbit' => 'required',
            'penerbit' => 'required',
            'judul_video' => [
                'rules' => 'required|is_unique[video.judul_video]',
                'errors' => [
                    'required' => 'Judul tidak boleh kosong',
                    'is_unique' => 'Judul sudah terdaftar'
                ]
            ]
        ])) {
            return redirect()->to('/admin/video/tambah')->withInput();
        }
        $video = $this->request->getFile('video');
        if ($video->getError() == 4) {
            $namaVideo = 'undraw_profile.svg';
        } else {
            $namaVideo = $video->getRandomName();
            $video->move("video", $namaVideo);
        }
        // $slug = url_title($this->request->getVar('gambar'), '-', true);
        $this->VideoModel->save([
            'url'  => $this->request->getVar('url'),
            'judul_video'    => $this->request->getVar('judul_video'),
            'created_at'  => $this->request->getVar('created_at'),
            'penerbit'      => $this->request->getVar('penerbit'),
            'tahun_terbit'  => $this->request->getVar('tahun_terbit'),
            'video'        => $namaVideo,
        ]);
        session()->setFlashdata('pesan', 'Berhasil ditambahkan');
        return redirect()->to('/admin/video');
    }
    public function deleteVideo($id_video)
    {
        // $video = $this->VideoModel->find($id_video);
        // if ($video['video'] != 'undraw_profile.svg') {
        //     unlink('video/' . $video['video']);
        // }        
        date_default_timezone_set('Asia/Jakarta');
        $db = \Config\Database::connect();
        $db->table('video')->set('deleted_at', date_create()->format('Y-m-d'), true)->where(['id_video' => $id_video])->update();
        session()->setFlashdata('pesan', 'Berhasil dihapus');
        return redirect()->to('/admin/video');
    }
    public function edit($id_video)
    {
        $db = \Config\Database::connect();
        $data = [
            'video'       => $this->VideoModel->getVideoById($id_video),
            'validation' => \Config\Services::validation()
        ];
        // dd($data);
        return view('Admin/Video/edit', $data);
    }
    public function update($id_video)
    {
        // dd($this->request->getVar());
        if (!$this->validate([
            'video' => [
                'rules' => [
                    'mime_in[video,video/mp4,video/mov]',
                ],
                'errors' => [
                    'mime_in' => 'Video harus berekstensi video.'
                ]
            ],
            'tahun_terbit' => 'required',
            'penerbit' => 'required',
            'judul_video' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Judul tidak boleh kosong',
                ]
            ]
        ])) {
            return redirect()->to('/admin/video/tambah')->withInput();
        }
        $video = $this->request->getFile('video');
        if ($video->getError() == 4) {
            $namaVideo = $this->request->getVar('videoLama');
        } else {
            $namaVideo = $video->getRandomName();
            $video->move("video", $namaVideo);
            unlink('video/' . $this->request->getVar('videoLama'));
        }
        $this->VideoModel->save([
            'id_video'       => $id_video,
            'url'  => $this->request->getVar('url'),
            'judul_video'    => $this->request->getVar('judul_video'),
            'penerbit'      => $this->request->getVar('penerbit'),
            'tahun_terbit'  => $this->request->getVar('tahun_terbit'),
            'video'        => $namaVideo,
        ]);
        session()->setFlashdata('pesan', 'Berhasil diupdate');
        return redirect()->to('/admin/video');
    }

    public function sampah()
    {
        $data = [
            'video' => $this->VideoModel->getSampahVideo()
        ];
        for ($i = 0; $i < count($data['video']); $i++) {
            // dd(count($data['video']));
            date_default_timezone_set('Asia/Jakarta');
            $tglDelete = date_create($data['video'][$i]->deleted_at);
            $tglSekarang = date_create();
            // dd(date_diff($tglDelete, $tglSekarang)->format('%a'));
            if (date_diff($tglDelete, $tglSekarang)->format('%a') == 10) {
                $this->VideoModel->delete($data['video'][$i]->id_video, true);
                if ($data['video'][$i]->video != 'undraw_profile.svg') {
                    unlink('video/' . $data['video'][$i]->video);
                }
            }
        }
        // dd($data);
        return view('Admin/Video/sampah', $data);
    }

    public function restore($id_video)
    {
        $db = \Config\Database::connect();
        $db->table('video')->set('deleted_at', null, true)->where(['id_video' => $id_video])->update();
        session()->setFlashdata('pesan', 'Berhasil restore');
        return redirect()->to('/admin/video');
    }
}
