<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\PengembalianModel;
use App\Models\BukuModel;
use \Myth\Auth\Models\UserModel;

class Pengembalian extends BaseController
{
    protected $PengembalianModel;
    protected $BukuModel;
    protected $UserModel;
    public function __construct()
    {
        $this->PengembalianModel = new PengembalianModel();
        $this->BukuModel = new BukuModel();
        $this->UserModel = new UserModel();
    }
    public function index()
    {
        $data = [
            'pengembalian'  => $this->PengembalianModel->getPengembalian(),
        ];
        // dd($data);
        // dd($tglPinjam1, $tglPinjam2);
        return view('Admin/Pengembalian/index', $data);
    }

    public function createPengembalian()
    {
        $db = \Config\Database::connect();
        $builder = $db->table('pengembalians')->get()->getResult();
        $data = [
            'kategori'  => $builder,
            'pengembalian'    => $this->PengembalianModel->getPengembalian(),
            'validation' => \Config\Services::validation(),
            'user'      => $db->table('users')->get()->getResult(),
            'buku'      => $this->BukuModel->getBuku()
        ];
        // dd($data);
        return view('Admin/Pengembalian/create', $data);
    }
    public function save()
    {
        // dd($this->request->getVar());
        // if (countOf()) {
        //     # code...
        // }
        // dd($this->request->getVar());
        if (!$this->validate([
            'user_id' => 'required',
            'buku_id' => 'required',
            'tgl_kembali' => 'required',
            'denda' => 'required',
        ])) {
            return redirect()->to('/admin/tambah-pengembalian')->withInput();
        }
        // $slug = url_title($this->request->getVar('gambar'), '-', true);
        $this->PengembalianModel->save([
            'user_id'        => $this->request->getVar('user_id'),
            'buku_id'    => $this->request->getVar('buku_id'),
            'tgl_kembali'   => $this->request->getVar('tgl_kembali'),
            'denda'     => $this->request->getVar('denda'),
            // 'tgl_kembali'      => $this->request->getVar('tgl_kembali'),
        ]);
        session()->setFlashdata('pesan', 'Berhasil ditambahkan');
        return redirect()->to('/admin/data-pengembalian');
    }
    public function deletePengembalian($id_pengembalian)
    {
        // $pinjam = $this->BukuModel->find($id_pengembalian);
        $this->PengembalianModel->delete($id_pengembalian);
        session()->setFlashdata('pesan', 'Berhasil dihapus');
        return redirect()->to('/admin/data-pengembalian');
    }
    public function cetakPengembalian()
    {
        $tglPinjam1 = $_POST['tglPinjam1'];
        $tglPinjam2 = $_POST['tglPinjam2'];
        $data = [
            'pengembalian' => $this->PengembalianModel->getPengembalianByTgl($tglPinjam1, $tglPinjam2),
        ];
        // dd($data);
        // dd($tglPinjam1, $tglPinjam2);
        return view('Admin/Pengembalian/cetak', $data);
    }
    public function editPengembalian()
    {
        $data = [
            'pengembalian'    => $this->PengembalianModel->getPengembalian(),
            'validation' => \Config\Services::validation(),
            'user'      => $this->UserModel->getUser(),
            'buku'      => $this->BukuModel->getBuku()
        ];
        // dd($data);
        return view('Admin/Pengembalian/edit', $data);
    }
    public function update($id_pengembalian)
    {
        // dd($this->request->getVar());
        if (!$this->validate([
            'user_id' => 'required',
            'buku_id' => 'required',
            'tgl_kembali' => 'required',
            'denda' => 'required',
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/admin/edit-pengembalian/' . $this->request->getVar('id_pengembalian'))->withInput()->with('validation', $validation);
        }
        $this->PengembalianModel->save([
            'id_pengembalian'     => $id_pengembalian,
            'user_id'        => $this->request->getVar('user_id'),
            'buku_id'    => $this->request->getVar('buku_id'),
            'tgl_kembali'   => $this->request->getVar('tgl_kembali'),
            'denda'     => $this->request->getVar('denda'),
            // 'tgl_kembali'      => $this->request->getVar('tgl_kembali'),
        ]);
        session()->setFlashdata('pesan', 'Berhasil diupdate');
        return redirect()->to('/admin/data-pengembalian');
    }
}
