<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\PesanModel;
use App\Models\PinjamModel;

class Pesan extends BaseController
{
    protected $PesanModel;
    protected $PinjamModel;

    public function __construct()
    {
        $this->PesanModel = new PesanModel();
        $this->PinjamModel = new PinjamModel();
    }
    public function index($user_id)
    {
        $db = \Config\Database::connect();
        $data = [
            'pesan' => $this->PesanModel->getPesan($user_id),
            'user'  => $db->table('users')->where('id', $user_id)->get()->getResult(),
            'validation' => \Config\Services::validation()
        ];
        // dd($data);
        return view('Admin/Pinjam/Pesan', $data);
    }
    public function kirim()
    {
        $user_id = $this->request->getVar('user_id');
        // dd($this->request->getVar());
        $db = \Config\Database::connect();
        $email = service('email');
        // dd($email);
        $data = [
            'pesan' => $this->PesanModel->getPesan($user_id),
            'user'  => $db->table('users')->where('id', $user_id)->get()->getResult(),
            'validation' => \Config\Services::validation()
        ];
        // dd($data);
        if (!$this->validate([
            'pesan' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Pesan belum ditulis.'
                ]
            ]
        ])) {
            // $validation = \Config\Services::validation();
            return redirect()->to('/admin/pesan/' . $this->request->getVar('user_id'))->withInput();
        }
        $email->setTo($data['user'][0]->email);
        $email->setFrom($email->fromEmail);
        $email->setSubject("Mohon Dibaca");
        $email->setMessage($this->request->getVar('pesan'));
        if ($email->send()) {
            echo ('Berhasil');
        } else {
            $data2 = $email->printDebugger(['headers']);
            print_r($data2);
        }
        $this->PesanModel->save([
            'user_id'      => $this->request->getVar('user_id'),
            'pesan'        => $this->request->getVar('pesan'),
            'created_at'   => $this->request->getVar('created_at')
        ]);
        return redirect()->to('/admin/pesan/' . $this->request->getVar('user_id'));
    }
}
