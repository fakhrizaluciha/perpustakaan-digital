<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Login extends BaseController
{
    public function index()
    {
        $data = [
            'config' => config('Auth')
        ];
        return view('auth/login', $data);
    }
}
