<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\PinjamModel;
use \Myth\Auth\Models\UserModel;
use \Myth\Auth\Models\GroupModel;
use App\Models\PeminjamanModel;
use App\Models\PengembalianModel;
// use CodeIgniter\Session\Session;
// use Myth\Auth\Config\Auth as AuthConfig;
// use Myth\Auth\Entities\User;

class Users extends BaseController
{
    protected $PinjamModel;
    protected $UserModel;
    protected $GroupModel;
    protected $PeminjamanModel;
    protected $PengembalianModel;
    // protected $config;
    public function __construct()
    {
        $this->PinjamModel = new PinjamModel();
        $this->UserModel = new UserModel();
        $this->GroupModel = new GroupModel();
        $this->PeminjamanModel = new PeminjamanModel();
        $this->PengembalianModel = new PengembalianModel();
    }
    public function index()
    {
        $data = [
            'user'  => $this->UserModel->where('id', user_id())->get()->getResult(),
            'validation' => \Config\Services::validation()
        ];
        // dd($data);
        return view('User/User/index', $data);
    }

    public function riwayat()
    {
        $data = [
            'peminjaman'  => $this->PeminjamanModel->getPeminjamanByIdUser(),
        ];
        // dd($data);
        return view('User/User/riwayat', $data);
    }

    public function cetak($id_peminjaman)
    {
        $data = [
            'pinjam'  => $this->PeminjamanModel->getPeminjamanById($id_peminjaman)
        ];
        // dd($data);
        return view('User/User/cetak', $data);
    }

    public function deleteUser($id)
    {
        // dd($userModel);
        // $db = \Config\Database::connect();
        // $peminjam = $this->UserModel->where('id', $id);
        // dd($peminjam);
        // $this->UserModel->delete($id);
        date_default_timezone_set('Asia/Jakarta');
        $db = \Config\Database::connect();
        $db->table('users')->set('deleted_at', date_create()->format('Y-m-d'), true)->where(['id' => $id])->update();
        session()->setFlashdata('pesan', 'Berhasil dihapus');
        // if ($peminjam == true) {
        // } else if ($peminjam != true) {
        //     session()->setFlashdata('gagal', 'Id tidak ditemukan');
        // }
        return redirect()->to('/admin/peminjam');
    }
    public function saveProfil()
    {
        // dd($this->request->getVar());
        $this->UserModel->save([
            'id'        => $this->request->getVar('id'),
            'nis'    => $this->request->getVar('nis'),
            'jenis_kelamin'       => $this->request->getVar('jenis_kelamin'),
            'alamat'      => $this->request->getVar('alamat'),
            'tgl_lahir'      => $this->request->getVar('tgl_lahir'),
        ]);
        session()->setFlashdata('message', 'Berhasil diupdate');
        return redirect()->to('/profile');
    }
    public function saveFoto()
    {
        // dd($this->request->getVar());
        if (!$this->validate([
            'foto' => [
                'rules' => 'is_image[foto]',
                'errors' => [
                    'is_image' => 'Foto harus berekstensi foto.'
                ]
            ],
        ])) {
            return redirect()->to('/profile')->withInput();
        }
        $foto = $this->request->getFile('foto');
        if ($foto->getError() == 4) {
            $namaFoto = 'undraw_profile_2.svg';
        } else {
            $namaFoto = $foto->getRandomName();
            $foto->move("img", $namaFoto);
        }
        $this->UserModel->save([
            'id'        => $this->request->getVar('id'),
            'foto'      => $this->request->getVar('foto'),
            'foto'      => $namaFoto,
        ]);
        session()->setFlashdata('message', 'Berhasil diupdate');
        return redirect()->to('/profile');
    }

    public function Petugas()
    {
        $db = \Config\Database::connect();
        $builder = $db->table('users')->join('auth_groups_users', 'auth_groups_users.user_id = users.id')
            ->join('auth_groups', 'auth_groups.id = auth_groups_users.group_id');
        $query   = $builder->where(['auth_groups.id' => 1])->get()->getResult();
        $data = [
            'users' => $query
        ];
        // dd($data['users']);
        return view('Admin/User/petugas', $data);
    }

    public function Peminjam()
    {
        $db = \Config\Database::connect();
        $builder = $db->table('users')->join('auth_groups_users', 'auth_groups_users.user_id = users.id')
            ->join('auth_groups', 'auth_groups.id = auth_groups_users.group_id');
        $query   = $builder->where(['auth_groups.id' => 2, 'deleted_at' => null])->get()->getResult();
        $data = [
            'users' => $query
        ];
        // dd($data);
        return view('Admin/User/peminjam', $data);
    }

    public function sampahPeminjam()
    {
        $db = \Config\Database::connect();
        $builder = $db->table('users')->join('auth_groups_users', 'auth_groups_users.user_id = users.id')
            ->join('auth_groups', 'auth_groups.id = auth_groups_users.group_id');
        $query   = $builder->where(['auth_groups.id' => 2, 'deleted_at !=' => null])->get()->getResult();
        $data = [
            'users' => $query
        ];
        for ($i = 0; $i < count($data['users']); $i++) {
            // dd(count($data['users']));
            date_default_timezone_set('Asia/Jakarta');
            $tglDelete = date_create($data['users'][$i]->deleted_at);
            $tglSekarang = date_create();
            // dd(date_diff($tglDelete, $tglSekarang)->format('%a'));
            if (date_diff($tglDelete, $tglSekarang)->format('%a') == 10) {
                $this->UserModel->delete($data['users'][$i]->user_id, true);
                if ($data['users'][$i]->foto != 'undraw_profile_2.svg') {
                    unlink('img/' . $data['users'][$i]->foto);
                }
            }
        }
        // dd($data);
        return view('Admin/User/sampahPeminjam', $data);
    }

    public function restorePeminjam($user_id)
    {
        // dd($user_id);
        $db = \Config\Database::connect();
        $db->table('users')->set('deleted_at', null, true)->where(['id' => $user_id])->update();
        session()->setFlashdata('pesan', 'Berhasil restore');
        return redirect()->to('/admin/peminjam');
    }

    public function editPetugas()
    {
        $db = \Config\Database::connect();
        $data = [
            'user'       => $this->UserModel->getUserById(),
            'validation' => \Config\Services::validation()
        ];
        // dd($data);
        return view('Admin/User/edit', $data);
    }

    public function updatePetugas($user_id)
    {
        if (!$this->validate([
            'username' => 'required',
            'password_hash' => 'required',
        ])) {
            return redirect()->to('/admin/petugas/edit/' . $user_id)->withInput();
        }
        // dd(bin2hex(random_bytes(26)));
        $this->UserModel->save([
            'id'       => $user_id,
            'username'        => $this->request->getVar('username'),
            'password_hash'    => bin2hex(random_bytes(26)),
        ]);
        session()->setFlashdata('pesan', 'Berhasil diupdate');
        return redirect()->to('/admin/petugas');
    }

    public function User()
    {
        $db = \Config\Database::connect();
        $builder = $db->table('users')->join('auth_groups_users', 'auth_groups_users.user_id = users.id')
            ->join('auth_groups', 'auth_groups.id = auth_groups_users.group_id');
        $query   = $builder->get()->getResult();
        $data = [
            'users' => $query
        ];
        // dd($data['users']);
        return view('Admin/User/index', $data);
    }

    public function create()
    {
        $db = \Config\Database::connect();
        $builder = $db->table('users')->join('auth_groups_users', 'auth_groups_users.user_id = users.id')
            ->join('auth_groups', 'auth_groups.id = auth_groups_users.group_id');
        $query   = $builder->get()->getResult();
        $data = [
            'validation' => \Config\Services::validation()
        ];
        // dd($data);
        return view('Admin/User/create', $data);
    }

    public function save()
    {
        // dd($this->request->getVar());
        $db = \Config\Database::connect();
        if (!$this->validate([
            'username' => 'required',
            'email' => 'required',
            'password_hash' => 'required',
            'jenis_kelamin' => 'required',
            'tgl_lahir' => 'required',
            'nis' => 'required',
            'alamat' => 'required',
        ])) {
            return redirect()->to('/admin/peminjam/tambah')->withInput();
        }
        $groupId = 2;
        $user = $db->table('users')->insert([
            'password_hash'   => $this->request->getVar('password_hash'),
            'email'    => $this->request->getVar('email'),
            'username'        => $this->request->getVar('username'),
            'jenis_kelamin'       => $this->request->getVar('jenis_kelamin'),
            'tgl_lahir'      => $this->request->getVar('tgl_lahir'),
            'nis'        => $this->request->getVar('nis'),
            'alamat'  => $this->request->getVar('alamat'),
            'active'          => $this->request->getVar('active'),
        ]);
        // $user = new User($this->request->getPost([
        //     'password'   => $this->request->getVar('password'),
        //     'email'    => $this->request->getVar('email'),
        //     'username'        => $this->request->getVar('username'),
        // ]));

        // $this->config->requireActivation === null ? $user->activate() : $user->generateActivateHash();
        $data = [
            'user_id'  => $user,
            'group_id' => $groupId,
        ];
        // dd($data);
        $db->table('auth_groups_users')->insert($data);
        session()->setFlashdata('pesan', 'Berhasil ditambahkan');
        return redirect()->to('/admin/peminjam');
    }
}
