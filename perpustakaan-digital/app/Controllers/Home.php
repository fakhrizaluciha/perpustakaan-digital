<?php

namespace App\Controllers;

use App\Models\BukuModel;
use App\Models\KategoriModel;
use App\Models\PinjamModel;
use App\Models\VideoModel;
use App\Models\PeminjamanModel;
use App\Models\PeminjamanHasBukuModel;
use App\Models\PengembalianModel;
use App\Models\PengembalianHasBukuModel;
use \Myth\Auth\Models\UserModel;
use App\Models\KeranjangModel;

use function PHPUnit\Framework\isEmpty;

class Home extends BaseController
{
    protected $BukuModel;
    protected $PinjamModel;
    protected $KategoriModel;
    protected $UserModel;
    protected $PeminjamanModel;
    protected $PengembalianModel;
    protected $PeminjamanHasBukuModel;
    protected $PengembalianHasBukuModel;
    protected $VideoModel;
    protected $KeranjangModel;
    public function __construct()
    {
        $this->KategoriModel = new KategoriModel();
        $this->BukuModel = new BukuModel();
        $this->PinjamModel = new PinjamModel();
        $this->UserModel = new UserModel();
        $this->PeminjamanModel = new PeminjamanModel();
        $this->PeminjamanHasBukuModel = new PeminjamanHasBukuModel();
        $this->PengembalianHasBukuModel = new PengembalianHasBukuModel();
        $this->PengembalianModel = new PengembalianModel();
        $this->VideoModel = new VideoModel();
        $this->KeranjangModel = new KeranjangModel();
    }
    public function index()
    {
        $db = \Config\Database::connect();
        $where = "group_id='2'";
        $data = [
            'buku' => $this->BukuModel->paginate(6),
            'bukuAdmin' => $this->BukuModel->getBuku(),
            'pager' => $this->BukuModel->pager,
            'user' => $db->table('users')->join('auth_groups_users', 'auth_groups_users.user_id = users.id')->where($where)->get()->getResult(),
            'peminjaman' => $db->table('peminjamans')->get()->getResult(),
            'pengembalian' => $db->table('peminjamans')
                ->join('pengembalians', 'pengembalians.peminjaman_id = peminjamans.id_peminjaman')
                ->where('pengembalians.tgl_kembali != 0000-00-00')->get()->getResult()
        ];
        // dd($data);
        return view('Home', $data);
    }
    public function detail($judul_buku)
    {
        $data = [
            // 'pinjam' => $this->PinjamModel->getPinjam(),
            'user' => $this->UserModel->get()->getResult(),
            'buku' => $this->BukuModel->getBukuByJudul($judul_buku),
            'validation' => \Config\Services::validation()
        ];
        // dd($data);
        return view('User/Buku/detail', $data);
    }
    public function save()
    {
        if (!$this->validate([
            'deadline'     => 'min_length[3]'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/keranjang')->withInput()->with('validation', $validation);
        }
        $keranjang = $this->KeranjangModel->getKeranjangByIdUser();
        // dd($keranjang);
        for ($i = 0; $i < count($keranjang); $i++) {
            if ($keranjang[$i]->stok == 0) {
                session()->setFlashdata('gagal', 'Buku dengan judul ' . $keranjang[$i]->judul_buku . ' stok sudah habis');
                return redirect()->to('/keranjang');
            } elseif ($keranjang[$i]->qty > $keranjang[$i]->stok) {
                session()->setFlashdata('gagal', 'Buku dengan judul ' . $keranjang[$i]->judul_buku . ' stok tinggal ' . $keranjang[$i]->stok);
                return redirect()->to('/keranjang');
            }
            $peminjaman = $this->PeminjamanModel->insert([
                'user_id'        => $this->request->getVar('user_id'),
                'buku_id'    => $keranjang[$i]->buku_id,
                'tgl_pinjam'   => $keranjang[$i]->tgl_pinjam,
                'deadline'     => $keranjang[$i]->deadline,
                'qty'       => $keranjang[$i]->qty,
                'kode'  => uniqid()
            ]);
            $this->PeminjamanHasBukuModel->save([
                'peminjaman_id' => $peminjaman,
                'buku_id'   => $keranjang[$i]->buku_id
            ]);
            $pengembalian = $this->PengembalianModel->insert([
                'user_id'   => $this->request->getVar('user_id'),
                'peminjaman_id' => $peminjaman,
                'denda'       => $this->request->getVar('denda'),
                'tgl_kembali'      => $this->request->getVar('tgl_kembali'),
            ]);
            // dd($pengembalian);
            $this->PengembalianHasBukuModel->save([
                'pengembalian_id' => $pengembalian,
                'buku_id'   => $keranjang[$i]->buku_id
            ]);
            $this->BukuModel->save([
                'id_buku'   => $keranjang[$i]->buku_id,
                'stok'      => $keranjang[$i]->stok - $keranjang[$i]->qty
            ]);
        }
        $this->KeranjangModel->deleteKeranjang();
        session()->setFlashdata('pesan', 'Berhasil dipinjam');
        return redirect()->to('/riwayat');
    }
    public function koleksi()
    {
        // dd($this->request->getVar());
        $keyword = $this->request->getGet('keyword');
        $kategori = $this->request->getGet('kategori');
        if ($keyword && $kategori) {
            $buku = $this->BukuModel->doubleSearch($keyword, $kategori);
        }
        if ($keyword && $kategori == '') {
            $buku = $this->BukuModel->searchJudul($keyword);
        }
        if ($kategori && $keyword == '') {
            $buku = $this->BukuModel->searchKategori($kategori);
        } else {
            $buku = $this->BukuModel;
        }
        $data = [
            'kategori' => $this->KategoriModel->getKategoriById(),
            'buku' => $buku->paginate(10),
            'pager' => $this->BukuModel->pager,
        ];
        // if (empty($data['buku'])) {
        //     throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound('Pencarian ' . $keyword . ' tidak ditemukan.');
        // }
        return view('User/Buku/koleksi', $data);
    }
    public function koleksiKategori($kategori_id)
    {
        $keyword = $this->request->getGet('keyword');
        $kategori = $this->request->getGet('kategori');
        if ($keyword && $kategori) {
            $buku = $this->BukuModel->doubleSearch($keyword, $kategori);
        }
        if ($keyword) {
            $buku = $this->BukuModel->searchJudul($keyword);
        }
        if ($kategori) {
            $buku = $this->BukuModel->searchKategori($kategori);
        } else {
            $buku = $this->BukuModel;
        }
        // $object = new stdClass();
        // if (is_array($buku->getBukuByKategori($kategori_id, $keyword))) {
        //     foreach ($buku->getBukuByKategori($kategori_id, $keyword) as $kolom => $sisi);
        //     $kolom = strtolower(trim($kolom));
        //     $object->$kolom = $sisi;
        // }
        $data = [
            'buku'  => $buku->getBukuByKategori($kategori_id, $keyword),
            'kategori' => $this->KategoriModel->getKategoriById(),
            'pager' => $this->BukuModel->pager,
        ];
        if (empty($data['buku'])) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound('Pencarian ' . $keyword . ' tidak ditemukan.');
        }
        // dd($data);
        return view('User/Buku/koleksiByKategori', $data);
    }
    public function koleksiVideo()
    {
        $keyword = $this->request->getGet('keyword');
        if ($keyword) {
            $video = $this->VideoModel->search($keyword);
        } else {
            $video = $this->VideoModel;
        }
        $data = [
            'video' => $video->paginate(10),
            'pager' => $this->VideoModel->pager,
        ];
        // dd($data);
        // if (empty($data['video'])) {
        //     throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound('Pencarian ' . $keyword . ' tidak ditemukan.');
        // } elseif (empty($data['video'])) {
        //     throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound('Pencarian ' . $keyword . ' tidak ditemukan.');
        // }
        return view('User/Video/index', $data);
    }
    public function Pesan()
    {
        $db = \Config\Database::connect();
        $user = user()->id;
        $pesan = $db->query("SELECT * FROM pesan WHERE user_id = $user ORDER BY id_pesan DESC")->getResult();
        $data = [
            'pesan' => $pesan
        ];
        return view('User/Pesan/index', $data);
    }
}
