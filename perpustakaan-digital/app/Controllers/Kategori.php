<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\KategoriModel;

class Kategori extends BaseController
{
    protected $KategoriModel;
    public function __construct()
    {
        $this->KategoriModel = new KategoriModel();
    }
    public function index()
    {
        $db = \Config\Database::connect();
        $builder = $db->table('kategoris')->get()->getResult();
        $data = [
            'kategori'   => $builder,
            'validation' => \Config\Services::validation()
        ];
        // dd($data);
        return view('Admin/Kategori/create', $data);
    }
    public function save()
    {
        if (!$this->validate([
            'kategori' => [
                'rules' => 'required|is_unique[kategoris.kategori]',
                'errors' => [
                    'required' => 'Kategori tidak boleh kosong',
                    'is_unique' => 'Kategori sudah terdaftar'
                ]
            ]
        ])) {
            return redirect()->to('/admin/kategori')->withInput();
        }
        $this->KategoriModel->save([
            'kategori'        => $this->request->getVar('kategori'),
        ]);
        session()->setFlashdata('pesan', 'Berhasil ditambahkan');
        return redirect()->to('/admin/kategori');
    }
    public function delete($id_kategori)
    {
        $buku = $this->KategoriModel->find($id_kategori);
        $this->KategoriModel->delete($id_kategori);
        session()->setFlashdata('pesan', 'Berhasil dihapus');
        return redirect()->to('/admin/kategori');
    }
    public function edit($id_kategori)
    {
        $data = [
            'kategori'       => $this->KategoriModel->getKategoriById($id_kategori),
            'validation' => \Config\Services::validation()
        ];
        // dd($data);
        return view('Admin/Kategori/edit', $data);
    }
    public function update($id_kategori)
    {
        if (!$this->validate([
            'kategori' => [
                'rules' => 'required|is_unique[kategoris.kategori]',
                'errors' => [
                    'required' => 'Kategori tidak boleh kosong',
                    'is_unique' => 'Kategori sudah terdaftar'
                ]
            ]
        ])) {
            return redirect()->to('/admin/kategori/' . $id_kategori)->withInput();
        }
        $this->KategoriModel->save([
            'id_kategori'       => $id_kategori,
            'kategori'          => $this->request->getVar('kategori')
        ]);
        session()->setFlashdata('pesan', 'Berhasil diupdate');
        return redirect()->to('/admin/kategori');
    }
}
