<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\BukuModel;

class Buku extends BaseController
{
    protected $BukuModel;
    public function __construct()
    {
        $this->BukuModel = new BukuModel();
    }
    public function index()
    {
        // $array = ['group_id' => $group_id];
        $db = \Config\Database::connect();
        $null = null;
        $where = "group_id='2'";
        $data = [
            'buku' => $this->BukuModel->getBuku(),
            'user' => $db->table('users')->join('auth_groups_users', 'auth_groups_users.user_id = users.id')->where($where)->get()->getResult(),
            'peminjaman' => $db->table('peminjamans')->get()->getResult(),
            'pengembalian' => $db->table('peminjamans')
                ->join('pengembalians', 'pengembalians.peminjaman_id = peminjamans.id_peminjaman')
                ->where('pengembalians.tgl_kembali != 0000-00-00')->get()->getResult()
        ];
        // dd($data);
        return view('Admin/index', $data);
    }
    public function Buku()
    {
        $data = [
            'buku'       => $this->BukuModel->getBuku(),
        ];
        // dd($data);
        return view('Admin/Buku/index', $data);
    }
    public function create()
    {
        $db = \Config\Database::connect();
        $builder = $db->table('kategoris')->get()->getResult();
        $data = [
            'kategori' => $builder,
            'buku'      => $this->BukuModel->getBuku(),
            'validation' => \Config\Services::validation()
        ];
        // dd($data);
        return view('Admin/Buku/create', $data);
    }
    public function save()
    {
        // dd($this->request->getVar());
        if (!$this->validate([
            'gambar' => [
                'rules' => 'is_image[gambar]',
                'errors' => [
                    'is_image' => 'Gambar harus berekstensi gambar.'
                ]
            ],
            'stok' => 'required',
            'uraian' => 'required',
            'kode_tempat' => 'required',
            'judul_buku' => [
                'rules' => 'required|is_unique[bukus.judul_buku]',
                'errors' => [
                    'required' => 'Judul tidak boleh kosong',
                    'is_unique' => 'Judul sudah terdaftar'
                ]
            ]
        ])) {
            return redirect()->to('/admin/buku/tambah')->withInput();
        }
        $gambar = $this->request->getFile('gambar');
        if ($gambar->getError() == 4) {
            $namaGambar = 'undraw_profile.svg';
        } else {
            $namaGambar = $gambar->getRandomName();
            $gambar->move("img", $namaGambar);
        }
        $this->BukuModel->save([
            'gambar'        => $this->request->getVar('gambar'),
            'stok'        => $this->request->getVar('stok'),
            'judul_buku'    => $this->request->getVar('judul_buku'),
            'kode_tempat'   => $this->request->getVar('kode_tempat'),
            'kategori_id'   => $this->request->getVar('kategori_id'),
            'penulis'       => $this->request->getVar('penulis'),
            'penerbit'      => $this->request->getVar('penerbit'),
            'uraian'        => $this->request->getVar('uraian'),
            'tahun_terbit'  => $this->request->getVar('tahun_terbit'),
            'isbn'          => $this->request->getVar('isbn'),
            'sumber'        => $this->request->getVar('sumber'),
            'gambar'        => $namaGambar,
            'created_at'    => $this->request->getVar('created_at')
        ]);
        session()->setFlashdata('pesan', 'Berhasil ditambahkan');
        return redirect()->to('/admin/buku');
    }
    public function deleteBuku($id_buku)
    {
        // $buku = $this->BukuModel->find($id_buku);
        // if ($buku['gambar'] != 'undraw_profile.svg') {
        //     unlink('img/' . $buku['gambar']);
        // }
        // $this->BukuModel->delete($id_buku);
        date_default_timezone_set('Asia/Jakarta');
        $db = \Config\Database::connect();
        $db->table('bukus')->set('deleted_at', date_create()->format('Y-m-d'), true)->where(['id_buku' => $id_buku])->update();
        session()->setFlashdata('pesan', 'Berhasil dihapus');
        return redirect()->to('/admin/buku');
    }
    public function editBuku($id_buku)
    {
        $db = \Config\Database::connect();
        $builder = $db->table('kategoris')->get()->getResult();
        $data = [
            'buku'       => $this->BukuModel->getBukuById($id_buku),
            'kategori'   => $builder,
            'validation' => \Config\Services::validation()
        ];
        // dd($data);
        return view('Admin/Buku/edit', $data);
    }
    public function updateBuku($id_buku)
    {
        // dd($this->request->getVar());
        if (!$this->validate([
            'gambar' => [
                'rules' => 'is_image[gambar]',
                'errors' => [
                    'is_image' => 'Gambar harus berekstensi gambar.'
                ]
            ],
            'stok' => 'required',
            'uraian' => 'required',
            'kode_tempat' => 'required',
            'judul_buku' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Judul tidak boleh kosong',
                ]
            ]
        ])) {
            // $validation = \Config\Services::validation();
            return redirect()->to('/admin/buku/edit/' . $id_buku)->withInput();
        }
        $gambar = $this->request->getFile('gambar');
        if ($gambar->getError() == 4) {
            $namaGambar = $this->request->getVar('gambarLama');
        } else {
            $namaGambar = $gambar->getRandomName();
            $gambar->move("img", $namaGambar);
            unlink('img/' . $this->request->getVar('gambarLama'));
        }
        $kategori = $this->request->getVar('kategori');
        $this->BukuModel->save([
            'id_buku'       => $id_buku,
            'gambar'        => $namaGambar,
            'kode_tempat'   => $this->request->getVar('kode_tempat'),
            'judul_buku'    => $this->request->getVar('judul_buku'),
            'kategori_id'   => $this->request->getVar('kategori_id'),
            'stok'          => $this->request->getVar('stok'),
            'penulis'       => $this->request->getVar('penulis'),
            'penerbit'      => $this->request->getVar('penerbit'),
            'uraian'        => $this->request->getVar('uraian'),
            'tahun_terbit'  => $this->request->getVar('tahun_terbit'),
            'isbn'          => $this->request->getVar('isbn'),
            'sumber'        => $this->request->getVar('sumber'),
        ]);
        session()->setFlashdata('pesan', 'Berhasil diupdate');
        return redirect()->to('/admin/buku');
    }

    public function sampah()
    {
        $data = [
            'buku' => $this->BukuModel->getSampahBuku()
        ];
        // dd($data['buku']);
        for ($i = 0; $i < count($data['buku']); $i++) {
            // dd(count($data['buku']));
            date_default_timezone_set('Asia/Jakarta');
            $tglDelete = date_create($data['buku'][$i]->deleted_at);
            $tglSekarang = date_create();
            // dd(date_diff($tglDelete, $tglSekarang)->format('%a'));
            if (date_diff($tglDelete, $tglSekarang)->format('%a') == 10) {
                $this->BukuModel->delete($data['buku'][$i]->id_buku, true);
                if ($data['buku'][$i]->gambar != 'undraw_profile.svg') {
                    unlink('img/' . $data['buku'][$i]->gambar);
                }
            }
        }
        return view('Admin/Buku/sampah', $data);
    }

    public function restore($id_buku)
    {
        $db = \Config\Database::connect();
        $db->table('bukus')->set('deleted_at', null, true)->where(['id_buku' => $id_buku])->update();
        session()->setFlashdata('pesan', 'Berhasil restore');
        return redirect()->to('/admin/buku');
    }
}
